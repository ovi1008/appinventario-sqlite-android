BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "documento" (
	"idDocumento"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"isbnDocumento"	varchar(10) NOT NULL,
	"idTipoDocumento"	int NOT NULL,
	"idEditorial"	int NOT NULL,
	"codIdioma"	int NOT NULL,
	"nombreDocumento"	varchar(20) NOT NULL,
	"edicionDocumento"	int NOT NULL,
	"publicacionDocumento"	VARCHAR(10),
	"prestadoDocumento"	INTEGER,
	"autoresDocumento"	VARCHAR(300) NOT NULL,
	CONSTRAINT "fk_DOCUMENTO_EDITORIAL" FOREIGN KEY("idEditorial") REFERENCES "editorial"("idEditorial") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_DOCUMENTO_IDIOMA" FOREIGN KEY("codIdioma") REFERENCES "idioma"("codIdioma") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_DOCUMENTO_TIPODOCUMENTO" FOREIGN KEY("idTipoDocumento") REFERENCES "tipoDocumento"("idTipoDocumento") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "documento" ("idDocumento","isbnDocumento","idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento","prestadoDocumento","autoresDocumento") VALUES (1,'1212123',1,1,1,'FISICA I',2,'04/03/2019',0,'SUS AUTORES: AUTOR1, AUTOR2'),
 (2,'7949464646',1,5,3,'PRUEBA00001',2,'22/05/2019',1,'JUAN PEREZ
SALVADOR ARRUE
'),
 (3,'4646464649',1,4,3,'SJSJSJ',4,'31/05/2019',1,'JUAN PEREZ
SALVADOR ARRUE
'),
 (4,'46464',2,3,3,'SJSJ',7,'24/05/2019',1,'SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ
');
COMMIT;
