BEGIN TRANSACTION;

/*TABLAS*/

/*1*/
CREATE TABLE IF NOT EXISTS "usuario" (
	"idUsuario"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"nombreUsuario"	VARCHAR(20) NOT NULL,
	"apellidoUsuario"	VARCHAR(20) NOT NULL,
	"userUsuario"	VARCHAR(10) NOT NULL UNIQUE,
	"passwordUsuario"	VARCHAR(7) NOT NULL
);

/*2*/
CREATE TABLE IF NOT EXISTS "tipoAutor" (
	"idTipoAutor"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoAutor"	varchar(10) NOT NULL UNIQUE
);

/*3*/
CREATE TABLE IF NOT EXISTS "editorial" (
	"idEditorial"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreEditorial"	varchar(20) NOT NULL UNIQUE
);

/*4*/
CREATE TABLE IF NOT EXISTS "tipoDocumento" (
	"idTipoDocumento"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoDocumento"	varchar(20) NOT NULL UNIQUE
);

/*5*/
CREATE TABLE IF NOT EXISTS "idioma" (
	"codIdioma"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreIdioma"	varchar(20) NOT NULL UNIQUE
);

/*6*/
CREATE TABLE IF NOT EXISTS "materia" (
	"idMateria"	VARCHAR(7) NOT NULL,
	"nombreMateria"	VARCHAR(40) NOT NULL UNIQUE,
	PRIMARY KEY("idMateria")
);


/*7*/
CREATE TABLE IF NOT EXISTS "tipoEquipo" (
	"idTipoEquipo"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoEquipo"	VARCHAR(20) NOT NULL UNIQUE
);

/*8*/
CREATE TABLE IF NOT EXISTS "marca" (
	"idMarca"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreMarca"	varchar(20) NOT NULL UNIQUE
);

/*9*/
CREATE TABLE IF NOT EXISTS "horario" (
	"idHorario"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"tiempoHorario"	varchar(19) NOT NULL UNIQUE
);

/*10*/
CREATE TABLE IF NOT EXISTS "unidadAdministrativa" (
	"idUnidadAdministrativa"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreUnidadAdministrativa"	varchar(10) NOT NULL UNIQUE
);

/*11*/
CREATE TABLE IF NOT EXISTS "motivo" (
	"idMotivo"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreMotivo"	varchar(50) NOT NULL UNIQUE
);

/*12*/

CREATE TABLE IF NOT EXISTS "docente" (
	"carnetDocente"	varchar(7) NOT NULL,
	"nombreDocente"	varchar(10) NOT NULL,
	"apellidoDocente"	varchar(10) NOT NULL,
	"telefonoDocente"	varchar(8),
	"direccionDocente"	varchar(20),
	PRIMARY KEY("carnetDocente")
);

/*13*/
CREATE TABLE IF NOT EXISTS "tipoMovimiento" (
	"idTipoMovimiento"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoMovimiento"	varchar(50) NOT NULL UNIQUE
);

/*14*/

CREATE TABLE IF NOT EXISTS "autor" (
	"idAutor"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"idTipoAutor"	int NOT NULL,
	"nombreAutor"	varchar(20) NOT NULL,
	"apellidoAutor"	varchar(20) NOT NULL,
	"direccionAutor"	varchar(20),
	"telefonoAutor"	varchar(8),
	CONSTRAINT "fk_autor_tipoAutor" FOREIGN KEY("idTipoAutor") REFERENCES "tipoAutor"("idTipoAutor") ON DELETE CASCADE ON UPDATE CASCADE
);


/*15*/
CREATE TABLE IF NOT EXISTS "documento" (
	"idDocumento"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"isbnDocumento"	varchar(10) NOT NULL,
	"idTipoDocumento"	int NOT NULL,
	"idEditorial"	int NOT NULL,
	"codIdioma"	int NOT NULL,
	"nombreDocumento"	varchar(20) NOT NULL,
	"edicionDocumento"	int NOT NULL,
	"publicacionDocumento"	VARCHAR(10),
	"prestadoDocumento"	INTEGER,
	"autoresDocumento"  		VARCHAR(300) NOT NULL,
	CONSTRAINT "fk_DOCUMENTO_EDITORIAL" FOREIGN KEY("idEditorial") REFERENCES "editorial"("idEditorial") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_DOCUMENTO_TIPODOCUMENTO" FOREIGN KEY("idTipoDocumento") REFERENCES "tipoDocumento"("idTipoDocumento") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_DOCUMENTO_IDIOMA" FOREIGN KEY("codIdioma") REFERENCES "idioma"("codIdioma") ON DELETE CASCADE ON UPDATE CASCADE
);


/*16*/
CREATE TABLE IF NOT EXISTS "equipo" (
	"serialEquipo"	VARCHAR(10) NOT NULL,
	"idTipoEquipo"	INTEGER NOT NULL,
	"idMarca"	INTEGER NOT NULL,
	"descripcionEquipo"	varchar(70) NOT NULL,
	"estadoEquipo"	INTEGER NOT NULL,
	"fechaEquipo"	VARCHAR(10) NOT NULL,
	"fechaInactivoEquipo"	VARCHAR(10),
	"prestadoEquipo"	INTEGER,
	PRIMARY KEY("serialEquipo"),
	CONSTRAINT "fk_EQUIPO_MARCA" FOREIGN KEY("idMarca") REFERENCES "marca"("idMarca") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_EQUIPO_TIPOEQUIPO" FOREIGN KEY("idTipoEquipo") REFERENCES "tipoEquipo"("idTipoEquipo") ON DELETE CASCADE ON UPDATE CASCADE
);

/*17*/
CREATE TABLE IF NOT EXISTS "movimientoInventario" (
	"idMovimientoInventario"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"idTipoMovimiento"	int NOT NULL,
	"carnetDocente"	varchar(7) NOT NULL,
	"fechaMovimientoInventario"	varchar(10) NOT NULL,
	"fechaDevolucionMovInv"	varchar(10),
	"estadoMovInv"	Integer NOT NULL,
	"idMotivo"	Integer NOT NULL,
	"idHorario"	Integer,
	"idUnidadAdministrativa"	Integer,
	"serialEquipo"	varchar(10),
	"idDocumento"	Integer,
	"idMateria"	VARCHAR(7),
	CONSTRAINT "fk_documento_movimientoInventario" FOREIGN KEY("idDocumento") REFERENCES "documento"("idDocumento") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_horario_movimientoInventario" FOREIGN KEY("idHorario") REFERENCES "horario"("idHorario") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_MOVIMIENTO_DOCENTE" FOREIGN KEY("carnetDocente") REFERENCES "DOCENTE"("CARNETDOCENTE") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_unidadAdministrativa_movimientoInventario" FOREIGN KEY("idUnidadAdministrativa") REFERENCES "unidadAdministrativa"("idUnidadAdministrativa") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_MOVIMIENTO_TIPOMOVIMIENTO" FOREIGN KEY("idTipoMovimiento") REFERENCES "TIPOMOVIMIENTO"("IDTIPOMOVIMIENTO") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_equipo_movimientoInventario" FOREIGN KEY("serialEquipo") REFERENCES "equipo"("serialEquipo") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_motivo_movimientoInventario" FOREIGN KEY("idMotivo") REFERENCES "motivo"("idMotivo") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "fk_materia_movimientoInventario" FOREIGN KEY("idMateria") REFERENCES "materia"("idMateria") ON DELETE CASCADE ON UPDATE CASCADE
);

/*18*/
CREATE TABLE IF NOT EXISTS "usuarioAccesos" (
	"idUsuarioAccesos"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"idUsuario"	Integer NOT NULL,
	"tMarca"	Integer NOT NULL,
	"tTipoEquipo"	Integer NOT NULL,
	"tEquipo"	Integer NOT NULL,
	"tEditorial"	Integer NOT NULL,
	"tDocumento"	Integer NOT NULL,
	"tAutor"	Integer NOT NULL,
	"tMateria"	Integer NOT NULL,
	"tTipoMovimiento"	Integer NOT NULL,
	"tMotivo"	Integer NOT NULL,
	"tTipoAutor"	Integer NOT NULL,
	"tTipoDocumento"	Integer NOT NULL,
	"tUnidadAdmin"	Integer NOT NULL,
	"tIdioma"	Integer NOT NULL,
	"tDocente"	Integer NOT NULL,
	"tHorario"	Integer NOT NULL,
	"tPrestamo"	Integer NOT NULL,

	CONSTRAINT "fk_usuaro_accesos" FOREIGN KEY("idUsuario") REFERENCES "usuario"("idUsuario") ON DELETE CASCADE ON UPDATE CASCADE
);

/*19*/
CREATE TABLE IF NOT EXISTS "usuarioActual" (
	"idUsuarioActual"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"idUsuario"	Integer ,
	CONSTRAINT "fk_usuaro_actual" FOREIGN KEY("idUsuario") REFERENCES "usuario"("idUsuario") ON DELETE CASCADE ON UPDATE CASCADE
);


/*TRIGGERS*/

/*VerficarAutor*/
CREATE TRIGGER checarAutor
BEFORE INSERT ON autor
FOR EACH ROW
BEGIN
SELECT CASE
WHEN ((SELECT idAutor FROM autor WHERE nombreAutor = NEW.nombreAutor AND apellidoAutor=NEW.apellidoAutor) IS NOT NULL)
THEN RAISE(ABORT, 'Ya existe el autor')
END;
END;


/*PrestarEquipo*/

CREATE TRIGGER prestarEquipo
AFTER INSERT ON movimientoInventario
FOR EACH ROW WHEN NEW.serialEquipo IS NOT NULL
BEGIN
UPDATE equipo SET prestadoEquipo=0 WHERE serialEquipo=NEW.serialEquipo;
END;

/*PrestarDocumento*/

CREATE TRIGGER prestarDocumento
AFTER INSERT ON movimientoInventario
FOR EACH ROW WHEN NEW.idDocumento IS NOT NULL
BEGIN
UPDATE documento SET prestadoDocumento=0 WHERE idDocumento=NEW.idDocumento;
END;

/*CambiarEquipo*/

CREATE TRIGGER cambiar1Equipo
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.serialEquipo <> OLD.serialEquipo
BEGIN
UPDATE equipo SET prestadoEquipo=0 WHERE serialEquipo=NEW.serialEquipo;
END;

CREATE TRIGGER cambiar2Equipo
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.serialEquipo <> OLD.serialEquipo
BEGIN
UPDATE equipo SET prestadoEquipo=1 WHERE serialEquipo=OLD.serialEquipo;
END;

/*CambiarDocumento*/

CREATE TRIGGER cambiar1Documento
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.idDocumento <> OLD.idDocumento
BEGIN
UPDATE documento SET prestadoDocumento=0 WHERE idDocumento=NEW.idDocumento;
END;

CREATE TRIGGER cambiar2Documento
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.idDocumento <> OLD.idDocumento
BEGIN
UPDATE documento SET prestadoDocumento=1 WHERE idDocumento=OLD.idDocumento;
END;



/*DevolverDocumento*/

CREATE TRIGGER devolverDocumento
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.estadoMovInv=0
BEGIN
UPDATE documento SET prestadoDocumento=1 WHERE idDocumento=NEW.idDocumento;
END;

/*DevolverEquipo*/

CREATE TRIGGER devolverEquipo
AFTER UPDATE ON movimientoInventario
FOR EACH ROW WHEN NEW.estadoMovInv=0
BEGIN
UPDATE equipo SET prestadoEquipo=1 WHERE serialEquipo=NEW.serialEquipo;
END;

/*BorrarPrestamoEquipo*/

CREATE TRIGGER borrarEquipo
AFTER DELETE ON movimientoInventario
FOR EACH ROW 
BEGIN
UPDATE equipo SET prestadoEquipo=1 WHERE serialEquipo=OLD.serialEquipo;
END;

/*BorrarDocumento*/

CREATE TRIGGER borrarDocumento
AFTER DELETE ON movimientoInventario
FOR EACH ROW 
BEGIN
UPDATE documento SET prestadoDocumento=1 WHERE idDocumento=OLD.idDocumento;
END;

/*REGISTROS*/

/*1*/

INSERT INTO "usuario" ("idUsuario","nombreUsuario","apellidoUsuario","userUsuario","passwordUsuario") VALUES (1,'JORGE','MEJIA','ovi10','MA13021'),
 (2,'VICTOR','GONZALEZ','victor1600','GA13016'),
 (3,'LORENA','DURAN','astrid','DR10025'),
 (4,'OSCAR','MAYEN','mayen03','MD12038'),
 (5,'ANTONIO','MEJIA','admEqui','AE19001'),
 (6,'ROXANA','ALVARADO','admDoc','AD19002'),
 (7,'MANUEL','GIRON','admMas','AD19003'),
 (8,'JOSE','MARIA','jefeInfo','AD19004');

/*2*/
INSERT INTO "tipoAutor" ("idTipoAutor","nombreTipoAutor") VALUES (1,'ESTUDIANTE'),
 (2,'POETA');
/*3*/
INSERT INTO "editorial" ("idEditorial","nombreEditorial") VALUES (1,'SANTILLANA'),
 (3,'ALIANZA'),
 (4,'ALFAGUARA'),
 (5,'AGUILAR');
/*4*/
INSERT INTO "tipoDocumento" ("idTipoDocumento","nombreTipoDocumento") VALUES (1,'TESIS'),
 (2,'LIBRO');
/*5*/
INSERT INTO "idioma" ("codIdioma","nombreIdioma") VALUES (1,'INGLES'),
 (2,'ESPAÑOL'),
 (3,'FRANCES'),
 (4,'ITALIANO');

/*6*/

INSERT INTO "materia" ("idMateria","nombreMateria") VALUES ('PRN-115','PROGRAMACION I'),
 ('PRN-215','PROGRAMACION II'),
 ('PRM-115', 'PROGRAMACION DE DISPOSITIVOS'),
 ('COS-115', 'COMUNICACIONES UNO');
/*7*/
INSERT INTO "tipoEquipo" ("idTipoEquipo","nombreTipoEquipo") VALUES (1,'LAPTOP'),
 (2,'CAÑON');
/*8*/
INSERT INTO "marca" ("idMarca","nombreMarca") VALUES (1,'DELL'),
 (2,'HP'),
 (3,'TOSHIBA');
/*9*/
INSERT INTO "horario" ("idHorario","tiempoHorario") VALUES (1,'06:20 a.m-08:00 a.m'),
 (2,'08:05 a.m-09:45 a.m'),
 (3,'09:50 a.m-11:30 a.m'),
  (4,'11:35 a.m-13:15 p.m'),
   (5,'13:20 p.m-15:00 p.m');
/*10*/

INSERT INTO "unidadAdministrativa" ("idUnidadAdministrativa","nombreUnidadAdministrativa") VALUES (1,'LCOMP1'),
 (2,'LCOMP2'),
 (3,'CUBICULO1');
/*11*/
INSERT INTO "motivo" ("idMotivo","nombreMotivo") VALUES (1,'CLASE EN GRUPO TEORICO(GT)'),
 (2,'CLASE EN GRUPO DE DISCUSION(GD)'),
 (3,'TRABAJO DE GRADUACION(TDG)'),
(4,'NUEVO INGRESO');
/*12*/
INSERT INTO "docente" ("carnetDocente","nombreDocente","apellidoDocente","telefonoDocente","direccionDocente") VALUES ('GR02008','CESAR','GONZALEZ','7777777','DIR1FDFJBGJDFB'),
 ('PA06021','KAREN','PEÑATE','22222222','DIR2FDFJBGJDFB'),
 ('EB05025','LUIS','ESCOBAR',NULL,NULL);
/*13*/
INSERT INTO "tipoMovimiento" ("idTipoMovimiento","nombreTipoMovimiento") VALUES
 (1,'DOCUMENTOS'),
 (2,'EQUIPO INFORMATICO'),
 (3,'CATEGORIA-A'),
 (4,'CATEGORIA-B');
/*14*/
INSERT INTO "autor" ("idAutor","idTipoAutor","nombreAutor","apellidoAutor","direccionAutor","telefonoAutor") VALUES (1,1,'ROQUE','DALTON','SAN SALVADOR','22433-32323'),
 (2,1,'JUAN','PEREZ',NULL,NULL),
 (3,2,'SALVADOR','ARRUE',NULL,NULL),
 (4,2,'EDGAR','POE',NULL,NULL),
 (5,2,'GABRIEL','MARQUEZ',NULL,NULL),
 (6,1,'HENRRY','LARSSON',NULL,NULL);
/*15*/
INSERT INTO "documento" ("idDocumento","isbnDocumento","idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento","prestadoDocumento","autoresDocumento") VALUES (1,'1212123',1,1,1,'FISICA I',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(2,'1212124',1,1,1,'FISICA II',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(3,'1212125',1,1,1,'FISICA III',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(4,'1212126',1,1,1,'FISICA I',2,'04/08/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(5,'1212125',1,1,1,'QUIMICA',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(6,'1212125',1,1,1,'MATE III',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(7,'1212125',1,1,1,'MATE III',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ"),
(8,'1212125',1,1,1,'MATE III',2,'04/03/2019',1, "SALVADOR ARRUE
EDGAR POE
GABRIEL MARQUEZ");
/*16*/
INSERT INTO "equipo" ("serialEquipo","idTipoEquipo","idMarca","descripcionEquipo","estadoEquipo","fechaEquipo","fechaInactivoEquipo","prestadoEquipo") VALUES ('J7NOCX13F1',1,1,'FNHDFBGDFJBGDFJBDFGJBN',1,'04/03/2019','05/03/2019',1),
 ('J7NOCX13F2',1,1,'FNHDFBGDFJBGDFJBDFGJBN',1,'08/03/2019','10/03/2019',1),
 ('J7NOCX13F3',2,1,'DFGNDFIUN',0,'15/03/2019','20/03/2019',1),
 ('J7NOCX13F4',2,1,'DFGNDFIUN',0,'27/03/2019','',1),
 ('J7NOCX13F5',2,1,'DFGNDFIUN',0,'16/03/2019','20/03/2019',1),
 ('J7NOCX13F6',2,1,'DFGNDFIUN',0,'17/03/2019','20/03/2019',1),
 ('J7NOCX13F7',2,1,'DFGNDFIUN',0,'15/03/2019','20/03/2019',1),
 ('J7NOCX13F8',2,1,'DFGNDFIUN',0,'15/03/2019','20/03/2019',1);
/*17*/
INSERT INTO "movimientoInventario" ("idMovimientoInventario","idTipoMovimiento","carnetDocente","fechaMovimientoInventario","fechaDevolucionMovInv","estadoMovInv","idMotivo","idHorario","idUnidadAdministrativa","serialEquipo","idDocumento","idMateria") 
VALUES 
(1,1,'GR02008','05/03/2019-07:10 a.m.',NULL,1,1,1,1,NULL,1,'PRN-115'),
(2,1,'GR02008','09/03/2019-09:22 a.m.',NULL,1,1,1,1,'J7NOCX13F1',NULL,'PRN-115'),
(3,1,'GR02008','17/03/2019-13:30 p.m.',NULL,1,2,2,2,'J7NOCX13F2',NULL,NULL),
(4,1,'GR02008','21/05/2019-16:52 p.m.',NULL,1,1,1,1,NULL,3,'PRN-115');

/*18*/
INSERT INTO "usuarioAccesos"
("idUsuarioAccesos","idUsuario","tMarca","tTipoEquipo","tEquipo","tEditorial","tDocumento","tAutor","tMateria","tTipoMovimiento",
"tMotivo","tTipoAutor","tTipoDocumento","tUnidadAdmin","tIdioma","tDocente","tHorario","tPrestamo")
VALUES
(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
(2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
(3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
(4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
(5,5,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0),
(6,6,0,0,0,1,1,1,0,0,0,1,1,0,1,0,0,0),
(7,7,0,0,0,0,0,0,1,1,1,0,0,1,0,1,1,1),
(8,8,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
/*19*/
INSERT INTO "usuarioActual"
("idUsuarioActual","idUsuario")
VALUES
(1, NULL);


COMMIT;
