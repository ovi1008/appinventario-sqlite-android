package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.R;

public class PrestamoDevolverActivity extends AppCompatActivity {

    Integer vista;
    EditText editIdPrestamo;
    Calendar calendar=Calendar.getInstance();
    String CERO = "0", DOS_PUNTOS = ":";
    boolean is24HourView=true;
    int hourOfDay, minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_devolver);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");
        if (vista==1){
            TextView titulo=findViewById(R.id.PrestamoRecibirIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud6P1));
        }else {
            TextView titulo=findViewById(R.id.PrestamoRecibirIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud6P2));
        }
        editIdPrestamo=findViewById(R.id.PrestamoRecibirId);
    }

    public String fechaActual(){
        String fecha;
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.US);
        fecha=format.format(calendar.getTime());
        return  fecha;
    }

    public String horaActual(){
        String hora;
        hourOfDay=calendar.get(Calendar.HOUR_OF_DAY);
        minute=calendar.get(Calendar.MINUTE);
        //Formateo el hora obtenido: antepone el 0 si son menores de 10
        String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
        //Formateo el minuto obtenido: antepone el 0 si son menores de 10
        String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
        //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
        String AM_PM;
        if(hourOfDay < 12) {
            AM_PM = "a.m.";
        } else {
            AM_PM = "p.m.";
        }
        hora=horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM;
        return hora;
    }

    public void recibirPrestamo(View v){
        final TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (editIdPrestamo.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.prestamoMsj1a),Toast.LENGTH_LONG).show();
        }else {
            final String mensaje;
            final String fechaYhora=fechaActual()+"-"+horaActual();
            helper.open();
            Prestamo prestamoVista=helper.prestamoActivosConsultar(Integer.valueOf(editIdPrestamo.getText().toString()));
            helper.close();
            if (prestamoVista==null){
                Toast.makeText(this,getResources().getString(R.string.msjesPrestamo03),Toast.LENGTH_LONG).show();
            }
            else {
                if (vista==1){
                    if (prestamoVista.getSerialEquipo()==null){
                        final Prestamo prestamo= new Prestamo();
                        prestamo.setIdMovimientoInventario(Integer.valueOf(editIdPrestamo.getText().toString()));
                        prestamo.setEstadoMovInv(0);
                        prestamo.setFechaDevolucionMovInv(fechaYhora);

                        // Build an AlertDialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(PrestamoDevolverActivity.this);

                        // Set a message/question for alert dialog
                        builder.setMessage(getResources().getString(R.string.prestamoMsj2)+" "+
                                fechaYhora+"" +
                                "\n\n"+getResources().getString(R.string.prestamoMsj3P1)+" "
                                +prestamo.getIdMovimientoInventario());

                        // Specify the dialog is not cancelable
                        builder.setCancelable(false);

                        // Set a title for alert dialog
                        builder.setTitle(getResources().getString(R.string.tituloPrestaRecibir));

                        // Set the positive/yes button click click listener
                        builder.setPositiveButton(getResources().getString(R.string.btnSeguro), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do something when click positive button
                                //rl.setBackgroundColor(Color.parseColor("#FFA4E098"));
                                helper.open();
                                String mensaje= helper.actualizarEstadoPrestamo(prestamo,prestamo.getIdMovimientoInventario());
                                helper.close();
                            }
                        });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do something when click the negative button
                            }
                        });

                        AlertDialog dialog = builder.create();
                        // Display the alert dialog on interface
                        dialog.show();
                        //Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }else {
                        mensaje=getResources().getString(R.string.msjEsPrestamo02);
                        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    if (prestamoVista.getSerialEquipo()==null){
                        mensaje=getResources().getString(R.string.msjEsPrestamo01);
                        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }else{
                        final Prestamo prestamo= new Prestamo();
                        prestamo.setIdMovimientoInventario(Integer.valueOf(editIdPrestamo.getText().toString()));
                        prestamo.setFechaDevolucionMovInv(fechaYhora);
                        prestamo.setEstadoMovInv(0);

                        // Build an AlertDialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(PrestamoDevolverActivity.this);

                        // Set a message/question for alert dialog
                        builder.setMessage(getResources().getString(R.string.prestamoMsj2)
                                +fechaYhora+"" +
                                "\n\n"+getResources().getString(R.string.prestamoMsj3P2)+" "
                                +prestamo.getIdMovimientoInventario());

                        // Specify the dialog is not cancelable
                        builder.setCancelable(false);

                        // Set a title for alert dialog
                        builder.setTitle(getResources().getString(R.string.tituloPrestaRecibir));

                        // Set the positive/yes button click click listener
                        builder.setPositiveButton(getResources().getString(R.string.btnSeguro), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do something when click positive button
                                //rl.setBackgroundColor(Color.parseColor("#FFA4E098"));
                                helper.open();
                                String mensaje= helper.actualizarEstadoPrestamo(prestamo,prestamo.getIdMovimientoInventario());
                                helper.close();
                            }
                        });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do something when click the negative button
                            }
                        });

                        AlertDialog dialog = builder.create();
                        // Display the alert dialog on interface
                        dialog.show();
                        //Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }
                }
            }
            editIdPrestamo.setText("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


}
