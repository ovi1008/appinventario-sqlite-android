package edu.ues.fia.inventarioues.Vistas.TipoMovimiento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.R;



public class TipoMovimientoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener  {

    EditText editId, editNombre;
    SearchView searchConsultar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_movimiento_consultar);
        editId=(EditText)findViewById(R.id.TipoMovimientoConsultarId);
        editNombre=(EditText)findViewById(R.id.TipoMovimientoConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.TipoMovimiento_searchConsultar);
        searchConsultar.setOnQueryTextListener(this);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasM helper= TablasM.getInstance(getApplicationContext());
        TipoMovimiento tipoMovimiento= new TipoMovimiento();
        helper.open();
        tipoMovimiento=helper.tipoMovimientoConsultar(newText.toUpperCase());
        helper.close();
        if (tipoMovimiento==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(tipoMovimiento.getIdTipoMovimiento()));
            editNombre.setText(tipoMovimiento.getNombreTipoMovimiento());
        }
        return false;
    }

    public void buscarEditarTipoMovimiento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, TipoMovimientoEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }


    public void buscarEliminarTipoMovimiento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            TipoMovimiento tipoMovimiento= new TipoMovimiento();
            tipoMovimiento.setIdTipoMovimiento(Integer.valueOf(editId.getText().toString()));
            tipoMovimiento.setNombreTipoMovimiento(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoMovimiento(tipoMovimiento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoMovimientoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoMovimiento.getIdTipoMovimiento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_movimiento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_movimiento_menu_insertar:
                Intent intent0= new Intent(this, TipoMovimientoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_movimiento_menu_consultar:
                Intent intent1= new Intent(this, TipoMovimientoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_movimiento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_movimiento_menu_Editar:
                Intent intentEd= new Intent(this, TipoMovimientoEditarActivity.class);
                this.startActivity(intentEd);
                return true;


            case R.id.action_tipo_movimiento_menu_eliminar:
                Intent intent2= new Intent(this, TipoMovimientoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_movimiento_listar:
                Intent intent3= new Intent(this, TipoMovimientoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
