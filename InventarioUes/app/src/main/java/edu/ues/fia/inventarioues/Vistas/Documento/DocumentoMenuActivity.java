package edu.ues.fia.inventarioues.Vistas.Documento;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.DocumentoAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_menu);
        ListView listView =(ListView)findViewById(R.id.listDocumento);
        List<Documento> lista= listadoDocumentos();
        DocumentoAdaptador documentoAdaptador=new DocumentoAdaptador(this,lista);
        listView.setAdapter(documentoAdaptador);
    }


    public List<Documento> listadoDocumentos(){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor c =helper.obtenerDocumentos();
        List<Documento> documentoList= new ArrayList<>();
        while (c.moveToNext()){
            Documento documento=new Documento();
            //camposMarca= new String[]{"idMarca", "nombreMarca"}
            documento.setIdDocumento(c.getInt(0));
            documento.setIsbnDocumento(c.getString(1));
            documento.setIdTipoDocumento(c.getInt(2));
            documento.setIdEditorial(c.getInt(3));
            documento.setCodIdioma(c.getInt(4));
            documento.setNombreDocumento(c.getString(5));
            documento.setEdicionDocumento(c.getInt(6));
            documento.setPublicacionDocumento(c.getString(7));

            documentoList.add(documento);

            //POSIBLE ERROR
        }
        return documentoList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0= new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1= new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente= new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2= new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3= new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
