package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;

public class EquipoEditarActivity extends AppCompatActivity {

    EditText editSerial, editDescripcion, editFechaIngreso, editFechaInactividad, editBusca;
    Button btnBuscar;
    String clave=null;
    Calendar calendar=Calendar.getInstance();
    Spinner spMarca, spTipo;
    Integer valorSelectMarca, valorSelectTipo, valorSelectOpcion=null, prestado, opcion;
    RadioButton opcion1, opcion2;
    Date fecha1=calendar.getTime();
    RadioGroup grupo;
    final List<Integer> marcaIdList = new ArrayList<>();
    final List<Integer> tipoIdList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_editar);
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        editSerial=(EditText)findViewById(R.id.EquipoEditarSerial);
        spMarca=(Spinner) findViewById(R.id.EquipoEditarMarcaId);
        spTipo=(Spinner) findViewById(R.id.EquipoEditarTipoId);
        editDescripcion=(EditText)findViewById(R.id.EquipoEditarDescripcion);
        grupo=(RadioGroup)findViewById(R.id.EquipoEditarEstadoGrupo);
        opcion1=(RadioButton)findViewById(R.id.EquipoEditarEstadoOpcion1);
        opcion2=(RadioButton)findViewById(R.id.EquipoEditarEstadoOpcion2);
        editFechaIngreso=(EditText)findViewById(R.id.EquipoEditarFechaIngreso);
        editFechaInactividad=(EditText)findViewById(R.id.EquipoEditarFechaInactividad);
        editBusca=(EditText)findViewById(R.id.EquipoEditarSearch);
        btnBuscar=(Button)findViewById(R.id.EquipoEditarBtnConsultar);

        Bundle bundle=getIntent().getExtras();

        Cursor marca=helper.obtenerMarcas();

        marcaIdList.add(0);
        List<String> marcaNombreList = new ArrayList<>();
        marcaNombreList.add(getResources().getString(R.string.equipoCampo2s));
        while (marca.moveToNext()){
            marcaIdList.add(marca.getInt(0));
            marcaNombreList.add(marca.getString(1));
        }
        spMarca.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,marcaIdList));
        spMarca.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,marcaNombreList));
        spMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMarca=null;
                }else {
                    valorSelectMarca=marcaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMarca=null;

            }
        });

        Cursor tipo=helper.obtenerTiposEquipo();

        tipoIdList.add(0);
        List<String> tipoNombreList = new ArrayList<>();
        tipoNombreList.add(getResources().getString(R.string.equipoCampo3s));
        while (tipo.moveToNext()){
            tipoIdList.add(tipo.getInt(0));
            tipoNombreList.add(tipo.getString(1));
        }
        spTipo.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_dropdown_item_1line,tipoIdList));
        spTipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,tipoNombreList));
        spTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipo=null;
                }else {
                    valorSelectTipo=tipoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipo=null;

            }
        });

        opcion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean marcado =((RadioButton) v).isChecked();
                if (marcado){
                    valorSelectOpcion=1;
                }
            }
        });

        opcion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean marcado =((RadioButton) v).isChecked();
                if (marcado){
                    valorSelectOpcion=0;
                }
            }
        });


        editFechaIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EquipoEditarActivity.this,fechaIngreso, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        editFechaInactividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EquipoEditarActivity.this,fechaInactiva, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        if (bundle==null){
            editSerial.setEnabled(false);
            spMarca.setEnabled(false);
            spTipo.setEnabled(false);
            editDescripcion.setEnabled(false);
            opcion1.setEnabled(false);
            opcion2.setEnabled(false);
            editFechaIngreso.setEnabled(false);
            editFechaInactividad.setEnabled(false);
            opcion=0;
        }else{
            editBusca.setVisibility(View.INVISIBLE);
            btnBuscar.setVisibility(View.INVISIBLE);
            int posicionEnListaMarca = marcaIdList.indexOf(bundle.getInt("marca"));
            int posicionEnListaTipo = tipoIdList.indexOf(bundle.getInt("tipo"));
            opcion=1;
            editSerial.setText(bundle.getString("serial"));
            spMarca.setSelection(posicionEnListaMarca);
            spTipo.setSelection(posicionEnListaTipo);
            editDescripcion.setText(bundle.getString("descripcion"));
            editFechaIngreso.setText(bundle.getString("ingreso"));
            editFechaInactividad.setText(bundle.getString("inactividad"));
            clave=bundle.getString("serial");
            if (bundle.getInt("estado")==1){
                opcion1.setChecked(true);
                valorSelectOpcion=1;
            }else {
                opcion2.setChecked(true);
                valorSelectOpcion=0;
            }
            prestado=bundle.getInt("prestamo");
        }


    }

    DatePickerDialog.OnDateSetListener fechaIngreso = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            fecha1=calendar.getTime();
            actualizarFechaIngreso();
        }
    };

    private void actualizarFechaIngreso(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
        editFechaIngreso.setText(format.format(calendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener fechaInactiva = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            Date fecha2=calendar.getTime();
            if(fecha1.compareTo(fecha2)<=0){
                actualizarFechaInactiva();
            }
            actualizarFechaInactiva();
        }
    };

    private void actualizarFechaInactiva(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.US);
        editFechaInactividad.setText(format.format(calendar.getTime()));
    }


    public void buscaEquipoEditar(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Equipo equipo=helper.equipoConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (equipo==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {

            editSerial.setEnabled(true);
            spMarca.setEnabled(true);
            spTipo.setEnabled(true);
            editDescripcion.setEnabled(true);
            opcion1.setEnabled(true);
            opcion2.setEnabled(true);
            editFechaIngreso.setEnabled(true);
            editFechaInactividad.setEnabled(true);

            editSerial.setText(equipo.getSerialEquipo());
            int posicionEnListaMarca = marcaIdList.indexOf(equipo.getIdMarca());
            int posicionEnListaTipo = tipoIdList.indexOf(equipo.getIdTipoEquipo());
            spMarca.setSelection(posicionEnListaMarca);
            spTipo.setSelection(posicionEnListaTipo);
            editDescripcion.setText(equipo.getDescripcionEquipo());
            if (equipo.getEstadoEquipo()==1){
                opcion1.setChecked(true);
                valorSelectOpcion=1;
            }else {
                opcion2.setChecked(true);
                valorSelectOpcion=0;
            }
            editFechaIngreso.setText(equipo.getFechaEquipo());
            editFechaInactividad.setText(equipo.getFechaInactivoEquipo());
            prestado = equipo.getPrestamo();

            clave=editBusca.getText().toString().toUpperCase();



            editBusca.setEnabled(false);
            btnBuscar.setEnabled(false);
        }
    }

    public void actualizarEquipo(View v){
        String serial,descripcion, ingreso, inactividad, mensaje;
        Integer marca, tipo, estado;
        serial=editSerial.getText().toString().toUpperCase();
        marca=valorSelectMarca;
        tipo=valorSelectTipo;
        descripcion=editDescripcion.getText().toString().toUpperCase();
        estado=valorSelectOpcion;
        ingreso=editFechaIngreso.getText().toString();
        inactividad=editFechaInactividad.getText().toString();
        if (serial.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper= TablasJ.getInstance(getApplicationContext());
            Equipo equipo = new Equipo();
            equipo.setSerialEquipo(serial);
            equipo.setIdMarca(marca);
            equipo.setIdTipoEquipo(tipo);
            equipo.setDescripcionEquipo(descripcion);
            equipo.setEstadoEquipo(estado);
            equipo.setFechaEquipo(ingreso);
            equipo.setFechaInactivoEquipo(inactividad);
            equipo.setPrestamo(prestado);
            helper.open();
            mensaje=helper.actualizarEquipo(equipo,clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,EquipoMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editSerial.setEnabled(false);
                editSerial.setText("");
                spMarca.setEnabled(false);
                spMarca.setSelection(0);
                spTipo.setEnabled(false);
                spTipo.setSelection(0);
                editDescripcion.setEnabled(false);
                editDescripcion.setText("");
                opcion1.setEnabled(false);
                opcion2.setEnabled(false);
                grupo.clearCheck();
                editFechaIngreso.setEnabled(false);
                editFechaIngreso.setText("");
                editFechaInactividad.setEnabled(false);
                editFechaInactividad.setText("");


                editBusca.setText("");
                clave="";
                editBusca.setEnabled(true);
                btnBuscar.setEnabled(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_equipo_menu_insertar:
                Intent intent0= new Intent(this, EquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_equipo_menu_consultar:
                Intent intent1= new Intent(this, EquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_equipo_menu_Editar:
                Intent intente= new Intent(this, EquipoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_equipo_menu_eliminar:
                Intent intent2= new Intent(this, EquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_equipo_listar:
                Intent intent3= new Intent(this, EquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
