package edu.ues.fia.inventarioues.Vistas.Documento;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    SearchView searchDocumento;
    TextView txtEstado, txtAutores;
    int valorPrestamo,valorSelectTipoDocumento, valorSelectEditorial, valorSelectIdioma;
    EditText editIdDocumento, editIsbn, editNombre, editEdicion,editPublicacionDocumento,editTipoDocumento,
    editEditorial, editIdioma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_consultar);

        searchDocumento = findViewById(R.id.DocumentoConsultarSearch);
        searchDocumento.setOnQueryTextListener(this);


        txtEstado = findViewById(R.id.DocumentoConsultarPrestamo);

        editIdDocumento =findViewById(R.id.DocumentoConsultarIdDocumento);
        editIsbn = findViewById(R.id.DocumentoConsultarIsbn);
        editNombre = findViewById(R.id.DocumentoConsultarNombre);
        editEdicion = findViewById(R.id.DocumentoConsultarEdicion);
        editPublicacionDocumento = findViewById(R.id.DocumentoConsultarPublicacion);
        editTipoDocumento = findViewById(R.id.DocumentoConsultarTipoDocumento);
        editEditorial = findViewById(R.id.DocumentoConsultarEditorial);
        editIdioma = findViewById(R.id.DocumentoConsultarIdioma);
        txtAutores = findViewById(R.id.DocumentoConsultarAutores);

    }


    @Override
    public boolean onQueryTextChange(String newText) {
        TablasV helper= TablasV.getInstance(getApplicationContext());
        Documento documento = new Documento();
        helper.open();
        try{
            documento=helper.documentoConsultar(Integer.parseInt(newText));
        }catch (Exception e){e.printStackTrace();}


        helper.close();
        if (documento==null){

            editIdDocumento.setText("");
            editIsbn.setText("");
            editTipoDocumento.setText("");

            editEditorial.setText("");
            editIdioma.setText("");
            editNombre.setText("");
            editEdicion.setText("");
            editPublicacionDocumento.setText("");

            txtEstado.setText(getResources().getString(R.string.documentoCampo10));
            txtEstado.setTextColor(Color.rgb(164,164,164));
        }else {
            if (documento.getPrestadoDocumento()==1){
                txtEstado.setText(getResources().getString(R.string.documentoCampo10e1));
                txtEstado.setTextColor(Color.rgb(100,221,23));
                valorPrestamo=1;
            }else {
                txtEstado.setText(getResources().getString(R.string.documentoCampo10e2));
                txtEstado.setTextColor(Color.rgb(213,0,0));
                valorPrestamo=0;
            }

            editIdDocumento.setText(String.valueOf(documento.getIdDocumento()));

            Cursor tipoDocumento = helper.obtenerTipoDocumentos();
            while (tipoDocumento.moveToNext() && editTipoDocumento.getText().toString().isEmpty()){
                if (tipoDocumento.getInt(0)==documento.getIdTipoDocumento()){
                    editTipoDocumento.setText(tipoDocumento.getString(1));
                    valorSelectTipoDocumento=tipoDocumento.getInt(0);
                }
            }
            Cursor editorial=helper.obtenerEditoriales();
            while (editorial.moveToNext() && editEditorial.getText().toString().isEmpty()){
                if (editorial.getInt(0)==documento.getIdEditorial()){
                    editEditorial.setText(editorial.getString(1));
                    valorSelectEditorial=editorial.getInt(0);
                }
            }

            Cursor idioma=helper.obtenerIdiomas();
            while (idioma.moveToNext() && editIdioma.getText().toString().isEmpty()){
                if (idioma.getInt(0)==documento.getCodIdioma()){
                    editIdioma.setText(idioma.getString(1));
                    valorSelectIdioma=idioma.getInt(0);
                }
            }

            /*
            if (equipo.getEstadoEquipo()==1){
                editEstado.setText("Funcional");
                valorSelectOpcion=1;
            }else {
                editEstado.setText("Arruinado");
                valorSelectOpcion=0;
            }*/

            //editDescripcion.setText(equipo.getDescripcionEquipo());
            editPublicacionDocumento.setText(documento.getPublicacionDocumento());
            editNombre.setText(documento.getNombreDocumento());
            editEdicion.setText(String.valueOf(documento.getEdicionDocumento()));
            editIsbn.setText(documento.getIsbnDocumento());
            txtAutores.setText(documento.getAutoresDocumento());

        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void equipoConsultarEdicion(View v){
        if(editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            /*
              editIdDocumento.setText(bundle.getString("idDocumento"));
            editIsbnDocumento.setText(bundle.getString("isbn"));
            editNombreDocumento.setText(bundle.getString("nombre"));
            editEdicionDocumento.setText(bundle.getString("edicion"));
            editFechaPublicacion.setText(bundle.getString("publicacion"));

            spTipoDocumento.setSelection(bundle.getInt("idTipoAutor"));
            spEditorial.setSelection(bundle.getInt("editorial"));
            spIdioma.setSelection(bundle.getInt("codIdioma"));
            txtAutores.setText(bundle.getString("autores"));

            */

            String idDocumento,isbnDocumento,NombreDocumento,EdicionDocumento,FechaPublicacion;

            idDocumento = editIdDocumento.getText().toString();
            isbnDocumento = editIsbn.getText().toString();
            NombreDocumento = editNombre.getText().toString().toUpperCase().replaceFirst("\\s++$", "");
            EdicionDocumento = editEdicion.getText().toString();
            FechaPublicacion = editPublicacionDocumento.getText().toString();

            Intent intentEditar= new Intent(this, DocumentoEditarActivity.class);

            intentEditar.putExtra("idDocumento",idDocumento);
            intentEditar.putExtra("isbn",isbnDocumento);
            intentEditar.putExtra("nombre",NombreDocumento);
            intentEditar.putExtra("edicion",EdicionDocumento);
            intentEditar.putExtra("publicacion",FechaPublicacion);
            intentEditar.putExtra("idTipoDocumento",valorSelectTipoDocumento);
            intentEditar.putExtra("editorial",valorSelectEditorial);
            intentEditar.putExtra("codIdioma",valorSelectIdioma);
            intentEditar.putExtra("autores",txtAutores.getText().toString().toUpperCase());



            this.startActivity(intentEditar);
        }
    }


    public void buscarEliminarDocumento(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Documento documento= new Documento();
            documento.setIdDocumento(Integer.parseInt(editIdDocumento.getText().toString()));
            String mensaje= helper.eliminar1Documento(documento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, DocumentoEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", documento.getIdDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_documento_menu_principal:
                Intent intentP = new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0 = new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1 = new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente = new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2 = new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3 = new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
