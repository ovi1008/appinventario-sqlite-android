package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;

public class IdiomaBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    EditText editId, editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_buscar);
        editId=(EditText)findViewById(R.id.IdiomaConsultarId);
        editNombre=(EditText)findViewById(R.id.IdiomaConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.Idioma_searchConsultar);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasL helper= TablasL.getInstance(getApplicationContext());
        Idioma idioma= new Idioma();
        helper.open();
        idioma=helper.idiomaConsultar(newText.toUpperCase());
        helper.close();
        if (idioma==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(idioma.getCodIdioma()));
            editNombre.setText(idioma.getNombreIdioma());
        }
        return false;
    }
    public void buscarEditarIdioma(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, IdiomaEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarIdioma(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasL helper = TablasL.getInstance(getApplicationContext());
            helper.open();
            Idioma idioma= new Idioma();
            idioma.setCodIdioma(Integer.valueOf(editId.getText().toString()));
            idioma.setNombreIdioma(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Idioma(idioma);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, IdiomaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", idioma.getNombreIdioma());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
