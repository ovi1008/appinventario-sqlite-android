package edu.ues.fia.inventarioues.Modelos;

public class UnidadAdministrativa {
    int idUnidadAdministrativa;
    String nombreUnidadAdministrativa;

    public UnidadAdministrativa() {
    }

    public int getIdUnidadAdministrativa() {
        return idUnidadAdministrativa;
    }

    public void setIdUnidadAdministrativa(int idUnidadAdministrativa) {
        this.idUnidadAdministrativa = idUnidadAdministrativa;
    }

    public String getNombreUnidadAdministrativa() {
        return nombreUnidadAdministrativa;
    }

    public void setNombreUnidadAdministrativa(String nombreUnidadAdministrativa) {
        this.nombreUnidadAdministrativa = nombreUnidadAdministrativa;
    }
}
