package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import edu.ues.fia.inventarioues.MainActivity;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaMenuActivity;

public class PrestamoActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    String [] menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo);
        menu=getResources().getStringArray(R.array.menuP);

        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, menu);
        ListView listView = findViewById(R.id.listPrestamo);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                Intent intentPrestamoDocumento = new Intent(this, PrestamoMenuActivity.class);
                intentPrestamoDocumento.putExtra("vista",1);
                this.startActivity(intentPrestamoDocumento);
                break;

            case 1:
                Intent intentPrestamoEquipo = new Intent(this, PrestamoMenuActivity.class);
                intentPrestamoEquipo.putExtra("vista",2);
                this.startActivity(intentPrestamoEquipo);
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_prestamo000,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_prestamo001:
                Intent intentMeP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentMeP);
        }
        return super.onOptionsItemSelected(item);
    }


}
