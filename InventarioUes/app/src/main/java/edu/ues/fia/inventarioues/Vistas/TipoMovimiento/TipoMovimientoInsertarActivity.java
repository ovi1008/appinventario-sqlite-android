package edu.ues.fia.inventarioues.Vistas.TipoMovimiento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.R;

public class TipoMovimientoInsertarActivity extends AppCompatActivity {

    EditText editNombreTipoMovimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_movimiento_insertar);
        editNombreTipoMovimiento=(EditText)findViewById(R.id.TipoMovimientoInsertarNombre);
    }


    public void insertarTipoMovimiento(View v){
        String nombre, mensaje;
        TablasM helper= TablasM.getInstance(getApplicationContext());
        nombre=editNombreTipoMovimiento.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.tipoMovimientoMsj1), Toast.LENGTH_LONG).show();
        }else{
            TipoMovimiento tipoMovimiento= new TipoMovimiento();
            tipoMovimiento.setNombreTipoMovimiento(nombre);
            helper.open();
            mensaje=helper.insertarTipoMovimiento(tipoMovimiento);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreTipoMovimiento.setText("");
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_movimiento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_movimiento_menu_insertar:
                Intent intent0= new Intent(this, TipoMovimientoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_movimiento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_movimiento_menu_Editar:
                Intent intentEd= new Intent(this, TipoMovimientoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_movimiento_menu_consultar:
                Intent intent1= new Intent(this, TipoMovimientoConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_tipo_movimiento_menu_eliminar:
                Intent intent2= new Intent(this, TipoMovimientoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_movimiento_listar:
                Intent intent3= new Intent(this, TipoMovimientoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
