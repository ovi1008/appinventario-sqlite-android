package edu.ues.fia.inventarioues.Vistas.Marca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

public class MarcaEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca;
    String clave="";
    int opcion=0;
    Button busca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_editar);
        editId = (EditText) findViewById(R.id.MarcaEditarId);
        editNombre = (EditText) findViewById(R.id.MarcaEditarNombre);
        editBusca=(EditText) findViewById(R.id.MarcaEditarSearch);
        busca=(Button)findViewById(R.id.MarcaEditarBtnConsultar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            editBusca.setVisibility(View.INVISIBLE);
            busca.setVisibility(View.INVISIBLE);

        }
    }

    public void buscaMarcaEditar(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Marca marca=helper.marcaConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (marca==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(marca.getIdMarca()));
            editNombre.setText(marca.getNombreMarca());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    public void actualizarMarca(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper= TablasJ.getInstance(getApplicationContext());
            Marca marca = new Marca();
            marca.setIdMarca(Integer.valueOf(editId.getText().toString()));
            marca.setNombreMarca(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarMarca(marca,clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,MarcaMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);
            }
        }


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
