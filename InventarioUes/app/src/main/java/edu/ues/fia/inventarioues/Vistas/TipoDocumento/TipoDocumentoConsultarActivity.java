package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

public class TipoDocumentoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    EditText editId, editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_consultar);
        editId=(EditText)findViewById(R.id.TipoDocumentoConsultarId);
        editNombre=(EditText)findViewById(R.id.TipoDocumentoConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.TipoDocumento_searchConsultar);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasM helper= TablasM.getInstance(getApplicationContext());
        TipoDocumento tipoDocumento= new TipoDocumento();
        helper.open();
        tipoDocumento=helper.tipoDocumentoConsultar(newText.toUpperCase());
        helper.close();
        if (tipoDocumento==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(tipoDocumento.getIdTipoDocumento()));
            editNombre.setText(tipoDocumento.getNombreTipoDocumento());
        }
        return false;
    }

    public void buscarEditarTipoDocumento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, TipoDocumentoEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarTipoDocumento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            TipoDocumento tipoDocumento= new TipoDocumento();
            tipoDocumento.setIdTipoDocumento(Integer.valueOf(editId.getText().toString()));
            tipoDocumento.setNombreTipoDocumento(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoDocumento(tipoDocumento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoDocumentoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoDocumento.getNombreTipoDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}