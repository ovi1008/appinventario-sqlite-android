package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import edu.ues.fia.inventarioues.Adaptador.EditorialAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;


import edu.ues.fia.inventarioues.R;


public class EditorialMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_menu);
        ListView listView =(ListView)findViewById(R.id.listEditorial);
        List<Editorial> lista= listadoEditoriales();
        EditorialAdaptador editorialAdaptador=new EditorialAdaptador(this,lista);
        listView.setAdapter(editorialAdaptador);
    }


    //    private  static final String [] camposEditorial = new String[]{"idEditorial",
//    "nombreEditorial"};
    public List<Editorial> listadoEditoriales(){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor c =helper.obtenerEditoriales();
        List<Editorial> editorialList= new ArrayList<>();
        while (c.moveToNext()){
            Editorial editorial=new Editorial();

            editorial.setIdEditorial(c.getInt(0));
            editorial.setNombreEditorial(c.getString(1));
            editorialList.add(editorial);
        }
        return editorialList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;



            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
