package edu.ues.fia.inventarioues.Vistas.Documento;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoInsertarActivity extends AppCompatActivity {

    EditText editIsbn, editNombreDocumento, editFechaPublicacion, editEdicionDocumento;
    Calendar calendar = Calendar.getInstance();
    Spinner spTipoDocumento, spEditorial, spIdioma;
    Integer valorSelectTipoDocumento, valorSelectEditorial, valorSelectIdioma, valorSelectOpcion = null;
    TextView txtAutores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_insertar);
        TablasV helper = TablasV.getInstance(getApplicationContext());

        editIsbn = findViewById(R.id.DocumentoInsertarIsbn);
        editNombreDocumento = findViewById(R.id.DocumentoInsertarNombreDocumento);
        editFechaPublicacion = findViewById(R.id.DocumentoInsertarPublicacionDocumento);
        editEdicionDocumento = findViewById(R.id.DocumentoInsertarEdicionDocumento);
        spTipoDocumento = findViewById(R.id.DocumentoInsertarTipoDocumento);
        spEditorial = findViewById(R.id.DocumentoInsertarEditorial);
        spIdioma = findViewById(R.id.DocumentoInsertarIdioma);
        txtAutores=findViewById(R.id.DocumentoInsertarAutores);


        Cursor tipoDocumento = helper.obtenerTipoDocumentos();
        final List<Integer> tipoDocumentoIdList = new ArrayList<>();
        tipoDocumentoIdList.add(0);
        List<String> tipoDocumentoNombreList = new ArrayList<>();
        tipoDocumentoNombreList.add(getResources().getString(R.string.documentoCampo3s));
        while (tipoDocumento.moveToNext()) {
            tipoDocumentoIdList.add(tipoDocumento.getInt(0));
            tipoDocumentoNombreList.add(tipoDocumento.getString(1));
        }
        spTipoDocumento.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, tipoDocumentoIdList));
        spTipoDocumento.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, tipoDocumentoNombreList));
        spTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectTipoDocumento = null;
                } else {
                    valorSelectTipoDocumento = tipoDocumentoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoDocumento = null;

            }
        });

        Cursor editorial = helper.obtenerEditoriales();
        final List<Integer> editorialIdList = new ArrayList<>();
        editorialIdList.add(0);
        List<String> editorialNombreList = new ArrayList<>();
        editorialNombreList.add(getResources().getString(R.string.documentoCampo4s));
        while (editorial.moveToNext()) {
            editorialIdList.add(editorial.getInt(0));
            editorialNombreList.add(editorial.getString(1));
        }
        spEditorial.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, editorialIdList));
        spEditorial.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, editorialNombreList));
        spEditorial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectEditorial = null;
                } else {
                    valorSelectEditorial= editorialIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectEditorial = null;

            }
        });
        Cursor idioma = helper.obtenerIdiomas();
        final List<Integer> idiomaIdList = new ArrayList<>();
        idiomaIdList.add(0);
        List<String> idiomaNombreList = new ArrayList<>();
        idiomaNombreList.add(getResources().getString(R.string.documentoCampo5s));
        while (idioma.moveToNext()) {
            idiomaIdList.add(idioma.getInt(0));
            idiomaNombreList.add(idioma.getString(1));
        }
        spIdioma.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, idiomaIdList));
        spIdioma.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, idiomaNombreList));
        spIdioma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectIdioma = null;
                } else {
                    valorSelectIdioma= idiomaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectIdioma = null;

            }
        });


        editFechaPublicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DocumentoInsertarActivity.this,fechaPublicacion, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }



    DatePickerDialog.OnDateSetListener fechaPublicacion = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            actualizarFechaPublicacion();
        }
    };

    private void actualizarFechaPublicacion(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
        editFechaPublicacion.setText(format.format(calendar.getTime()));
    }


    public void autoresDocumento(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(DocumentoInsertarActivity.this);
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor cAutor=helper.obtenerAutores();
        String[] autores = new String[cAutor.getCount()];
        final boolean[] checkedAutores = new boolean[cAutor.getCount()];
        Integer contador=0;
        while (cAutor.moveToNext()){
            autores[contador]=cAutor.getString(2)+" "+cAutor.getString(3);
            checkedAutores[contador]=false;
            contador++;
        }
        final List<String>autoresList= Arrays.asList(autores);
        builder.setMultiChoiceItems(autores, checkedAutores, new DialogInterface.OnMultiChoiceClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedAutores[which] = isChecked;

                // Get the current focused item
                String currentItem = autoresList.get(which);

                // Notify the current action
                Toast.makeText(getApplicationContext(),
                        currentItem, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle(getResources().getString(R.string.documentoCampo9d));

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                for (int i = 0; i<checkedAutores.length; i++){
                    boolean checked = checkedAutores[i];
                    if (checked) {
                        txtAutores.setText(txtAutores.getText() + autoresList.get(i) + "\n");
                    }
                }
            }
        });



        // Set the neutral/cancel button click listener
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the neutral button
                txtAutores.setText("");
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    public void DocumentoIngresar(View v){
        String isbnDocumento,nombreDocumento,publicacionDocumento, mensaje, autores;
        Integer idTipoDocumento, idEditorial, idCodIdioma,edicionDocumento,prestadoDocumento;

        isbnDocumento=editIsbn.getText().toString().toUpperCase();
        idTipoDocumento = valorSelectTipoDocumento;
        idEditorial = valorSelectEditorial;
        idCodIdioma = valorSelectIdioma;
        if (editEdicionDocumento.getText().toString().isEmpty()){
            edicionDocumento=null;
        }else {
            edicionDocumento =Integer.valueOf(editEdicionDocumento.getText().toString());
        }
        publicacionDocumento =editFechaPublicacion.getText().toString();
        autores=txtAutores.getText().toString().toUpperCase();
        nombreDocumento =editNombreDocumento.getText().toString().toUpperCase().replaceFirst("\\s++$", "");


        if (isbnDocumento.isEmpty()|| !(idTipoDocumento instanceof Integer) || !(idEditorial instanceof Integer) || autores.isEmpty()||
                !(idCodIdioma instanceof Integer)  ||!(edicionDocumento instanceof Integer)||nombreDocumento.isEmpty()){

            Toast.makeText(this, getResources().getString(R.string.documentoMsj1), Toast.LENGTH_LONG).show();
        }else {
            TablasV helper = TablasV.getInstance(getApplicationContext());
            Documento documento = new Documento();

            documento.setCodIdioma(idCodIdioma);
            documento.setEdicionDocumento(edicionDocumento);
            documento.setIdEditorial(idEditorial);
            documento.setIdTipoDocumento(idTipoDocumento);
            documento.setIsbnDocumento(isbnDocumento);
            documento.setNombreDocumento(nombreDocumento);
            documento.setPrestadoDocumento(1);
            documento.setPublicacionDocumento(publicacionDocumento);
            documento.setAutoresDocumento(autores);



            //equipo.setPrestamo(1);

            helper.open();
            mensaje=helper.insertarDocumento(documento);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

            editNombreDocumento.setText("");
            editIsbn.setText("");
            spIdioma.setSelection(0);
            spEditorial.setSelection(0);
            spTipoDocumento.setSelection(0);
            editEdicionDocumento.setText("");
            editFechaPublicacion.setText("");
            txtAutores.setText("");

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_documento_menu_principal:
                Intent intentP = new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0 = new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1 = new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente = new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2 = new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3 = new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
