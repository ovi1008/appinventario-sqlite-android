package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

public class PrestamoEditarActivity extends AppCompatActivity {
    Integer vista;
    Spinner spTipoMov, spDocente, spMateria, spMotivo, spHorario, spUnidad, spEquipo, spDocumento;
    Integer valorSelectTipoMov, valorSelectMotivo, valorSelectHorario=null, valorSelectUnidad=null,
            valorSelectDocumento=null, opcion, clave=null;
    String valorSelectDocente, valorSelectMateria=null, valorSelectEquipo=null, nombreAdaptativo;
    Button btnBuscar;
    EditText editBuscar, editId;
    final List<Integer> tipoMovIdList = new ArrayList<>();
    final List<String> docenteIdList = new ArrayList<>();
    final List<String> materiaIdList = new ArrayList<>();
    final List<Integer> motivoIdList = new ArrayList<>();
    final List<Integer> horarioIdList = new ArrayList<>();
    final List<Integer> unidadIdList = new ArrayList<>();
    final List<Integer> documentoIdList = new ArrayList<>();
    final List<String> equipoIdList = new ArrayList<>();
    final List<String> equipoNombreList = new ArrayList<>();
    final List<String> documentoNombreList = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_editar);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");
        if (vista==1){
            nombreAdaptativo="DOCUMENTO";
            TextView titulo=findViewById(R.id.PrestamoEditarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud3P1));
        }else {
            nombreAdaptativo="EQUIPO";
            TextView titulo=findViewById(R.id.PrestamoEditarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud3P2));
        }
        spTipoMov=findViewById(R.id.PrestamoEditarTipoMovimiento);
        spDocente=findViewById(R.id.PrestamoEditarDocente);
        spMateria=findViewById(R.id.PrestamoEditarMateria);
        spMotivo=findViewById(R.id.PrestamoEditarMotivo);
        spHorario=findViewById(R.id.PrestamoEditarHorario);
        spUnidad=findViewById(R.id.PrestamoEditarUnidad);
        spEquipo=findViewById(R.id.PrestamoEditarEquipo);
        spDocumento=findViewById(R.id.PrestamoEditarDocumento);
        btnBuscar=findViewById(R.id.PrestamoEditarBtnConsultar);
        editBuscar=findViewById(R.id.PrestamoEditarSearch);
        editId=findViewById(R.id.PrestamoEditarId);
        editId.setText("");
        editBuscar.setText("");

        TablasJ helperJ= TablasJ.getInstance(getApplicationContext());
        TablasV helperV= TablasV.getInstance(getApplicationContext());
        TablasM helperM= TablasM.getInstance(getApplicationContext());
        TablasL helperL= TablasL.getInstance(getApplicationContext());
        helperJ.open();

        Cursor tipoMov=helperM.obtenerTiposMovimiento();
        tipoMovIdList.add(0);
        List<String> tipoMovNombreList = new ArrayList<>();
        tipoMovNombreList.add(getResources().getString(R.string.prestamoCampo2S));
        while (tipoMov.moveToNext()){
            tipoMovIdList.add(tipoMov.getInt(0));
            tipoMovNombreList.add(tipoMov.getString(1));
        }
        spTipoMov.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,tipoMovIdList));
        spTipoMov.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,tipoMovNombreList));
        spTipoMov.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipoMov=null;
                }else {
                    valorSelectTipoMov=tipoMovIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoMov=null;

            }
        });

        Cursor docente=helperM.obtenerDocentes();

        docenteIdList.add("0");
        List<String> docenteNombreList = new ArrayList<>();
        docenteNombreList.add(getResources().getString(R.string.prestamoCampo3s));
        while (docente.moveToNext()){
            docenteIdList.add(docente.getString(0));
            docenteNombreList.add(docente.getString(1)+" "+docente.getString(2));
        }
        spDocente.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,docenteIdList));
        spDocente.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,docenteNombreList));
        spDocente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectDocente=null;
                }else {
                    valorSelectDocente=docenteIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectDocente=null;

            }
        });

        Cursor materia=helperL.obtenerMaterias();

        materiaIdList.add("0");
        List<String> materiaNombreList = new ArrayList<>();
        materiaNombreList.add(getResources().getString(R.string.prestamoCampo4s));
        while (materia.moveToNext()){
            materiaIdList.add(materia.getString(0));
            materiaNombreList.add(materia.getString(1));
        }
        spMateria.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,materiaIdList));
        spMateria.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,materiaNombreList));
        spMateria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMateria=null;
                }else {
                    valorSelectMateria=materiaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMateria=null;

            }
        });

        Cursor motivo=helperM.obtenerMotivos();

        motivoIdList.add(0);
        List<String> motivoNombreList = new ArrayList<>();
        motivoNombreList.add(getResources().getString(R.string.prestamoCampo5s));
        while (motivo.moveToNext()){
            motivoIdList.add(motivo.getInt(0));
            motivoNombreList.add(motivo.getString(1));
        }
        spMotivo.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,motivoIdList));
        spMotivo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,motivoNombreList));
        spMotivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMotivo=null;
                }else {
                    valorSelectMotivo=motivoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMotivo=null;

            }
        });

        Cursor horario=helperM.obtenerHorarios();

        horarioIdList.add(0);
        List<String> horarioNombreList = new ArrayList<>();
        horarioNombreList.add(getResources().getString(R.string.prestamoCampo6s));
        while (horario.moveToNext()){
            horarioIdList.add(horario.getInt(0));
            horarioNombreList.add(horario.getString(1));
        }
        spHorario.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,horarioIdList));
        spHorario.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,horarioNombreList));
        spHorario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectHorario=null;
                }else {
                    valorSelectHorario=horarioIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectHorario=null;

            }
        });

        Cursor unidad=helperL.obtenerUnidadesAdministrativas();
        unidadIdList.add(0);
        List<String> unidadNombreList = new ArrayList<>();
        unidadNombreList.add(getResources().getString(R.string.prestamoCampo7s));
        while (unidad.moveToNext()){
            unidadIdList.add(unidad.getInt(0));
            unidadNombreList.add(unidad.getString(1));
        }
        spUnidad.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,unidadIdList));
        spUnidad.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,unidadNombreList));
        spUnidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectUnidad=null;
                }else {
                    valorSelectUnidad=unidadIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectUnidad=null;

            }
        });

        Cursor documento= helperJ.obtenerDocumentoDisponibles();

        documentoIdList.add(0);
        documentoNombreList.add(getResources().getString(R.string.prestamoCampo8s));
        while (documento.moveToNext()){
            documentoIdList.add(documento.getInt(0));
            documentoNombreList.add(documento.getString(0)+"-"+documento.getString(5));
        }
        spDocumento.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,documentoIdList));
        spDocumento.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,documentoNombreList));
        spDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectDocumento=null;
                }else {
                    valorSelectDocumento=documentoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectDocumento=null;

            }
        });

        Cursor equipo=helperJ.obtenerEquipoDisponibles();
        equipoIdList.add("0");
        final List<Integer> tipoIdList = new ArrayList<>();
        final List<String> tipoNombreList = new ArrayList<>();

        Cursor tipo=helperJ.obtenerTiposEquipo();
        while (tipo.moveToNext()){
            tipoIdList.add(tipo.getInt(0));
            tipoNombreList.add(tipo.getString(1));
        }

        equipoNombreList.add(getResources().getString(R.string.prestamoCampo9s));
        while (equipo.moveToNext()){
            String nombreT;
            Integer lugar=0;
            equipoIdList.add(equipo.getString(0));
            for (int t:tipoIdList){
                if (equipo.getInt(1)==t){
                    nombreT=tipoNombreList.get(lugar);
                    equipoNombreList.add(equipo.getString(0)+"-"+nombreT);
                }
                lugar++;
            }

        }
        spEquipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,equipoIdList));
        spEquipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,equipoNombreList));
        spEquipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectEquipo=null;
                }else {
                    valorSelectEquipo=equipoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectEquipo=null;

            }
        });

        helperJ.close();


        if (vista==1){
            spEquipo.setEnabled(true);
            spEquipo.setVisibility(View.INVISIBLE);
        }else {
            spDocumento.setEnabled(false);
            spDocumento.setVisibility(View.INVISIBLE);
        }
        if (getIntent().hasExtra("idPrestamo")){
            opcion=1;
            sinBusqueda(bundle.getInt("idPrestamo"));
            editBuscar.setVisibility(View.INVISIBLE);
            btnBuscar.setVisibility(View.INVISIBLE);
        }else {
            opcion=0;
            spTipoMov.setEnabled(false);
            spDocente.setEnabled(false);
            spMateria.setEnabled(false);
            spMotivo.setEnabled(false);
            spHorario.setEnabled(false);
            spUnidad.setEnabled(false);
            spEquipo.setEnabled(false);
            spDocumento.setEnabled(false);
        }


    }

    public void buscarPrestamoEditar(View v){
        if (vista==1){
            if (editBuscar.getText().toString().isEmpty()){
                Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
            }
            else {
                TablasJ helper = TablasJ.getInstance(getApplicationContext());
                helper.open();
                Prestamo prestamo=helper.prestamoConsultar(Integer.valueOf(editBuscar.getText().toString()));
                helper.close();
                if (prestamo==null){
                    Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else {
                    if (prestamo.getSerialEquipo()==null){
                        TablasV helperV = TablasV.getInstance(getApplicationContext());
                        helperV.open();
                        Documento documentoActual=helperV.documentoConsultar(prestamo.getIdDocumento());
                        helperV.close();
                        documentoIdList.add(prestamo.getIdDocumento());
                        documentoNombreList.add(String.valueOf(prestamo.getIdDocumento())+"-"+documentoActual.getNombreDocumento());


                        spTipoMov.setEnabled(true);
                        spDocente.setEnabled(true);
                        spMateria.setEnabled(true);
                        spMotivo.setEnabled(true);
                        spHorario.setEnabled(true);
                        spUnidad.setEnabled(true);
                        spEquipo.setEnabled(true);
                        spDocumento.setEnabled(true);
                        editBuscar.setEnabled(false);
                        btnBuscar.setEnabled(false);

                        int posSpTipoMov = tipoMovIdList.indexOf(prestamo.getIdTipoMovimiento());
                        int posSpDocente = docenteIdList.indexOf(prestamo.getCarnetDocente());
                        int posSpMateria = materiaIdList.indexOf(prestamo.getIdMateria());
                        int posSpMotivo = motivoIdList.indexOf(prestamo.getIdMotivo());
                        int posSpHorario = horarioIdList.indexOf(prestamo.getIdHorario());
                        int posSpUnidad = unidadIdList.indexOf(prestamo.getIdUnidadAdministrativa());
                        int posSpEquipo = equipoIdList.indexOf(prestamo.getSerialEquipo());
                        int posSpDocumento = documentoIdList.indexOf(prestamo.getIdDocumento());

                        editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                        spTipoMov.setSelection(posSpTipoMov);
                        spDocente.setSelection(posSpDocente);
                        spMateria.setSelection(posSpMateria);
                        spMotivo.setSelection(posSpMotivo);
                        spHorario.setSelection(posSpHorario);
                        spUnidad.setSelection(posSpUnidad);
                        spEquipo.setSelection(posSpEquipo);
                        spDocumento.setSelection(posSpDocumento);
                        clave=prestamo.getIdMovimientoInventario();
                    }else {
                        Toast.makeText(this,getResources().getString(R.string.msjEsPrestamo02), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }else {
            if (editBuscar.getText().toString().isEmpty()){
                Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
            }
            else {
                TablasJ helper = TablasJ.getInstance(getApplicationContext());
                helper.open();
                Prestamo prestamo=helper.prestamoConsultar(Integer.valueOf(editBuscar.getText().toString()));
                helper.close();
                if (prestamo==null){
                    Toast.makeText(this,getResources().getString(R.string.msjEsPrestamo02), Toast.LENGTH_LONG).show();
                }else {
                    if (prestamo.getSerialEquipo()==null){
                        Toast.makeText(this,getResources().getString(R.string.msjEsPrestamo01), Toast.LENGTH_LONG).show();
                    }else {
                        helper.open();
                        Equipo equipoActual=helper.equipoConsultar(prestamo.getSerialEquipo());
                        TipoEquipo tipoEquiActual=helper.tipoEquipoConsultarActual(equipoActual.getIdTipoEquipo());
                        helper.close();
                        equipoIdList.add(prestamo.getSerialEquipo());
                        equipoNombreList.add(prestamo.getSerialEquipo()+"-"+tipoEquiActual.getNombreTipoEquipo());


                        spTipoMov.setEnabled(true);
                        spDocente.setEnabled(true);
                        spMateria.setEnabled(true);
                        spMotivo.setEnabled(true);
                        spHorario.setEnabled(true);
                        spUnidad.setEnabled(true);
                        spEquipo.setEnabled(true);
                        spDocumento.setEnabled(true);
                        editBuscar.setEnabled(false);
                        btnBuscar.setEnabled(false);

                        int posSpTipoMov = tipoMovIdList.indexOf(prestamo.getIdTipoMovimiento());
                        int posSpDocente = docenteIdList.indexOf(prestamo.getCarnetDocente());
                        int posSpMateria = materiaIdList.indexOf(prestamo.getIdMateria());
                        int posSpMotivo = motivoIdList.indexOf(prestamo.getIdMotivo());
                        int posSpHorario = horarioIdList.indexOf(prestamo.getIdHorario());
                        int posSpUnidad = unidadIdList.indexOf(prestamo.getIdUnidadAdministrativa());
                        int posSpEquipo = equipoIdList.indexOf(prestamo.getSerialEquipo());
                        int posSpDocumento = documentoIdList.indexOf(prestamo.getIdDocumento());

                        editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                        spTipoMov.setSelection(posSpTipoMov);
                        spDocente.setSelection(posSpDocente);
                        spMateria.setSelection(posSpMateria);
                        spMotivo.setSelection(posSpMotivo);
                        spHorario.setSelection(posSpHorario);
                        spUnidad.setSelection(posSpUnidad);
                        spEquipo.setSelection(posSpEquipo);
                        spDocumento.setSelection(posSpDocumento);
                        clave=prestamo.getIdMovimientoInventario();
                    }
                }
            }
        }


    }

    public void sinBusqueda(Integer idPres){
        if (vista==1){
            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            Prestamo prestamo=helper.prestamoConsultar(idPres);
            helper.close();
            if (prestamo==null){
            }else {
                if (prestamo.getSerialEquipo()==null){
                    TablasV helperV = TablasV.getInstance(getApplicationContext());
                    helperV.open();
                    Documento documentoActual=helperV.documentoConsultar(prestamo.getIdDocumento());
                    helperV.close();
                    documentoIdList.add(prestamo.getIdDocumento());
                    documentoNombreList.add(String.valueOf(prestamo.getIdDocumento())+"-"+documentoActual.getNombreDocumento());


                    spTipoMov.setEnabled(true);
                    spDocente.setEnabled(true);
                    spMateria.setEnabled(true);
                    spMotivo.setEnabled(true);
                    spHorario.setEnabled(true);
                    spUnidad.setEnabled(true);
                    spEquipo.setEnabled(true);
                    spDocumento.setEnabled(true);
                    editBuscar.setEnabled(false);
                    btnBuscar.setEnabled(false);

                    int posSpTipoMov = tipoMovIdList.indexOf(prestamo.getIdTipoMovimiento());
                    int posSpDocente = docenteIdList.indexOf(prestamo.getCarnetDocente());
                    int posSpMateria = materiaIdList.indexOf(prestamo.getIdMateria());
                    int posSpMotivo = motivoIdList.indexOf(prestamo.getIdMotivo());
                    int posSpHorario = horarioIdList.indexOf(prestamo.getIdHorario());
                    int posSpUnidad = unidadIdList.indexOf(prestamo.getIdUnidadAdministrativa());
                    int posSpEquipo = equipoIdList.indexOf(prestamo.getSerialEquipo());
                    int posSpDocumento = documentoIdList.indexOf(prestamo.getIdDocumento());

                    editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                    spTipoMov.setSelection(posSpTipoMov);
                    spDocente.setSelection(posSpDocente);
                    spMateria.setSelection(posSpMateria);
                    spMotivo.setSelection(posSpMotivo);
                    spHorario.setSelection(posSpHorario);
                    spUnidad.setSelection(posSpUnidad);
                    spEquipo.setSelection(posSpEquipo);
                    spDocumento.setSelection(posSpDocumento);
                    clave=prestamo.getIdMovimientoInventario();
                }else {
                    Toast.makeText(this,"id  es de un prestamo de equipo", Toast.LENGTH_LONG).show();
                }
            }
        }else {
            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            Prestamo prestamo=helper.prestamoConsultar(idPres);
            helper.close();
            if (prestamo==null){
            }else {
                if (prestamo.getSerialEquipo()==null){
                    Toast.makeText(this,"id  es de un prestamo de documento", Toast.LENGTH_LONG).show();
                }else {
                    helper.open();
                    Equipo equipoActual=helper.equipoConsultar(prestamo.getSerialEquipo());
                    TipoEquipo tipoEquiActual=helper.tipoEquipoConsultarActual(equipoActual.getIdTipoEquipo());
                    helper.close();
                    equipoIdList.add(prestamo.getSerialEquipo());
                    equipoNombreList.add(prestamo.getSerialEquipo()+"-"+tipoEquiActual.getNombreTipoEquipo());


                    spTipoMov.setEnabled(true);
                    spDocente.setEnabled(true);
                    spMateria.setEnabled(true);
                    spMotivo.setEnabled(true);
                    spHorario.setEnabled(true);
                    spUnidad.setEnabled(true);
                    spEquipo.setEnabled(true);
                    spDocumento.setEnabled(true);
                    editBuscar.setEnabled(false);
                    btnBuscar.setEnabled(false);

                    int posSpTipoMov = tipoMovIdList.indexOf(prestamo.getIdTipoMovimiento());
                    int posSpDocente = docenteIdList.indexOf(prestamo.getCarnetDocente());
                    int posSpMateria = materiaIdList.indexOf(prestamo.getIdMateria());
                    int posSpMotivo = motivoIdList.indexOf(prestamo.getIdMotivo());
                    int posSpHorario = horarioIdList.indexOf(prestamo.getIdHorario());
                    int posSpUnidad = unidadIdList.indexOf(prestamo.getIdUnidadAdministrativa());
                    int posSpEquipo = equipoIdList.indexOf(prestamo.getSerialEquipo());
                    int posSpDocumento = documentoIdList.indexOf(prestamo.getIdDocumento());

                    editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                    spTipoMov.setSelection(posSpTipoMov);
                    spDocente.setSelection(posSpDocente);
                    spMateria.setSelection(posSpMateria);
                    spMotivo.setSelection(posSpMotivo);
                    spHorario.setSelection(posSpHorario);
                    spUnidad.setSelection(posSpUnidad);
                    spEquipo.setSelection(posSpEquipo);
                    spDocumento.setSelection(posSpDocumento);
                    clave=prestamo.getIdMovimientoInventario();
                }
            }
        }
    }

    public void actualizarPrestamo(View v){
        if (editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {

            Prestamo prestamo = new Prestamo();
            prestamo.setIdMovimientoInventario(clave);
            prestamo.setIdTipoMovimiento(valorSelectTipoMov);
            prestamo.setCarnetDocente(valorSelectDocente);
            prestamo.setIdMotivo(valorSelectMotivo);
            prestamo.setIdHorario(valorSelectHorario);
            prestamo.setIdUnidadAdministrativa(valorSelectUnidad);
            prestamo.setSerialEquipo(valorSelectEquipo);
            prestamo.setIdDocumento(valorSelectDocumento);
            prestamo.setIdMateria(valorSelectMateria);

            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            final String mensaje =helper.actualizarPrestamo(prestamo, clave);
            helper.close();
            //Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

            // Build an AlertDialog
            AlertDialog.Builder builder = new AlertDialog.Builder(PrestamoEditarActivity.this);

            // Set a message/question for alert dialog
            builder.setMessage(mensaje);

            // Specify the dialog is not cancelable
            builder.setCancelable(false);

            // Set a title for alert dialog
            builder.setTitle("Mensaje");

            // Set the positive/yes button click click listener
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do something when click positive button
                    //rl.setBackgroundColor(Color.parseColor("#FFA4E098"));
                    Intent intentAyuda= new Intent(getBaseContext(), PrestamoEditarActivity.class);
                    intentAyuda.putExtra("vista",vista);
                    getBaseContext().startActivity(intentAyuda);
                }
            });

            AlertDialog dialog = builder.create();
            // Display the alert dialog on interface
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
