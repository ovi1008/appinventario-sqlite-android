package edu.ues.fia.inventarioues.Controladores;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;

public class TablasL {

    //BASE DE DATOS ACCESO

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TablasL instance;

    private TablasL(Context context){
        this.openHelper=new DatabaseOpenHelper(context);
    }

    public static TablasL getInstance(Context context){
        if(instance==null){
            instance=new TablasL(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getWritableDatabase();
        habilitarForaneas();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public void habilitarForaneas(){
        db.execSQL("PRAGMA foreign_keys=ON;");
    }


    //Controles de las tablas dentro de la base de datos

    //TABLA MATERIA
    private static final String[]camposMateria= new String[]{"idMateria", "nombreMateria"};

    public Cursor obtenerMaterias() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("materia", camposMateria,null,null, null, null, null);
    }


    public String insertarMateria(Materia materia){
        String mensaje= "Materia insertada correctamente ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //(nombreEnBd,mocelo.meto
        v.put("idMateria", materia.getIdMateria());
        v.put("nombreMateria", materia.getNombreMateria());
        inserto=db.insert("materia", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar materia, registro duplicado.";
        }else {
            mensaje=mensaje+materia.getNombreMateria();
        }
        return mensaje;
    }

    public String actualizarMateria(Materia materia, String clave){
        int  exito;
        String mensaje= "Se actualizo materia con nombre: ";
        String id[]={clave};
        //{"idMateria", "nombreMateria"};
        Cursor c=db.query("materia", null,"idMateria=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            //{"idMateria", "nombreMateria"};
            v.put("idMateria",materia.getIdMateria());
            v.put("nombreMateria",materia.getNombreMateria ());
            try {
                db.update("materia",v,"idMateria=?",id);
                mensaje=mensaje+materia.getIdMateria();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La materia con id: "+materia.getIdMateria()+" no existe";
        }
    }


    public String eliminar1Materia(Materia materia){
        String mensaje = "Se elimino la materia con id: ";
        int contador=0;
        String id[]={materia.getIdMateria()};
        //{"idMateria", "nombreMateria"};
        Cursor c=db.query("materia", null,"idMateria=?",id,null,null,null);
        if(c.moveToFirst()){
            Materia materia2=materiaConsultar(materia.getIdMateria());
            String id2[]={materia2.getIdMateria()};
            // "movimientoInventario" campos "idTipoMovimiento",	"carnetDocente"	,"fechaMovimientoInventario", "fechaDevolucionMovInv","estadoMovInv", "idMotivo","idHorario",
            //	"idUnidadAdministrativa","serialEquipo","idDocumento","idMateria"
            contador=db.query("movimientoInventario",null,"idMateria=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("materia","idMateria=?",id);
                return mensaje=mensaje+materia.getIdMateria();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="La materia con id: "+materia.getIdMateria()+" no existe";
        }
    }

    public String eliminar2Materia(Materia materia){
        String mensaje = "Se elimino la materia con id: "+materia.getIdMateria();
        String id[]={materia.getIdMateria()};
        db.delete("materia","idMateria=?",id);
        return mensaje;
    }
    public Materia materiaConsultar(String idMateria){
        String[] id={idMateria};
        Cursor c=db.query("materia", camposMateria, "idMateria=?", id, null, null, null);
        if(c.moveToFirst()){
            Materia materia= new Materia();
            //{"idMateria", "nombreMateria"}
            materia.setIdMateria(c.getString(0));
            materia.setNombreMateria(c.getString(1));
            return materia;
        }else {
            return null;
        }



    }

   //TABLA Unidad administrativa
    private static final String[]camposUnidadAdministrativa= new String[]{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};

    public Cursor obtenerUnidadesAdministrativas() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("unidadAdministrativa", camposUnidadAdministrativa,null,null, null, null, null);
    }

    public String insertarUnidadAdministrativa(UnidadAdministrativa unidadAdministrativa){
        String mensaje= "Unidad Administrativa a insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
        v.put("nombreUnidadAdministrativa", unidadAdministrativa.getNombreUnidadAdministrativa());
        inserto=db.insert("unidadAdministrativa", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar Unidad Administrativa, registro duplicado.";
        }else {
            mensaje=mensaje+unidadAdministrativa.getNombreUnidadAdministrativa();
        }
        return mensaje;
    }
    public UnidadAdministrativa unidadAdministrativaConsultar(String nombreUnidadAdministrativa){
        String[] id={nombreUnidadAdministrativa};
        Cursor c=db.query("unidadAdministrativa", camposUnidadAdministrativa, "nombreUnidadAdministrativa=?", id, null, null, null);
        if(c.moveToFirst()){
            UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
            //{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
            unidadAdministrativa.setIdUnidadAdministrativa(c.getInt(0));
            unidadAdministrativa.setNombreUnidadAdministrativa(c.getString(1));
            return unidadAdministrativa;
        }else {
            return null;
        }
    }

    public String actualizarUnidadAdministrativa(UnidadAdministrativa unidadAdministrativa, String clave){
        String mensaje= "Se actualizo unidad administrativa con nombre: ";
        String id[]={clave};
        ////{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
        Cursor c=db.query("unidadAdministrativa", null,"nombreUnidadAdministrativa=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                ////{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
                v.put("idUnidadAdministrativa",unidadAdministrativa.getIdUnidadAdministrativa());
                v.put("nombreUnidadAdministrativa",unidadAdministrativa.getNombreUnidadAdministrativa());
                db.update("unidadAdministrativa",v,"nombreUnidadAdministrativa=?",id);
                mensaje=mensaje+unidadAdministrativa.getNombreUnidadAdministrativa();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="La unidad administrativa con nombre: "+unidadAdministrativa.getNombreUnidadAdministrativa()+" no existe";
        }
    }

    public String eliminar1UnidadAdministrativa(UnidadAdministrativa unidadAdministrativa){
        String mensaje = "Se elimino la unidad administrativa con el nombre: ";
        int contador=0;
        String id[]={unidadAdministrativa.getNombreUnidadAdministrativa()};
        //{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
        Cursor c=db.query("unidadAdministrativa", null,"nombreUnidadAdministrativa=?",id,null,null,null);
        if(c.moveToFirst()){
            UnidadAdministrativa unidadAdministrativa2=unidadAdministrativaConsultar(unidadAdministrativa.getNombreUnidadAdministrativa());
            String id2[]={String.valueOf(unidadAdministrativa2.getIdUnidadAdministrativa())};
            //duda
            contador=db.query("movimientoInventario",null,"idUnidadAdministrativa=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("unidadAdministrativa","nombreUnidadAdministrativa=?",id);
                return mensaje=mensaje+unidadAdministrativa.getNombreUnidadAdministrativa();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="La unidad administrativa con nombre: : "+unidadAdministrativa.getNombreUnidadAdministrativa()+" no existe";
        }
    }


    public String eliminar2UnidadAdministrativa(UnidadAdministrativa unidadAdministrativa){
        String mensaje = "Se elimino unidad administrativa con nombre: "+unidadAdministrativa.getNombreUnidadAdministrativa();
        String id[]={unidadAdministrativa.getNombreUnidadAdministrativa()};
        db.delete("unidadAdministrativa","nombreUnidadAdministrativa=?",id);
        return mensaje;
    }

    //tabla idioma

    private static final String[]camposIdioma= new String[]{"codIdioma", "nombreIdioma"};

    public Cursor obtenerIdioma() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("idioma", camposIdioma,null,null, null, null, null);
    }
    public String insertarIdioma(Idioma idioma){
        String mensaje= "Idioma insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //{"codIdioma", "nombreIdioma"}
        v.put("nombreIdioma", idioma.getNombreIdioma());
        inserto=db.insert("idioma", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar idioma, registro duplicado.";
        }else {
            mensaje=mensaje+idioma.getNombreIdioma();
        }
        return mensaje;
    }
    public Idioma idiomaConsultar(String nombreIdioma){
        String[] id={nombreIdioma};
        Cursor c=db.query("idioma", camposIdioma, "nombreIdioma=?", id, null, null, null);
        if(c.moveToFirst()){
            Idioma idioma= new Idioma();
            //{"codIdioma", "nombreIdioma"};
            idioma.setCodIdioma(c.getInt(0));
            idioma.setNombreIdioma(c.getString(1));
            return idioma;
        }else {
            return null;
        }
    }

    public String actualizarIdioma(Idioma idioma, String clave){
        String mensaje= "Se actualizo Idioma con nombre: ";
        String id[]={clave};
        //{"codIdioma", "nombreIdioma"};
        Cursor c=db.query("idioma", null,"nombreIdioma=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"codIdioma", "nombreIdioma"};
                v.put("codIdioma",idioma.getCodIdioma());
                v.put("nombreIdioma",idioma.getNombreIdioma());
                db.update("idioma",v,"nombreIdioma=?",id);
                mensaje=mensaje+idioma.getNombreIdioma();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El idioma con el nombre: "+idioma.getNombreIdioma()+" no existe";
        }
    }
    public String eliminar1Idioma(Idioma idioma){
        String mensaje = "Se elimino el Idioma con el nombre: ";
        int contador=0;
        String id[]={idioma.getNombreIdioma()};
        //{"codIdioma", "nombreIdioma"};
        Cursor c=db.query("idioma", null,"nombreIdioma=?",id,null,null,null);
        if(c.moveToFirst()){
            Idioma idioma2=idiomaConsultar(idioma.getNombreIdioma());
            String id2[]={String.valueOf(idioma2.getCodIdioma())};
            //duda
            contador=db.query("documento",null,"codIdioma=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("idioma","nombreIdioma=?",id);
                return mensaje=mensaje+idioma.getNombreIdioma();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El idioma con nombre: : "+idioma.getNombreIdioma()+" no existe";
        }
    }

    public String eliminar2Idioma(Idioma idioma){
        String mensaje = "Se elimino el idioma con el nombre: "+idioma.getNombreIdioma();
        String id[]={idioma.getNombreIdioma()};
        db.delete("idioma","nombreIdioma=?",id);
        return mensaje;
    }
}
