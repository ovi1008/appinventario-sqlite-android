package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.TipoDocumentoAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;


public class TipoDocumentoMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_menu);
        ListView listView =(ListView)findViewById(R.id.listTiposDocumentos);
        List<TipoDocumento> lista= listadoTipoDocumentos();
        TipoDocumentoAdaptador tipoDocumentoAdaptador=new TipoDocumentoAdaptador(this,lista);
        listView.setAdapter(tipoDocumentoAdaptador);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    public List<TipoDocumento> listadoTipoDocumentos(){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        Cursor c =helper.obtenerTiposDocumento();
        List<TipoDocumento> tipoDocumentosList= new ArrayList<>();
        while (c.moveToNext()){
            TipoDocumento tipoDocumento=new TipoDocumento();
            //camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"};
            tipoDocumento.setIdTipoDocumento(c.getInt(0));
            tipoDocumento.setNombreTipoDocumento(c.getString(1));
            tipoDocumentosList.add(tipoDocumento);
        }
        return tipoDocumentosList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
