package edu.ues.fia.inventarioues;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.Usuario;
import edu.ues.fia.inventarioues.Controladores.UsuarioAccesos;
import edu.ues.fia.inventarioues.Vistas.Autor.AutorMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Docente.DocenteMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Documento.DocumentoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Editorial.EditorialMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Equipo.EquipoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Horario.HorarioMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Idioma.IdiomaMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Materia.MateriaMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Motivo.MotivoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.Prestamo.PrestamoActivity;
import edu.ues.fia.inventarioues.Vistas.Prestamo.PrestamoDevolverActivity;
import edu.ues.fia.inventarioues.Vistas.TipoAutor.TipoAutorMenuActivity;
import edu.ues.fia.inventarioues.Vistas.TipoDocumento.TipoDocumentoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.TipoEquipo.TipoEquipoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoMenuActivity;
import edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa.UnidadAdministrativaMenuActivity;

public class MenuPrincipalActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    //aqui pones el menu principal
    String [] menu;
    String msj1, msj2;
    Integer idUsuario;
    UsuarioAccesos userAcce = new UsuarioAccesos();
    Usuario user= new Usuario();
    Locale locale;
    Configuration config = new Configuration();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        menu=getResources().getStringArray(R.array.menuPrincipal);
        msj1=getResources().getString(R.string.menuPrincipalAlertMsj1)+": ";
        msj2=getResources().getString(R.string.menuPrincipalAlertMsj2);
        

        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        idUsuario=helper.obtenerUsuarioActual();
        helper.close();

        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, menu);
        ListView listView = findViewById(R.id.listMenuPrincipal);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    public Integer metodoAcceso(Integer id, Integer posicion){
        Integer acceso=0;
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        userAcce=helper.usuarioAccesosConsultar(id);
        user=helper.usuarioDatosConsultar(id);
        helper.close();

        switch (posicion){
            case 0:
                acceso=userAcce.gettMarca();
                break;
            case 1:
                acceso=userAcce.gettTipoEquipo();
                break;
            case 2:
                acceso=userAcce.gettEquipo();
                break;
            case 3:
                acceso=userAcce.gettEditorial();
                break;
            /* case 4:
                acceso=userAcce.gettAutor();
                break; */
            case 4:
                acceso=userAcce.gettDocumento();
                break;
            case 5:
                acceso=userAcce.gettMateria();
                break;
            case 6:
                acceso=userAcce.gettTipoMovimiento();
                break;
            case 7:
                acceso=userAcce.gettMotivo();
                break;
            case 8:
                acceso=userAcce.gettTipoAutor();

            case 9:
                acceso=userAcce.gettTipoDocumento();
                break;
            case 10:
                acceso=userAcce.gettUnidadAdmin();
                break;
            case 11:
                acceso=userAcce.gettIdioma();
                break;
            case 12:
                acceso=userAcce.gettDocente();
                break;
            case 13:
                acceso=userAcce.gettHorario();
                break;
            case 14:
                acceso=userAcce.gettPrestamo();
                break;

        }

        return  acceso;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentMarca = new Intent(this, MarcaMenuActivity.class);
                    this.startActivity(intentMarca);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 1:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentTipoEquipo = new Intent(this, TipoEquipoMenuActivity.class);
                    this.startActivity(intentTipoEquipo);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 2:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentEquipo = new Intent(this, EquipoMenuActivity.class);
                    this.startActivity(intentEquipo);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
            case 3:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentEditorial=new Intent(this, EditorialMenuActivity.class);
                    this.startActivity(intentEditorial);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
            /*case 4:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentAutor=new Intent(this, AutorMenuActivity.class);
                    this.startActivity(intentAutor);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break; */

            case 4:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentDocumento=new Intent(this, DocumentoMenuActivity.class);
                    this.startActivity(intentDocumento);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 5:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentMateria=new Intent(this, MateriaMenuActivity.class);
                    this.startActivity(intentMateria);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 6:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentTipoMov=new Intent(this, TipoMovimientoMenuActivity.class);
                    this.startActivity(intentTipoMov);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 7:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentMotivo=new Intent(this, MotivoMenuActivity.class);
                    this.startActivity(intentMotivo);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 8:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentTipoAutor=new Intent(this, TipoAutorMenuActivity.class);
                    this.startActivity(intentTipoAutor);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 9:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentTipoDocu=new Intent(this, TipoDocumentoMenuActivity.class);
                    this.startActivity(intentTipoDocu);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 10:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentUnidaAdmi=new Intent(this, UnidadAdministrativaMenuActivity.class);
                    this.startActivity(intentUnidaAdmi);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 11:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentIdioma=new Intent(this, IdiomaMenuActivity.class);
                    this.startActivity(intentIdioma);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 12:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentDocente=new Intent(this, DocenteMenuActivity.class);
                    this.startActivity(intentDocente);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 13:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentHorario=new Intent(this, HorarioMenuActivity.class);
                    this.startActivity(intentHorario);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

            case 14:
                if (metodoAcceso(idUsuario, position)==1){
                    Intent intentPrestamo=new Intent(this, PrestamoActivity.class);
                    this.startActivity(intentPrestamo);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
                    builder.setMessage(msj1+user.getNombreUsuario()+" "+user.getApellidoUsuario()+"\n\n"+msj2);

                    builder.setCancelable(false);
                    builder.setTitle(R.string.menuPrincipalAlertTitulo);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_menuPrincipal_configuraciones:
                // Build an AlertDialog
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MenuPrincipalActivity.this);

                // Set a title for alert dialog
                builder.setTitle(R.string.loginIdioma);

                // Initializing an array of idiomas
                final String[] idiomas = getResources().getStringArray(R.array.idiomas);

                // Set the list of items for alert dialog
                builder.setItems(idiomas, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String selectedIdioma = Arrays.asList(idiomas).get(which);
                        // Set the layout background color as user selection
                        //rl.setBackgroundColor(Color.parseColor(selectedIdioma));
                        if (selectedIdioma.equals(idiomas[0])){
                            locale = new Locale("es");
                            config.locale =locale;
                            getResources().updateConfiguration(config, null);
                            Intent refresh = new Intent(MenuPrincipalActivity.this, MenuPrincipalActivity.class);
                            startActivity(refresh);

                        }
                        if (selectedIdioma.equals(idiomas[1])){
                            locale = new Locale("en");
                            config.locale =locale;
                            getResources().updateConfiguration(config, null);
                            Intent refresh = new Intent(MenuPrincipalActivity.this, MenuPrincipalActivity.class);
                            startActivity(refresh);
                        }
                        if (selectedIdioma.equals(idiomas[2])){
                            locale = new Locale("it");
                            config.locale =locale;
                            getResources().updateConfiguration(config, null);
                            Intent refresh = new Intent(MenuPrincipalActivity.this, MenuPrincipalActivity.class);
                            startActivity(refresh);
                        }
                        if (selectedIdioma.equals(idiomas[3])){
                            locale = new Locale("pt");
                            config.locale =locale;
                            getResources().updateConfiguration(config, null);
                            Intent refresh = new Intent(MenuPrincipalActivity.this, MenuPrincipalActivity.class);
                            startActivity(refresh);
                        }

                    }
                });

                android.app.AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
                return true;

            case R.id.action_principal_menu_principal:
                Intent intentLogin= new Intent(this,MainActivity.class);
                this.startActivity(intentLogin);
                return true;




        }
        return super.onOptionsItemSelected(item);
    }


}
