package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;


public class IdiomaEliminarAdvertenciaActivity extends AppCompatActivity {
    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_eliminar_advertencia);
        msj=(TextView)findViewById(R.id.IdiomaEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.idiomaMsj2)+" "
                +bundle.getString("nombre")
                +"\n\n"+getResources().getString(R.string.idiomaMsj3)+" "
                +bundle.getString("msj"));
    }
    public void eliminarAdvIdioma(View v){
        Bundle bundle=getIntent().getExtras();
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        Idioma idioma= new Idioma();
        idioma.setNombreIdioma(bundle.getString("nombre"));
        String mensaje= helper.eliminar2Idioma(idioma);
        helper.close();
        Intent intentM= new Intent(this, IdiomaMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarIdioma(View v){
        Intent intentCancelar = new Intent(this, IdiomaMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
