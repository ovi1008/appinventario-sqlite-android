package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

public class TipoDocumentoEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca;
    String clave="";
    Button busca;
    int opcion=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_editar);
        editId=(EditText)findViewById(R.id.TipoDocumentoEditarId);
        editNombre=(EditText)findViewById(R.id.TipoDocumentoEditarNombre);
        editBusca=(EditText) findViewById(R.id.TipoDocumentoEditarSearch);
        busca=(Button)findViewById(R.id.TipoDocumentoEditarBtnBuscar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);

        }
    }

    public void actualizarTipoDocumento(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper= TablasM.getInstance(getApplicationContext());
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setIdTipoDocumento(Integer.valueOf(editId.getText().toString()));
            tipoDocumento.setNombreTipoDocumento(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarTipoDocumento(tipoDocumento, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    public void buscarTipoDocumentoEditar(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        TipoDocumento tipoDocumento=helper.tipoDocumentoConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (tipoDocumento==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(tipoDocumento.getIdTipoDocumento()));
            editNombre.setText(tipoDocumento.getNombreTipoDocumento());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}