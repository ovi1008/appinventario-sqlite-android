package edu.ues.fia.inventarioues.Vistas.Docente;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.DocenteAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;

public class DocenteMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docente_menu);
        ListView listView =(ListView)findViewById(R.id.listDocente);
        List<Docente> lista= listadoDocentes();
        DocenteAdaptador docenteAdaptador=new DocenteAdaptador(this,lista);
        listView.setAdapter(docenteAdaptador);
    }

    public List<Docente> listadoDocentes(){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        Cursor c =helper.obtenerDocentes();
        List<Docente> docenteList= new ArrayList<>();
        while (c.moveToNext()){
            Docente docente=new Docente();
            //camposDocente= new String[]{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"}
            docente.setCarnetDocente(c.getString(0));
            docente.setNombreDocente(c.getString(1));
            docente.setApellidoDocente(c.getString(2));
            docente.setTelefonoDocente(c.getString(3));
            docente.setDireccionDocente(c.getString(4));
            docenteList.add(docente);

            //POSIBLE ERROR
        }
        return docenteList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docente,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_docente_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_docente_menu_insertar:
                Intent intent0= new Intent(this, DocenteInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_docente_menu_Editar:
                Intent intente= new Intent(this, DocenteEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_docente_menu_consultar:
                Intent intent1= new Intent(this, DocenteBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_docente_menu_eliminar:
                Intent intent2= new Intent(this, DocenteEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_docente_listar:
                Intent intent3= new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
