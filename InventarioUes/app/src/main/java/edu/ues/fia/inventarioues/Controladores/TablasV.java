package edu.ues.fia.inventarioues.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;

public class TablasV {

    //BASE DE DATOS ACCESO

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TablasV instance;

    private TablasV(Context context){
        this.openHelper=new DatabaseOpenHelper(context);
    }

    public static TablasV getInstance(Context context){
        if(instance==null){
            instance=new TablasV(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getWritableDatabase();
        habilitarForaneas();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public void habilitarForaneas(){
        db.execSQL("PRAGMA foreign_keys=ON;");
    }


    //Controles de las tablas dentro de la base de datos


    // TABLA EDITORIAL
    private  static final String [] camposEditorial = new String[]{"idEditorial", "nombreEditorial"};

    public Cursor obtenerEditoriales() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("editorial", camposEditorial,null,null, null, null, null);
    }

    public String insertarEditorial(Editorial editorial){
        String mensaje= "Editorial insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        v.put("nombreEditorial", editorial.getNombreEditorial());
        inserto=db.insert("editorial", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar editorial, registro duplicado.";
        }else {
            mensaje=mensaje+editorial.getNombreEditorial();
        }
        return mensaje;
    }

    public String actualizarEditorial(Editorial editorial, String clave){
        int  exito;
        String mensaje= "Se actualizo editorial con nombre: ";
        String id[]={clave};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("editorial", null,"nombreEditorial=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            //{"idMarca", "nombreMarca"};
            v.put("idEditorial",editorial.getIdEditorial());
            v.put("nombreEditorial",editorial.getNombreEditorial());
            try {
                db.update("editorial",v,"nombreEditorial=?",id);
                mensaje=mensaje+editorial.getNombreEditorial();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La editorial con el nombre: "+editorial.getNombreEditorial()+" no existe";
        }
    }

    public Editorial editorialConsultar(String nombreEditorial){
        String[] id={nombreEditorial};
        Cursor c=db.query("editorial", camposEditorial, "nombreEditorial=?", id, null, null, null);
        if(c.moveToFirst()){
            Editorial editorial = new Editorial();
            //{"idMarca", "nombreMarca"}
            editorial.setIdEditorial(c.getInt(0));
            editorial.setNombreEditorial(c.getString(1));
            return editorial;
        }else {
            return null;
        }
    }

    //added!!!
    private  static final String [] camposDocumento = new String[]{"idDocumento", "isbnDocumento",
            "idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento","prestadoDocumento","autoresDocumento"};


    public String eliminar1Editorial(Editorial editorial){
        String mensaje = "Se elimino la editorial con el nombre: ";
        int contador=0;
        String id[]={editorial.getNombreEditorial()};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("editorial", null,"nombreEditorial=?",id,null,null,null);
        if(c.moveToFirst()){
            Editorial editorial2=editorialConsultar(editorial.getNombreEditorial());
            String id2[]={String.valueOf(editorial2.getIdEditorial())};


            contador=db.query("documento",camposDocumento,"idEditorial=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("editorial","nombreEditorial=?",id);
                return mensaje=mensaje+editorial.getNombreEditorial();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="La editorial con el nombre: "+editorial.getNombreEditorial()+" no existe";
        }
    }

    public String eliminar2Editorial(Editorial editorial){
        String mensaje = "Se elimino la editorial con el nombre: "+editorial.getNombreEditorial();
        String id[]={editorial.getNombreEditorial()};
        db.delete("editorial","nombreEditorial=?",id);
        return mensaje;
    }


//TABLA EDITORIAL

    // TABLA AUTOR
    private  static final String [] camposAutor = new String[]{"idAutor", "idTipoAutor",
            "nombreAutor","apellidoAutor","direccionAutor","telefonoAutor"};
    private static final String [] camposIdioma = new String[]{"codIdioma","nombreIdioma"};
    private static final String[] camposTipoAutor = new String[]{"idTipoAutor","nombreTipoAutor"};

    public Cursor obtenerTipoAutores() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("tipoAutor", camposTipoAutor,null,null, null, null, null);
    }

    public Cursor obtenerIdiomas() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("idioma", camposIdioma,null,null, null, null, null);
    }

    public Cursor obtenerAutores() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("autor", camposAutor,null,null, null, null, null);
    }

    public Autor AutorConsultar(String nombreAutor, String apellidoAutor){
        String[] id={nombreAutor,apellidoAutor};
        Cursor c=db.query("autor", camposAutor, "nombreAutor=? AND apellidoAutor=?", id, null, null, null);
        if(c.moveToFirst()){
            Autor autor = new Autor();

            autor.setIdAutor(c.getInt(0));
            autor.setIdTipoAutor(c.getInt(1));
            autor.setNombreAutor(c.getString(2));
            autor.setApellidoAutor(c.getString(3));
            autor.setDireccionAutor(c.getString(4));
            autor.setTelefonoAutor(c.getString(5));

            return autor;
        }else {
            return null;
        }
    }

    public String consultarTipoAutor(String idTipoAutor){
        String[] id={idTipoAutor};

        Cursor c =db.query("tipoAutor",camposTipoAutor,"idTipoAutor=?",id, null, null, null);
        if(c.moveToFirst()){
            String tipoAutor =c.getString(1);
            return tipoAutor;
        }

        else{
            return "Error";
        }
    }




    public String insertarAutor(Autor autor){
        String mensaje= "Autor insertado correctamente";
        long inserto=0;

        ContentValues v = new ContentValues();
        v.put("idTipoAutor", autor.getIdTipoAutor());
        v.put("nombreAutor", autor.getNombreAutor());
        v.put("apellidoAutor", autor.getApellidoAutor());
        v.put("direccionAutor", autor.getDireccionAutor());
        v.put("telefonoAutor", autor.getTelefonoAutor());
        inserto=db.insert("autor", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar autor, registro duplicado.";
        }
        return mensaje;
    }
    private static final String[] camposDetalleAutor ={"idDocumento","idAutor"};

    public String eliminar1Autor(Autor autor){
        String mensaje = "Se elimino el autor con el nombre: ";
        int contador=0;
        String id[]={autor.getNombreAutor(),autor.getApellidoAutor()};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("autor", null,"nombreAutor=? AND apellidoAutor=?",id,null,null,null);
        if(c.moveToFirst()){
            System.out.println("Encontro algo");

            Autor autor2=AutorConsultar(autor.getNombreAutor(),autor.getApellidoAutor());
            String id2[]={String.valueOf(autor2.getIdAutor())};

            db.delete("autor","nombreAutor=? AND apellidoAutor=?",id);
            return mensaje=mensaje+autor.getNombreAutor()+" "+autor.getApellidoAutor();


//            contador=db.query("detalleAutor",camposDetalleAutor,"idAutor=?", id2,null,null,null).getCount();
//            if (contador==0){
//                System.out.println("Hola eliminar sencillo");
//
//                db.delete("autor","nombreAutor=? AND apellidoAutor=?",id);
//                return mensaje=mensaje+autor.getNombreAutor()+" "+autor.getApellidoAutor();
//            }else {
//                System.out.println("Tiene hijos");
//                return mensaje=String.valueOf(contador);
//            }
        }else {
            System.out.println("No encontro nada");
            return mensaje="El autor con el nombre: "+autor.getNombreAutor()+" " +autor.getApellidoAutor()+" no existe";
        }
    }

    public String eliminar2Autor(Autor autor){
        String mensaje = "Se elimino el autor con el nombre: "+autor.getNombreAutor()+" "+autor.getApellidoAutor();
        String id[]={autor.getNombreAutor(),autor.getApellidoAutor()};
        db.delete("autor","nombreAutor=? AND apellidoAutor=?",id);
        return mensaje;
    }
/*
{"idAutor", "idTipoAutor",
            "nombreAutor","apellidoAutor","direccionAutor","telefonoAutor"};
*/

    public String actualizarAutor(Autor autor, String nombre, String apellido){
        int  exito;
        String mensaje= "Se actualizo autor con nombre: ";
        String id[]={nombre,apellido};

        Cursor c=db.query("autor", null,"nombreAutor=? AND apellidoAutor=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            v.put("idAutor",autor.getIdAutor());
            v.put("idTipoAutor",autor.getIdTipoAutor());
            v.put("nombreAutor",autor.getNombreAutor());
            v.put("apellidoAutor",autor.getApellidoAutor());
            v.put("direccionAutor",autor.getDireccionAutor());
            v.put("telefonoAutor",autor.getTelefonoAutor());


            try {
                db.update("autor",v,"nombreAutor=? AND apellidoAutor=?",id);
                mensaje=mensaje+autor.getNombreAutor()+" "+autor.getApellidoAutor();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="El autor con el nombre: "+autor.getNombreAutor()+" " +autor.getApellidoAutor()+  "no existe";
        }
    }

    public String actualizarAutorTest(Autor autor, int idAutor){
        String mensaje= "Se actualizo autor con nombre: ";
        String id[]={String.valueOf(idAutor)};
        String id2[]={autor.getNombreAutor(),autor.getApellidoAutor()};

        Cursor c=db.query("autor", null,"idAutor=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            v.put("idAutor",autor.getIdAutor());
            v.put("idTipoAutor",autor.getIdTipoAutor());
            v.put("nombreAutor",autor.getNombreAutor());
            v.put("apellidoAutor",autor.getApellidoAutor());
            v.put("direccionAutor",autor.getDireccionAutor());
            v.put("telefonoAutor",autor.getTelefonoAutor());


            try {
                db.update("autor",v,"idAutor=?",id);
                mensaje=mensaje+autor.getNombreAutor()+" "+autor.getApellidoAutor();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="El autor con el nombre: "+autor.getNombreAutor()+" " +autor.getApellidoAutor()+  "no existe";
        }
    }






//TABLA AUTOR

    //TABLA DOCUMENTO

    //ARRAY CON CAMPOS DOCUMENTO YA FUE CREADO PREVIAMENTE

    public Cursor obtenerDocumentos() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("documento", camposDocumento,null,null, null, null, null);
    }

    private static final String[] camposTipoDocumento ={"idTipoDocumento","nombreTipoDocumento"};

    public Cursor obtenerTipoDocumentos() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("tipoDocumento", camposTipoDocumento,null,null, null, null, null);
    }

    public String insertarDocumento(Documento documento){
        String mensaje= "Documento insertado correctamente.";
        long inserto=0;
        /*    private  static final String [] camposDocumento = new String[]{"idDocumento", "isbnDocumento",
            "idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento"," prestadoDocumento" , "autoresDocumento"};*/
        ContentValues v = new ContentValues();
        v.put("isbnDocumento", documento.getIsbnDocumento());
        v.put("idTipoDocumento", documento.getIdTipoDocumento());
        v.put("idEditorial", documento.getIdEditorial());
        v.put("codIdioma", documento.getCodIdioma());
        v.put("nombreDocumento", documento.getNombreDocumento());
        v.put("edicionDocumento", documento.getEdicionDocumento());
        v.put("publicacionDocumento", documento.getPublicacionDocumento());
        v.put("prestadoDocumento", documento.getPrestadoDocumento());
        v.put("autoresDocumento", documento.getAutoresDocumento());

        inserto=db.insert("documento", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar documento, registro duplicado.";
        }
        return mensaje;
    }

    public Documento documentoConsultar(int idDocumento){
        String[] id={String.valueOf(idDocumento)};
        Cursor c=db.query("documento", camposDocumento, "idDocumento=?", id, null, null, null);
        if(c.moveToFirst()){
            Documento documento= new Documento();

            /*
            private  static final String [] camposDocumento = new String[]{"idDocumento", "isbnDocumento",
            "idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento","prestadoDocumento"};
           */
            documento.setIdDocumento(c.getInt(0));
            documento.setIsbnDocumento(c.getString(1));
            documento.setIdTipoDocumento(c.getInt(2));
            documento.setIdEditorial(c.getInt(3));
            documento.setCodIdioma(c.getInt(4));
            documento.setNombreDocumento(c.getString(5));
            documento.setEdicionDocumento(c.getInt(6));
            documento.setPublicacionDocumento(c.getString(7));
            documento.setPrestadoDocumento(c.getInt(8));
            documento.setAutoresDocumento(c.getString(9));
            return documento;
        }else {
            return null;
        }
    }

    private final static String [] camposDetalleDocumento = {"idDetalleDocumento","idMovimientoInventario","idDocumento"};

    public String eliminar1Documento(Documento documento){
        String mensaje = "Se elimino el documento con el id: ";
        int contador=0;
        String id[]={String.valueOf(documento.getIdDocumento())};
        Cursor c=db.query("documento", null,"idDocumento=?",id,null,null,null);
        if(c.moveToFirst()){
            Documento documento2=documentoConsultar(documento.getIdDocumento());
            String id2[]={String.valueOf(documento2.getIdDocumento())};
            contador=db.query("movimientoInventario",null ,"idDocumento=?", id2,null,null,null).getCount();

            if (contador==0){
                db.delete("documento","idDocumento=?",id);
                return mensaje=mensaje+String.valueOf(documento.getIdDocumento());
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El documento con el id: "+documento.getIdDocumento()+" no existe";
        }
    }
/*
    public String checarDependenciaDetalleDocumento(Documento documento){

        String idDocumento[] ={String.valueOf(documento.getIdDocumento())};
        int contador2=0;
        contador2=db.query("detalleDocumento",camposDetalleDocumento,"idDocumento=?", idDocumento,null,null,null).getCount();


    }*/

    public String eliminar2Documento(Documento documento){
        String mensaje = "Se elimino el documento con el id: "+documento.getIdDocumento();
        String id[]={String.valueOf(documento.getIdDocumento())};
        db.delete("documento","idDocumento=?",id);
        return mensaje;
    }

    public String actualizarDocumento(Documento documento, String clave){
        int  exito;
        String mensaje= "Se actualizo documento con id: ";
        String id[]={clave};

        Cursor c=db.query("documento", null,"idDocumento=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            /*
            "idDocumento", "isbnDocumento",
            "idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento","prestadoDocumento"};
             */
            v.put("idDocumento",documento.getIdDocumento());
            v.put("isbnDocumento",documento.getIsbnDocumento());
            v.put("idTipoDocumento",documento.getIdTipoDocumento());
            v.put("idEditorial",documento.getIdEditorial());
            v.put("codIdioma",documento.getCodIdioma());
            v.put("nombreDocumento",documento.getNombreDocumento());
            v.put("edicionDocumento",documento.getEdicionDocumento());
            v.put("publicacionDocumento",documento.getPublicacionDocumento());
            v.put("prestadoDocumento",documento.getPrestadoDocumento());
            v.put("autoresDocumento", documento.getAutoresDocumento());

            try {
                db.update("documento",v,"idDocumento=?",id);
                mensaje=mensaje+documento.getIdDocumento();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en un campo al actualizar";
            }
        }else {
            return mensaje="El documento con el nombre: "+documento.getIdDocumento()+" no existe";
        }
    }
    //TABLA DOCUMENTO

    //TABLA TIPOAUTOR

    public String insertarTipoAutor(TipoAutor tipoAutor){
        String mensaje= "Tipo de Autor insertado con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        v.put("nombreTipoAutor", tipoAutor.getNombreTipoAutor());
        inserto=db.insert("tipoAutor", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar Tipo de Autor, registro duplicado.";
        }else {
            mensaje=mensaje+tipoAutor.getNombreTipoAutor();
        }
        return mensaje;
    }

    public TipoAutor tipoAutorConsultar(String nombreTipoAutor){
        String[] id={nombreTipoAutor};
        Cursor c=db.query("tipoAutor", camposTipoAutor, "nombreTipoAutor=?", id, null, null, null);
        if(c.moveToFirst()){
            TipoAutor tipoAutor= new TipoAutor();
            //{"idMarca", "nombreMarca"}
            tipoAutor.setIdTipoAutor(c.getInt(0));
            tipoAutor.setNombreTipoAutor(c.getString(1));
            return tipoAutor;
        }else {
            return null;
        }
    }



    public String eliminar1TipoAutor(TipoAutor tipoAutor){
        String mensaje = "Se elimino el tipo de autor con el nombre: ";
        int contador=0;
        String id[]={tipoAutor.getNombreTipoAutor()};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("tipoAutor", null,"nombreTipoAutor=?",id,null,null,null);
        if(c.moveToFirst()){
            TipoAutor tipoAutor2=tipoAutorConsultar(tipoAutor.getNombreTipoAutor());
            String id2[]={String.valueOf(tipoAutor2.getIdTipoAutor())};
            //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo", "fechaEquipo", "fechaInactivoEquipo"};
            contador=db.query("autor",camposAutor,"idTipoAutor=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("tipoAutor","nombreTipoAutor=?",id);
                return mensaje=mensaje+tipoAutor.getNombreTipoAutor();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El tipo de autor con el nombre: "+tipoAutor.getNombreTipoAutor()+" no existe";
        }
    }

    public String eliminar2TipoAutor(TipoAutor tipoAutor){
        String mensaje = "Se elimino el tipo de autor con el nombre: "+tipoAutor.getNombreTipoAutor();
        String id[]={tipoAutor.getNombreTipoAutor()};
        db.delete("tipoAutor","nombreTipoAutor=?",id);
        return mensaje;
    }

    public String actualizarTipoAutor(TipoAutor tipoAutor, String clave){
        int  exito;
        String mensaje= "Se actualizo el tipo de autor con nombre: ";
        String id[]={clave};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("tipoAutor", null,"nombreTipoAutor=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            //{"idMarca", "nombreMarca"};
            v.put("idTipoAutor",tipoAutor.getIdTipoAutor());
            v.put("nombreTipoAutor",tipoAutor.getNombreTipoAutor());
            try {
                db.update("tipoAutor",v,"nombreTipoAutor=?",id);
                mensaje=mensaje+tipoAutor.getNombreTipoAutor();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="El tipo de autor con el nombre: "+tipoAutor.getNombreTipoAutor()+" no existe";
        }
    }
    //TABLA TIPOAUTOR

}
