package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.TipoEquipoAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

public class TipoEquipoMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_menu);
        ListView listView =(ListView)findViewById(R.id.listTiposEquipo);
        List<TipoEquipo> lista= listadoTiposEquipo();
        TipoEquipoAdaptador tipoEquipoAdaptador=new TipoEquipoAdaptador(this,lista);
        listView.setAdapter(tipoEquipoAdaptador);
    }


    public List<TipoEquipo> listadoTiposEquipo(){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        Cursor c =helper.obtenerTiposEquipo();
        List<TipoEquipo> tipoEquipoList= new ArrayList<>();
        while (c.moveToNext()){
            TipoEquipo tipoEquipo = new TipoEquipo();
            //{"idTipoEquipo", "nombreTipoEquipo"};

            tipoEquipo.setIdTipoEquipo(c.getInt(0));
            tipoEquipo.setNombreTipoEquipo(c.getString(1));
            tipoEquipoList.add(tipoEquipo);

        }
        return tipoEquipoList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
