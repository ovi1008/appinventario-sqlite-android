package edu.ues.fia.inventarioues.Modelos;

public class Horario {

    int idHorario;
    String tiempoHorario;

    public Horario() {
    }

    public Horario(int idHorario, String tiempoHorario) {
        this.idHorario = idHorario;
        this.tiempoHorario = tiempoHorario;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public String getTiempoHorario() {
        return tiempoHorario;
    }

    public void setTiempoHorario(String tiempoHorario) {
        this.tiempoHorario = tiempoHorario;
    }
}
