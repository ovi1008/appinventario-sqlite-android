package edu.ues.fia.inventarioues.Modelos;


public class Equipo {

    //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
    // "fechaEquipo", "fechaInactivoEquipo"};

    String serialEquipo, descripcionEquipo, fechaEquipo, fechaInactivoEquipo;;
    int idTipoEquipo, idMarca, estadoEquipo, prestamo;

    public Equipo() {
    }

    public Equipo(String serialEquipo, String descripcionEquipo, int idTipoEquipo, int idMarca, int estadoEquipo, String fechaEquipo, String fechaInactivoEquipo, int prestamo) {
        this.serialEquipo = serialEquipo;
        this.descripcionEquipo = descripcionEquipo;
        this.idTipoEquipo = idTipoEquipo;
        this.idMarca = idMarca;
        this.estadoEquipo = estadoEquipo;
        this.fechaEquipo = fechaEquipo;
        this.fechaInactivoEquipo = fechaInactivoEquipo;
        this.prestamo = prestamo;
    }

    public String getSerialEquipo() {
        return serialEquipo;
    }

    public void setSerialEquipo(String serialEquipo) {
        this.serialEquipo = serialEquipo;
    }

    public String getDescripcionEquipo() {
        return descripcionEquipo;
    }

    public void setDescripcionEquipo(String descripcionEquipo) {
        this.descripcionEquipo = descripcionEquipo;
    }

    public int getIdTipoEquipo() {
        return idTipoEquipo;
    }

    public void setIdTipoEquipo(int idTipoEquipo) {
        this.idTipoEquipo = idTipoEquipo;
    }

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public int getEstadoEquipo() {
        return estadoEquipo;
    }

    public void setEstadoEquipo(int estadoEquipo) {
        this.estadoEquipo = estadoEquipo;
    }

    public String getFechaEquipo() {
        return fechaEquipo;
    }

    public void setFechaEquipo(String fechaEquipo) {
        this.fechaEquipo = fechaEquipo;
    }

    public String getFechaInactivoEquipo() {
        return fechaInactivoEquipo;
    }

    public void setFechaInactivoEquipo(String fechaInactivoEquipo) {
        this.fechaInactivoEquipo = fechaInactivoEquipo;
    }

    public int getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(int prestamo) {
        this.prestamo = prestamo;
    }
}
