package edu.ues.fia.inventarioues.Modelos;

import java.util.Date;

public class Documento {

    /*

     private  static final String [] camposDocumento = new String[]{"idDocumento", "isbnDocumento",
    "idTipoDocumento","idEditorial","codIdioma","nombreDocumento","edicionDocumento","publicacionDocumento"};

     */
    int idDocumento;
    String isbnDocumento;
    int idTipoDocumento;
    int idEditorial;
    int codIdioma;
    String nombreDocumento;
    int edicionDocumento;
    String publicacionDocumento; //posible error.
    int prestadoDocumento;
    String autoresDocumento;

    public Documento() {
    }

    public Documento(int idDocumento, String isbnDocumento, int idTipoDocumento, int idEditorial, int codIdioma, String nombreDocumento, int edicionDocumento, String publicacionDocumento, int prestadoDocumento, String autoresDocumento) {
        this.idDocumento = idDocumento;
        this.isbnDocumento = isbnDocumento;
        this.idTipoDocumento = idTipoDocumento;
        this.idEditorial = idEditorial;
        this.codIdioma = codIdioma;
        this.nombreDocumento = nombreDocumento;
        this.edicionDocumento = edicionDocumento;
        this.publicacionDocumento = publicacionDocumento;
        this.prestadoDocumento = prestadoDocumento;
        this.autoresDocumento= autoresDocumento;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getIsbnDocumento() {
        return isbnDocumento;
    }

    public void setIsbnDocumento(String isbnDocumento) {
        this.isbnDocumento = isbnDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(int idEditorial) {
        this.idEditorial = idEditorial;
    }

    public int getCodIdioma() {
        return codIdioma;
    }

    public void setCodIdioma(int codIdioma) {
        this.codIdioma = codIdioma;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public int getEdicionDocumento() {
        return edicionDocumento;
    }

    public void setEdicionDocumento(int edicionDocumento) {
        this.edicionDocumento = edicionDocumento;
    }

    public String getPublicacionDocumento() {
        return publicacionDocumento;
    }

    public void setPublicacionDocumento(String publicacionDocumento) {
        this.publicacionDocumento = publicacionDocumento;
    }

    public int getPrestadoDocumento() {
        return prestadoDocumento;
    }

    public void setPrestadoDocumento(int prestadoDocumento) {
        this.prestadoDocumento = prestadoDocumento;
    }

    public String getAutoresDocumento() {
        return autoresDocumento;
    }

    public void setAutoresDocumento(String autoresDocumento) {
        this.autoresDocumento = autoresDocumento;
    }
}
