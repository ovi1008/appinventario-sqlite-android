package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

public class TipoDocumentoInsertarActivity extends AppCompatActivity {

    EditText editNombreTipoDocumento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_insertar);
        editNombreTipoDocumento=(EditText)findViewById(R.id.TipoDocumentoInsertarNombre);
    }

    public void insertarTipoDocumento(View v){
        String nombre, mensaje;
        TablasM helper= TablasM.getInstance(getApplicationContext());
        nombre=editNombreTipoDocumento.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.tipoDocumentoMsj1), Toast.LENGTH_LONG).show();
        }else{
            TipoDocumento tipoDocumento= new TipoDocumento();
            tipoDocumento.setNombreTipoDocumento(nombre);
            helper.open();
            mensaje=helper.insertarTipoDocumento(tipoDocumento);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreTipoDocumento.setText("");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}