package edu.ues.fia.inventarioues.Vistas.Materia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Materia.MateriaMenuActivity;

public class MateriaEditarActivity extends AppCompatActivity {
    EditText editId, editNombre, editBusca;
    String clave="";
    int opcion=0;
    Button busca;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_editar);
        editId = (EditText) findViewById(R.id.MateriaEditarId);
        editNombre = (EditText) findViewById(R.id.MateriaEditarNombre);
        editBusca = (EditText) findViewById(R.id.MateriaEditarSearch);
        busca = (Button) findViewById(R.id.MateriaEditarBtnConsultar);//buscar
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        } else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave = bundle.getString("id");
            opcion = 1;
            editBusca.setVisibility(View.INVISIBLE);
            busca.setVisibility(View.INVISIBLE);

        }
    }

    public void buscaMateriaEditar(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        Materia materia=helper.materiaConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (materia==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(materia.getIdMateria());
            editNombre.setText(materia.getNombreMateria());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }

    public void actualizarMateria(View v) {
        if (clave.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.msj3Estad), Toast.LENGTH_LONG).show();
        } else {
            TablasL helper = TablasL.getInstance(getApplicationContext());
            Materia materia = new Materia();
            materia.setIdMateria(editId.getText().toString().toUpperCase());
            materia.setNombreMateria(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje = helper.actualizarMateria(materia, clave.toUpperCase());
            helper.close();
            if (opcion == 1) {
                Intent intentEdi = new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intentEdi);
            } else {
                Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave ="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materia,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_materia_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_materia_menu_insertar:
                Intent intent0= new Intent(this, MateriaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_materia_menu_Editar:
                Intent intente= new Intent(this, MateriaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_materia_menu_consultar:
                Intent intent1= new Intent(this, MateriaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_materia_menu_eliminar:
                Intent intent2= new Intent(this, MateriaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_materia_listar:
                Intent intent3= new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
