package edu.ues.fia.inventarioues.Modelos;

public class Editorial {

    //    private  static final String [] camposEditorial
    //    = new String[]{"idEditorial", "nombreEditorial"};

    int idEditorial;
    String nombreEditorial;

    public Editorial() {
    }

    public Editorial(int idEditorial, String nombreEditorial) {
        this.idEditorial = idEditorial;
        this.nombreEditorial = nombreEditorial;
    }

    public int getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(int idEditorial) {
        this.idEditorial = idEditorial;
    }

    public String getNombreEditorial() {
        return nombreEditorial;
    }

    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }
}
