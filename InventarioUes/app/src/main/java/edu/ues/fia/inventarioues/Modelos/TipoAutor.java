package edu.ues.fia.inventarioues.Modelos;

public class TipoAutor {
    /*
        private static final String[] camposTipoAutor = new String[]{"idTipoAutor","nombreTipoAutor"};

     */

    int idTipoAutor;
    String nombreTipoAutor;

    public TipoAutor() {
    }

    public TipoAutor(int idTipoAutor, String nombreTipoAutor) {
        this.idTipoAutor = idTipoAutor;
        this.nombreTipoAutor = nombreTipoAutor;
    }

    public int getIdTipoAutor() {
        return idTipoAutor;
    }

    public void setIdTipoAutor(int idTipoAutor) {
        this.idTipoAutor = idTipoAutor;
    }

    public String getNombreTipoAutor() {
        return nombreTipoAutor;
    }

    public void setNombreTipoAutor(String nombreTipoAutor) {
        this.nombreTipoAutor = nombreTipoAutor;
    }
}
