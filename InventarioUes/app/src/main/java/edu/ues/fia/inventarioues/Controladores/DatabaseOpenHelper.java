package edu.ues.fia.inventarioues.Controladores;
import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DatabaseOpenHelper extends SQLiteAssetHelper {

    //Construccion de base
    private static final String DATABSE_NAME="inventarioUesVer0017.db";
    private static final int DATABASE_VERSION=1;

    public DatabaseOpenHelper(Context context){
        super(context, DATABSE_NAME, null, DATABASE_VERSION);
    }

}
