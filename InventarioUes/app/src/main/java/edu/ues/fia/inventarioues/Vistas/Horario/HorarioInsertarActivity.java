package edu.ues.fia.inventarioues.Vistas.Horario;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Equipo.EquipoInsertarActivity;

public class HorarioInsertarActivity extends AppCompatActivity {

    EditText editHorarioInicio, editHorarioFinal;

    Calendar calendar=Calendar.getInstance();
    Integer hora1=25, min1=61;
    String CERO = "0", DOS_PUNTOS = ":";
    boolean is24HourView=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_insertar);
        editHorarioInicio=(EditText)findViewById(R.id.HorarioInsertarHoraInicio);
        editHorarioFinal=(EditText)findViewById(R.id.HorarioInsertarHoraFinal);

        editHorarioInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(HorarioInsertarActivity.this,inicioHora, calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),is24HourView).show();
            }
        });

        editHorarioFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(HorarioInsertarActivity.this,inicioFinal, calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),is24HourView).show();
            }
        });
    }

    TimePickerDialog.OnTimeSetListener inicioHora= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            hora1=hourOfDay;
            min1=minute;
            //Formateo el hora obtenido: antepone el 0 si son menores de 10
            String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
            //Formateo el minuto obtenido: antepone el 0 si son menores de 10
            String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
            //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
            String AM_PM;
            if(hourOfDay < 12) {
                AM_PM = "a.m.";
            } else {
                AM_PM = "p.m.";
            }
            editHorarioInicio.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
        }
    };

    TimePickerDialog.OnTimeSetListener inicioFinal= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            //Formateo el hora obtenido: antepone el 0 si son menores de 10
            String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
            //Formateo el minuto obtenido: antepone el 0 si son menores de 10
            String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
            //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
            String AM_PM;
            if(hourOfDay < 12) {
                AM_PM = "a.m.";
            } else {
                AM_PM = "p.m.";
            }
            editHorarioFinal.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            if (hourOfDay<=hora1){
                if (hourOfDay==hora1){
                    if (minute<=min1){
                        editHorarioFinal.setText("");
                    }else {}
                }else {
                    editHorarioFinal.setText("");
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario,menu);
        return true;
    }


    public void insertarHorario(View v){
        String inicio, hfinal, mensaje, nombre;
        nombre=editHorarioInicio.getText().toString()+"-"+editHorarioFinal.getText().toString();
        TablasM helper= TablasM.getInstance(getApplicationContext());
        inicio=editHorarioInicio.getText().toString().toUpperCase();
        hfinal=editHorarioFinal.getText().toString().toUpperCase();
        if(inicio.isEmpty() || hfinal.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.horarioMsj1), Toast.LENGTH_LONG).show();
        }else{
            Horario horario= new Horario();
            horario.setTiempoHorario(nombre);
            helper.open();
            mensaje=helper.insertarHorario(horario);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editHorarioInicio.setText("");
            editHorarioFinal.setText("");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_horario_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_horario_menu_insertar:
                Intent intent0= new Intent(this, HorarioInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_horario_menu_consultar:
                Intent intent1= new Intent(this, HorarioConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_horario_menu_Editar:
                Intent intente= new Intent(this, HorarioEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_horario_menu_eliminar:
                Intent intent2= new Intent(this, HorarioEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_horario_listar:
                Intent intent3= new Intent(this, HorarioMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
