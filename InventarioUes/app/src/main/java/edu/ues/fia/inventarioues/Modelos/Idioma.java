package edu.ues.fia.inventarioues.Modelos;

public class Idioma {
    //{"codIdioma", "nombreIdioma"};
    int codIdioma;
    String nombreIdioma;

    public Idioma() {
    }

    public int getCodIdioma() {
        return codIdioma;
    }

    public void setCodIdioma(int codIdioma) {
        this.codIdioma = codIdioma;
    }

    public String getNombreIdioma() {
        return nombreIdioma;
    }

    public void setNombreIdioma(String nombreIdioma) {
        this.nombreIdioma = nombreIdioma;
    }
}
