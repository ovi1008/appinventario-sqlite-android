package edu.ues.fia.inventarioues.Vistas.Marca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

public class MarcaEliminarActivity extends AppCompatActivity {

    EditText editNombreMarca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_eliminar);
        editNombreMarca=(EditText)findViewById(R.id.MarcaEliminarNombre1);
    }

    public void eliminarDeleteMarca(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (editNombreMarca.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.marcaMsj1),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Marca marca= new Marca();
            marca.setNombreMarca(editNombreMarca.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Marca(marca);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,MarcaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", marca.getNombreMarca());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombreMarca.setText("");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
