package edu.ues.fia.inventarioues.Vistas.Motivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.R;

public class MotivoEliminarAdvActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivo_eliminar_adv);
        msj=(TextView)findViewById(R.id.MotivoEliminarAdvMens);

        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.motivoMsj2)+" "+
                +bundle.getInt("nombre")+
                "\n"+getResources().getString(R.string.motivoMsj3)+" "+
                bundle.getString("msj"));
    }

    public void eliminarAdvMotivo(View v){
        Bundle bundle=getIntent().getExtras();
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        Motivo motivo= new Motivo();
        motivo.setIdMotivo(bundle.getInt("nombre"));
        String mensaje= helper.eliminar2Motivo(motivo);
        helper.close();
        Intent intentM= new Intent(this,MotivoMenuActivity.class);
        this.startActivity(intentM);
    }

    public void eliminarCancelarMotivo(View v){
        Intent intentCancelar = new Intent(this, MotivoMenuActivity.class);
        this.startActivity(intentCancelar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_motivo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_motivo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_motivo_menu_insertar:
                Intent intent0= new Intent(this, MotivoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_motivo_menu_consultar:
                Intent intent1= new Intent(this, MotivoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_motivo_menu_Editar:
                Intent intente= new Intent(this, MotivoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_motivo_menu_eliminar:
                Intent intent2= new Intent(this, MotivoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_motivo_listar:
                Intent intent3= new Intent(this, MotivoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
