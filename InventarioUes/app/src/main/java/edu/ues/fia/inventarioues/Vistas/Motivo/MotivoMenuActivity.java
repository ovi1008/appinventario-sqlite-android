package edu.ues.fia.inventarioues.Vistas.Motivo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.MotivoAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.R;

public class MotivoMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivo_menu);
        ListView listView =(ListView)findViewById(R.id.listMotivo);
        List<Motivo> lista= listadoMotivos();
        MotivoAdaptador motivoAdaptador=new MotivoAdaptador(this,lista);
        listView.setAdapter(motivoAdaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_motivo,menu);
        return true;
    }

    public List<Motivo> listadoMotivos(){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        Cursor c =helper.obtenerMotivos();
        List<Motivo> motivosList= new ArrayList<>();
        while (c.moveToNext()){
            Motivo motivo=new Motivo();
            //camposMotivo= new String[]{"idMotivo", "nombreMotivo"};
            motivo.setIdMotivo(c.getInt(0));
            motivo.setNombreMotivo(c.getString(1));
            motivosList.add(motivo);
        }
        return motivosList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_motivo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_motivo_menu_insertar:
                Intent intent0= new Intent(this, MotivoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_motivo_menu_consultar:
                Intent intent1= new Intent(this, MotivoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_motivo_menu_Editar:
                Intent intente= new Intent(this, MotivoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_motivo_menu_eliminar:
                Intent intent2= new Intent(this, MotivoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_motivo_listar:
                Intent intent3= new Intent(this, MotivoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
