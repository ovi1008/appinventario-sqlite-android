package edu.ues.fia.inventarioues.Modelos;

public class TipoEquipo {
    //"idTipoEquipo", "nombreTipoEquipo"
    int idTipoEquipo;
    String nombreTipoEquipo;

    public TipoEquipo() {
    }

    public TipoEquipo(int idTipoEquipo, String nombreTipoEquipo) {
        this.idTipoEquipo = idTipoEquipo;
        this.nombreTipoEquipo = nombreTipoEquipo;
    }

    public int getIdTipoEquipo() {
        return idTipoEquipo;
    }

    public void setIdTipoEquipo(int idTipoEquipo) {
        this.idTipoEquipo = idTipoEquipo;
    }

    public String getNombreTipoEquipo() {
        return nombreTipoEquipo;
    }

    public void setNombreTipoEquipo(String nombreTipoEquipo) {
        this.nombreTipoEquipo = nombreTipoEquipo;
    }
}
