package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.Intent;
import android.gesture.Prediction;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaEditarActivity;

public class PrestamoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
    Integer vista;
    SearchView searchBuscar;
    TextView txtEstado;
    EditText editId, editTipo, editDocente, editFechaInicio, editFechaFinal, editMotivo,
            editHorario, editUnidad, editEquipo, editDocumento, editMateria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_consultar);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");
        searchBuscar=findViewById(R.id.Prestamo_searchConsultar);
        txtEstado=findViewById(R.id.PrestamoConsultarEstado);
        editId=findViewById(R.id.PrestamoConsultarId);
        editTipo=findViewById(R.id.PrestamoConsultarTipo);
        editDocente=findViewById(R.id.PrestamoConsultarDocente);
        editFechaInicio=findViewById(R.id.PrestamoConsultarFechaInicio);
        editFechaFinal=findViewById(R.id.PrestamoConsultarFechaFinal);
        editMotivo=findViewById(R.id.PrestamoConsultarMotivo);
        editHorario=findViewById(R.id.PrestamoConsultarHorario);
        editUnidad=findViewById(R.id.PrestamoConsultarUnidad);
        editEquipo=findViewById(R.id.PrestamoConsultarEquipo);
        editDocumento=findViewById(R.id.PrestamoConsultarDocumento);
        editMateria=findViewById(R.id.PrestamoConsultarMateria);

        editId.setText("");
        searchBuscar.setOnQueryTextListener(this);

        if (vista==1){
            editEquipo.setVisibility(View.INVISIBLE);
            TextView titulo=findViewById(R.id.PrestamoConsultarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud2P1));
        }else {
            editDocumento.setVisibility(View.INVISIBLE);
            TextView titulo=findViewById(R.id.PrestamoConsultarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud2P2));
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (newText.isEmpty()){

        }else {
            TablasJ helperJ= TablasJ.getInstance(getApplicationContext());
            TablasV helperV= TablasV.getInstance(getApplicationContext());
            TablasL helperL= TablasL.getInstance(getApplicationContext());
            TablasM helperM= TablasM.getInstance(getApplicationContext());
            Prestamo prestamo= new Prestamo();
            helperJ.open();
            prestamo=helperJ.prestamoConsultar(Integer.valueOf(newText));
            helperJ.close();
            if (prestamo==null){
                editId.setText("");
                editTipo.setText("");
                editDocente.setText("");
                editFechaInicio.setText("");
                editFechaFinal.setText("");
                editMotivo.setText("");
                editHorario.setText("");
                editUnidad.setText("");
                editEquipo.setText("");
                editDocumento.setText("");
                editMateria.setText("");
                txtEstado.setText(getResources().getString(R.string.prestamoCampo12));
                txtEstado.setTextColor(Color.rgb(164,164,164));

            }
            else {
                if (vista==1){
                    if (prestamo.getSerialEquipo()==null){
                        if (prestamo.getEstadoMovInv()==1){
                            txtEstado.setText(getResources().getString(R.string.prestamoCampo12e1));
                            txtEstado.setTextColor(Color.rgb(100,221,23));
                        }
                        else {
                            txtEstado.setText(getResources().getString(R.string.prestamoCampo12e2));
                            txtEstado.setTextColor(Color.rgb(213,0,0));
                        }
                        editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                        helperM.open();
                        TipoMovimiento tipo=helperM.tipoMovimientoConsultar(String.valueOf(prestamo.getIdTipoMovimiento()));
                        helperM.close();
                        editTipo.setText(tipo.getNombreTipoMovimiento());
                        helperM.open();
                        Docente docente=helperM.docenteConsultar(prestamo.getCarnetDocente());
                        helperM.close();
                        editDocente.setText(docente.getNombreDocente()+" "+docente.getApellidoDocente());
                        editFechaInicio.setText(prestamo.getFechaMovimientoInventario());
                        editFechaFinal.setText(prestamo.getFechaDevolucionMovInv());
                        helperM.open();
                        Motivo motivo=helperM.motivoConsultar(String.valueOf(prestamo.getIdMotivo()));
                        helperM.close();
                        editMotivo.setText(motivo.getNombreMotivo());
                        if (prestamo.getIdHorario() instanceof Integer){
                            editHorario.setText("");
                        }else {
                            helperM.open();
                            Horario horario=helperM.horarioConsultar(String.valueOf(prestamo.getIdHorario()));
                            helperM.close();
                            editHorario.setText(horario.getTiempoHorario());
                        }
                        if (prestamo.getIdUnidadAdministrativa() instanceof Integer){
                            editUnidad.setText("");
                        }else {
                            helperJ.open();
                            UnidadAdministrativa unidad = helperJ.unidadActual(prestamo.getIdUnidadAdministrativa());
                            helperJ.close();
                            editUnidad.setText(unidad.getNombreUnidadAdministrativa());
                        }
                        editEquipo.setText(prestamo.getSerialEquipo());
                        helperV.open();
                        Documento documento=helperV.documentoConsultar(prestamo.getIdDocumento());
                        helperV.close();
                        editDocumento.setText(documento.getIdDocumento()+"-"+documento.getNombreDocumento());
                        if (prestamo.getIdMateria()==null){
                            editMateria.setText("");
                        }else {
                            helperL.open();
                            Materia materia=helperL.materiaConsultar(prestamo.getIdMateria());
                            helperL.close();
                            editMateria.setText(materia.getNombreMateria());
                        }

                    }else {//nada
                        }
                }else {
                    if (prestamo.getSerialEquipo()==null){
                        //nada
                    }else {
                        if (prestamo.getEstadoMovInv()==1){
                            txtEstado.setText(getResources().getString(R.string.prestamoCampo12e1));
                            txtEstado.setTextColor(Color.rgb(100,221,23));
                        }
                        else {
                            txtEstado.setText(getResources().getString(R.string.prestamoCampo12e2));
                            txtEstado.setTextColor(Color.rgb(213,0,0));
                        }
                        editId.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
                        helperM.open();
                        TipoMovimiento tipo=helperM.tipoMovimientoConsultar(String.valueOf(prestamo.getIdTipoMovimiento()));
                        helperM.close();
                        editTipo.setText(tipo.getNombreTipoMovimiento());
                        helperM.open();
                        Docente docente=helperM.docenteConsultar(prestamo.getCarnetDocente());
                        helperM.close();
                        editDocente.setText(docente.getNombreDocente()+" "+docente.getApellidoDocente());
                        editFechaInicio.setText(prestamo.getFechaMovimientoInventario());
                        editFechaFinal.setText(prestamo.getFechaDevolucionMovInv());
                        helperM.open();
                        Motivo motivo=helperM.motivoConsultar(String.valueOf(prestamo.getIdMotivo()));
                        helperM.close();
                        editMotivo.setText(motivo.getNombreMotivo());
                        if (prestamo.getIdHorario() instanceof Integer){
                            editHorario.setText("");
                        }else {
                            helperM.open();
                            Horario horario=helperM.horarioConsultar(String.valueOf(prestamo.getIdHorario()));
                            helperM.close();
                            editHorario.setText(horario.getTiempoHorario());
                        }
                        if (prestamo.getIdUnidadAdministrativa() instanceof Integer){
                            editUnidad.setText("");
                        }else {
                            helperJ.open();
                            UnidadAdministrativa unidad = helperJ.unidadActual(prestamo.getIdUnidadAdministrativa());
                            helperJ.close();
                            editUnidad.setText(unidad.getNombreUnidadAdministrativa());
                        }
                        helperJ.open();
                        Equipo equipo=helperJ.equipoConsultar(prestamo.getSerialEquipo());
                        TipoEquipo tipoE=helperJ.tipoEquipoConsultarActual(equipo.getIdTipoEquipo());
                        helperJ.close();
                        editEquipo.setText(prestamo.getSerialEquipo()+"-"+tipoE.getNombreTipoEquipo());
                        editDocumento.setText(String.valueOf(prestamo.getIdDocumento()));

                        if (prestamo.getIdMateria()==null){
                            editMateria.setText("");
                        }else {
                            helperL.open();
                            Materia materia=helperL.materiaConsultar(prestamo.getIdMateria());
                            helperL.close();
                            editMateria.setText(materia.getNombreMateria());
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void prestamoConsultarEditar(View v){
        if (editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, PrestamoEditarActivity.class);
            intentEditar.putExtra("vista", vista);
            intentEditar.putExtra("idPrestamo",Integer.valueOf(editId.getText().toString()));
            this.startActivity(intentEditar);
        }
    }

    public void prestamoConsultarEliminar(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Prestamo prestamoVista=helper.prestamoConsultar(Integer.valueOf(editId.getText().toString()));
            helper.close();

            if (vista==1){
                if (prestamoVista.getSerialEquipo()==null){
                    Prestamo prestamo= new Prestamo();
                    prestamo.setIdMovimientoInventario(Integer.valueOf(editId.getText().toString()));
                    helper.open();
                    String mensaje= helper.eliminarPrestamo(prestamo);
                    helper.close();
                    Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                }else {
                    String mensaje=getResources().getString(R.string.msjEsPrestamo02);
                    Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                }
            }
            else{
                if (prestamoVista.getSerialEquipo()==null){
                    String mensaje=getResources().getString(R.string.msjEsPrestamo01);
                    Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                }
                Prestamo prestamo= new Prestamo();
                prestamo.setIdMovimientoInventario(Integer.valueOf(editId.getText().toString()));
                helper.open();
                String mensaje= helper.eliminarPrestamo(prestamo);
                helper.close();
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
            editId.setText("");
        }
    }
    public void funcionLimpiar(View v){
        editId.setText("");
        editTipo.setText("");
        editDocente.setText("");
        editFechaInicio.setText("");
        editFechaFinal.setText("");
        editMotivo.setText("");
        editHorario.setText("");
        editUnidad.setText("");
        editEquipo.setText("");
        editDocumento.setText("");
        editMateria.setText("");
        txtEstado.setText(getResources().getString(R.string.prestamoCampo12));
        txtEstado.setTextColor(Color.rgb(164,164,164));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
