package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;

public class EditorialEliminarAdvertenciaActivity extends AppCompatActivity {
    TextView msj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_eliminar_advertencia);

        msj=(TextView)findViewById(R.id.EditorialEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        msj.setText(getResources().getString(R.string.editorialMsj2)+" "+
                bundle.getString("nombre")+"\n"+getResources().getString(R.string.editorialMsj3)+" "+
                bundle.getString("msj"));
    }

    public void eliminarAdvEditorial(View v){
        Bundle bundle=getIntent().getExtras();
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        Editorial editorial= new Editorial();
        editorial.setNombreEditorial(bundle.getString("nombre"));
        String mensaje= helper.eliminar2Editorial(editorial);
        helper.close();
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
        Intent intentM= new Intent(this,EditorialMenuActivity.class);
        this.startActivity(intentM);
    }

    public void eliminarCancelarEditorial(View v){
        Intent intentCancelar = new Intent(this, EditorialMenuActivity.class);
        this.startActivity(intentCancelar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
