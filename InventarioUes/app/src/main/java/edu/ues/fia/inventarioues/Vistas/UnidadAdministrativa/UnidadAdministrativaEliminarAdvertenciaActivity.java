package edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoMenuActivity;

public class UnidadAdministrativaEliminarAdvertenciaActivity extends AppCompatActivity {
    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unidad_administrativa_eliminar_advertencia);
        msj=(TextView)findViewById(R.id.UnidadAdministrativaEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.unidadMsj2)+" "+
                bundle.getString("nombre")+
                "\n"+getResources().getString(R.string.unidadMsj3)+" "
                +bundle.getString("msj"));
    }

    public void eliminarAdvUnidadAdministrativa(View v){
        Bundle bundle=getIntent().getExtras();
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
        unidadAdministrativa.setNombreUnidadAdministrativa(bundle.getString("nombre"));
        String mensaje= helper.eliminar2UnidadAdministrativa(unidadAdministrativa);
        helper.close();
        Intent intentM= new Intent(this, UnidadAdministrativaMenuActivity.class);
        this.startActivity(intentM);
    }
    public void elimincarCancelarUnidadAdministrativa(View v){
        Intent intentCancelar = new Intent(this, UnidadAdministrativaMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unidad_administrativa,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_unidad_administrativa_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_unidad_administrativa_menu_insertar:
                Intent intent0= new Intent(this, UnidadAdministrativaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_unidad_administrativa_menu_consultar:
                Intent intent1= new Intent(this, UnidadAdministrativaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_unidad_administrativa_menu_Editar:
                Intent intente= new Intent(this, UnidadAdministrativaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_unidad_administrativa_menu_eliminar:
                Intent intent2= new Intent(this, UnidadAdministrativaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_unidad_administrativa_listar:
                Intent intent3= new Intent(this, UnidadAdministrativaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
