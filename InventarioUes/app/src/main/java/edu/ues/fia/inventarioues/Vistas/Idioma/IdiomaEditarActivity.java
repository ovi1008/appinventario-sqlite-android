package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;

public class IdiomaEditarActivity extends AppCompatActivity {
    EditText editId, editNombre, editBusca;
    String clave="";
    Button busca;
    int opcion=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_editar);
        editId=(EditText)findViewById(R.id.IdiomaEditarId);
        editNombre=(EditText)findViewById(R.id.IdiomaEditarNombre);
        editBusca=(EditText) findViewById(R.id.IdiomaEditarSearch);
        busca=(Button)findViewById(R.id.IdiomaEditarBtnBuscar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);

        }
    }

    public void actualizarIdioma(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasL helper= TablasL.getInstance(getApplicationContext());
            Idioma idioma = new Idioma();
            idioma.setCodIdioma(Integer.valueOf(editId.getText().toString()));
            idioma.setNombreIdioma(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarIdioma(idioma, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }
    public void buscarIdiomaEditar(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        Idioma idioma=helper.idiomaConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (idioma==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(idioma.getCodIdioma()));
            editNombre.setText(idioma.getNombreIdioma());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
