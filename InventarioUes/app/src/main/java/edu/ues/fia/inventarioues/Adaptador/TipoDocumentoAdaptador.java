package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

public class TipoDocumentoAdaptador extends ArrayAdapter<TipoDocumento> {

    public TipoDocumentoAdaptador(@NonNull Context context, @NonNull List<TipoDocumento> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_tipo_documento,parent,false);
        }
        TextView idTipoDocumento=convertView.findViewById(R.id.ListarTipoDocumentoId);
        TextView nombreTipoDocumento=convertView.findViewById(R.id.ListarTipoDocumentoNombre);

        TipoDocumento tipoDocumento=getItem(position);
        idTipoDocumento.setText(String.valueOf(tipoDocumento.getIdTipoDocumento()));
        nombreTipoDocumento.setText(tipoDocumento.getNombreTipoDocumento());

        return convertView;
    }

}
