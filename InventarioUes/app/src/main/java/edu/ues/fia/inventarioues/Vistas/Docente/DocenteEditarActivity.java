package edu.ues.fia.inventarioues.Vistas.Docente;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;

public class DocenteEditarActivity extends AppCompatActivity {

    EditText editCarnet, editNombre, editApellido, editTelefono, editDireccion, editBusca;
    String clave="";
    Button busca;
    int opcion=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docente_editar);
        editCarnet=(EditText)findViewById(R.id.DocenteEditarId);
        editNombre=(EditText)findViewById(R.id.DocenteEditarNombre);
        editApellido=(EditText)findViewById(R.id.DocenteEditarApellido);
        editTelefono=(EditText)findViewById(R.id.DocenteEditarTelefono);
        editDireccion=(EditText)findViewById(R.id.DocenteEditarDireccion);
        editBusca=(EditText) findViewById(R.id.DocenteEditarSearch);
        busca=(Button)findViewById(R.id.DocenteEditarBtnConsultar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editCarnet.setEnabled(false);
            editNombre.setEnabled(false);
            editApellido.setEnabled(false);
            editTelefono.setEnabled(false);
            editDireccion.setEnabled(false);
        }else {
            editCarnet.setText(bundle.getString("carnet"));
            editNombre.setText(bundle.getString("nombre"));
            editApellido.setText(bundle.getString("apellido"));
            editTelefono.setText(bundle.getString("telefono"));
            editDireccion.setText(bundle.getString("direccion"));
            clave=bundle.getString("carnet");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);
        }

    }

    public void actualizarDocente(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper= TablasM.getInstance(getApplicationContext());
            Docente docente = new Docente();
            docente.setCarnetDocente(editCarnet.getText().toString().toUpperCase());
            docente.setNombreDocente(editNombre.getText().toString().toUpperCase());
            docente.setApellidoDocente(editApellido.getText().toString().toUpperCase());
            docente.setTelefonoDocente(editTelefono.getText().toString().toUpperCase());
            docente.setDireccionDocente(editDireccion.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarDocente(docente, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editCarnet.setText("");
                editNombre.setText("");
                editApellido.setText("");
                editTelefono.setText("");
                editDireccion.setText("");
                editBusca.setText("");
                clave="";
                editCarnet.setEnabled(false);
                editNombre.setEnabled(false);
                editApellido.setEnabled(false);
                editTelefono.setEnabled(false);
                editDireccion.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    public void buscarMotivoEditar(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        Docente docente=helper.docenteConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (docente==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editCarnet.setText(docente.getCarnetDocente());
            editNombre.setText(docente.getNombreDocente());
            editApellido.setText(docente.getApellidoDocente());
            editTelefono.setText(docente.getTelefonoDocente());
            editDireccion.setText(docente.getDireccionDocente());
            editCarnet.setEnabled(true);
            editNombre.setEnabled(true);
            editApellido.setEnabled(true);
            editTelefono.setEnabled(true);
            editDireccion.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docente,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_docente_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_docente_menu_insertar:
                Intent intent0= new Intent(this, DocenteInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_docente_menu_Editar:
                Intent intente= new Intent(this, DocenteEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_docente_menu_consultar:
                Intent intent1= new Intent(this, DocenteBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_docente_menu_eliminar:
                Intent intent2= new Intent(this, DocenteEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_docente_listar:
                Intent intent3= new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
