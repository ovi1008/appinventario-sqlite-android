package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.R;

public class PrestamoInsertarActivity extends AppCompatActivity {
    Integer vista;
    Spinner spTipoMov, spDocente, spMateria, spMotivo, spHorario, spUnidad, spEquipo, spDocumento;
    Integer valorSelectTipoMov, valorSelectMotivo=null, valorSelectHorario=null, valorSelectUnidad=null,
            valorSelectDocumento=null;
    String valorSelectDocente, valorSelectMateria=null, valorSelectEquipo=null, nombreAdaptativo;
    Calendar calendar=Calendar.getInstance();
    String CERO = "0", DOS_PUNTOS = ":";
    boolean is24HourView=true;
    int hourOfDay, minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_insertar);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");
        if (vista==1){
            nombreAdaptativo="DOCUMENTO";
            TextView titulo=findViewById(R.id.PrestamoInsertarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud1P1));
        }else {
            nombreAdaptativo="EQUIPO";
            TextView titulo=findViewById(R.id.PrestamoInsertarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud1P2));
        }

        spTipoMov=findViewById(R.id.PrestamoInsertarTipoMovimiento);
        spDocente=findViewById(R.id.PrestamoInsertarDocente);
        spMateria=findViewById(R.id.PrestamoInsertarMateria);
        spMotivo=findViewById(R.id.PrestamoInsertarMotivo);
        spHorario=findViewById(R.id.PrestamoInsertarHorario);
        spUnidad=findViewById(R.id.PrestamoInsertarUnidad);
        spEquipo=findViewById(R.id.PrestamoInsertarEquipo);
        spDocumento=findViewById(R.id.PrestamoInsertarDocumento);

        TablasJ helperJ= TablasJ.getInstance(getApplicationContext());
        TablasV helperV= TablasV.getInstance(getApplicationContext());
        TablasM helperM= TablasM.getInstance(getApplicationContext());
        TablasL helperL= TablasL.getInstance(getApplicationContext());
        helperJ.open();

        Cursor tipoMov=helperM.obtenerTiposMovimiento();
        final List<Integer> tipoMovIdList = new ArrayList<>();
        tipoMovIdList.add(0);
        List<String> tipoMovNombreList = new ArrayList<>();
        tipoMovNombreList.add(getResources().getString(R.string.prestamoCampo2S));
        while (tipoMov.moveToNext()){
            tipoMovIdList.add(tipoMov.getInt(0));
            tipoMovNombreList.add(tipoMov.getString(1));
        }
        spTipoMov.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,tipoMovIdList));
        spTipoMov.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,tipoMovNombreList));
        spTipoMov.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipoMov=null;
                }else {
                    valorSelectTipoMov=tipoMovIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoMov=null;

            }
        });

        Cursor docente=helperM.obtenerDocentes();
        final List<String> docenteIdList = new ArrayList<>();
        docenteIdList.add("0");
        List<String> docenteNombreList = new ArrayList<>();
        docenteNombreList.add(getResources().getString(R.string.prestamoCampo3s));
        while (docente.moveToNext()){
            docenteIdList.add(docente.getString(0));
            docenteNombreList.add(docente.getString(1)+" "+docente.getString(2));
        }
        spDocente.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,docenteIdList));
        spDocente.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,docenteNombreList));
        spDocente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectDocente=null;
                }else {
                    valorSelectDocente=docenteIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectDocente=null;

            }
        });

        Cursor materia=helperL.obtenerMaterias();
        final List<String> materiaIdList = new ArrayList<>();
        materiaIdList.add("0");
        List<String> materiaNombreList = new ArrayList<>();
        materiaNombreList.add(getResources().getString(R.string.prestamoCampo4s));
        while (materia.moveToNext()){
            materiaIdList.add(materia.getString(0));
            materiaNombreList.add(materia.getString(1));
        }
        spMateria.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,materiaIdList));
        spMateria.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,materiaNombreList));
        spMateria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMateria=null;
                }else {
                    valorSelectMateria=materiaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMateria=null;

            }
        });

        Cursor motivo=helperM.obtenerMotivos();
        final List<Integer> motivoIdList = new ArrayList<>();
        motivoIdList.add(0);
        List<String> motivoNombreList = new ArrayList<>();
        motivoNombreList.add(getResources().getString(R.string.prestamoCampo5s));
        while (motivo.moveToNext()){
            motivoIdList.add(motivo.getInt(0));
            motivoNombreList.add(motivo.getString(1));
        }
        spMotivo.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,motivoIdList));
        spMotivo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,motivoNombreList));
        spMotivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMotivo=null;
                }else {
                    valorSelectMotivo=motivoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMotivo=null;

            }
        });

        Cursor horario=helperM.obtenerHorarios();
        final List<Integer> horarioIdList = new ArrayList<>();
        horarioIdList.add(0);
        List<String> horarioNombreList = new ArrayList<>();
        horarioNombreList.add(getResources().getString(R.string.prestamoCampo6s));
        while (horario.moveToNext()){
            horarioIdList.add(horario.getInt(0));
            horarioNombreList.add(horario.getString(1));
        }
        spHorario.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,horarioIdList));
        spHorario.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,horarioNombreList));
        spHorario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectHorario=null;
                }else {
                    valorSelectHorario=horarioIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectHorario=null;

            }
        });

        Cursor unidad=helperL.obtenerUnidadesAdministrativas();
        final List<Integer> unidadIdList = new ArrayList<>();
        unidadIdList.add(0);
        List<String> unidadNombreList = new ArrayList<>();
        unidadNombreList.add(getResources().getString(R.string.prestamoCampo7s));
        while (unidad.moveToNext()){
            unidadIdList.add(unidad.getInt(0));
            unidadNombreList.add(unidad.getString(1));
        }
        spUnidad.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,unidadIdList));
        spUnidad.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,unidadNombreList));
        spUnidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectUnidad=null;
                }else {
                    valorSelectUnidad=unidadIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectUnidad=null;

            }
        });

        Cursor documento=helperJ.obtenerDocumentoDisponibles();
        final List<Integer> documentoIdList = new ArrayList<>();
        documentoIdList.add(0);
        List<String> documentoNombreList = new ArrayList<>();
        documentoNombreList.add(getResources().getString(R.string.prestamoCampo8s));
        while (documento.moveToNext()){
            documentoIdList.add(documento.getInt(0));
            documentoNombreList.add(documento.getString(0)+"-"+documento.getString(5));
        }
        spDocumento.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,documentoIdList));
        spDocumento.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,documentoNombreList));
        spDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectDocumento=null;
                }else {
                    valorSelectDocumento=documentoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectDocumento=null;

            }
        });

        Cursor equipo=helperJ.obtenerEquipoDisponibles();
        final List<String> equipoIdList = new ArrayList<>();
        equipoIdList.add("0");
        List<String> equipoNombreList = new ArrayList<>();
        Cursor tipo=helperJ.obtenerTiposEquipo();
        final List<Integer> tipoIdList = new ArrayList<>();
        List<String> tipoNombreList = new ArrayList<>();
        while (tipo.moveToNext()){
            tipoIdList.add(tipo.getInt(0));
            tipoNombreList.add(tipo.getString(1));
        }

        equipoNombreList.add(getResources().getString(R.string.prestamoCampo9s));
        while (equipo.moveToNext()){
            String nombreT;
            Integer lugar=0;
            equipoIdList.add(equipo.getString(0));
            for (int t:tipoIdList){
                if (equipo.getInt(1)==t){
                    nombreT=tipoNombreList.get(lugar);
                    equipoNombreList.add(equipo.getString(0)+"-"+nombreT);
                }
                lugar++;
            }

        }
        spEquipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,equipoIdList));
        spEquipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,equipoNombreList));
        spEquipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectEquipo=null;
                }else {
                    valorSelectEquipo=equipoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectEquipo=null;

            }
        });

        helperJ.close();


        if (vista==1){
            spEquipo.setEnabled(true);
            spEquipo.setVisibility(View.INVISIBLE);
        }else {
            spDocumento.setEnabled(false);
            spDocumento.setVisibility(View.INVISIBLE);
        }


    }

    public String fechaActual(){
        String fecha;
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.US);
        fecha=format.format(calendar.getTime());
        return  fecha;
    }

    public Integer validar(){
        Integer valido;
        if (vista==1){
            if (!(valorSelectTipoMov instanceof Integer) || !(valorSelectDocente instanceof String) || !(valorSelectMotivo instanceof Integer) || !(valorSelectDocumento instanceof Integer)){
                return valido=0;
            }else {
                return valido=1;
            }
        }else {
            if (!(valorSelectTipoMov instanceof Integer) || !(valorSelectDocente instanceof String) || !(valorSelectMotivo instanceof Integer)|| !(valorSelectEquipo instanceof String)){
                return valido=0;
            }else {
                return valido=1;
            }
        }
    }

    public String horaActual(){
        String hora;
        hourOfDay=calendar.get(Calendar.HOUR_OF_DAY);
        minute=calendar.get(Calendar.MINUTE);
        //Formateo el hora obtenido: antepone el 0 si son menores de 10
        String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
        //Formateo el minuto obtenido: antepone el 0 si son menores de 10
        String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
        //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
        String AM_PM;
        if(hourOfDay < 12) {
            AM_PM = "a.m.";
        } else {
            AM_PM = "p.m.";
        }
        hora=horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM;
        return hora;
    }

    public void ingresarPrestamo(View v){
        String fechaYhora=fechaActual()+"-"+horaActual();
        String error, mensaje;
        Integer verificacion=validar();

        //Integer valorSelectTipoMov, valorSelectMotivo, valorSelectHorario=null, valorSelectUnidad=null,
        // valorSelectDocumento=null;
        //String valorSelectDocente, valorSelectMateria=null, valorSelectEquipo=null;

        if (verificacion==0){
            if (vista==1){
                Toast.makeText(this, getResources().getString(R.string.prestamoMsj1P1), Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(this, getResources().getString(R.string.prestamoMsj1P2), Toast.LENGTH_LONG).show();
            }

        }else {
            Prestamo prestamo = new Prestamo();
            prestamo.setIdTipoMovimiento(valorSelectTipoMov);
            prestamo.setCarnetDocente(valorSelectDocente);
            prestamo.setFechaMovimientoInventario(fechaYhora);
            prestamo.setFechaDevolucionMovInv(null);
            prestamo.setEstadoMovInv(1);
            prestamo.setIdMotivo(valorSelectMotivo);
            prestamo.setIdHorario(valorSelectHorario);
            prestamo.setIdUnidadAdministrativa(valorSelectUnidad);
            prestamo.setSerialEquipo(valorSelectEquipo);
            prestamo.setIdDocumento(valorSelectDocumento);
            prestamo.setIdMateria(valorSelectMateria);

            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            mensaje =helper.insertarPrestamo(prestamo);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

            spTipoMov.setSelection(0);
            spDocente.setSelection(0);
            spMateria.setSelection(0);
            spMotivo.setSelection(0);
            spHorario.setSelection(0);
            spUnidad.setSelection(0);
            spEquipo.setSelection(0);
            spDocumento.setSelection(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
