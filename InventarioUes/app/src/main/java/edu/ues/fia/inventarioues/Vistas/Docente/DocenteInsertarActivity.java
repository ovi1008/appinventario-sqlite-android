package edu.ues.fia.inventarioues.Vistas.Docente;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;

public class DocenteInsertarActivity extends AppCompatActivity {

    EditText editCarnetDocente, editNombreDocente, editApellidoDocente, editTelefonoDocente, editDireccionDocente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docente_insertar);
        editCarnetDocente=(EditText)findViewById(R.id.DocenteInsertarCarnet);
        editNombreDocente=(EditText)findViewById(R.id.DocenteInsertarNombre);
        editApellidoDocente=(EditText)findViewById(R.id.DocenteInsertarApellido);
        editTelefonoDocente=(EditText)findViewById(R.id.DocenteInsertarTelefono);
        editDireccionDocente=(EditText)findViewById(R.id.DocenteInsertarDireccion);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docente,menu);
        return true;
    }

    public void insertarDocente(View v){
        String nombre, carnet, apellido, telefono, direccion, mensaje;

        TablasM helper= TablasM.getInstance(getApplicationContext());
        carnet=editCarnetDocente.getText().toString().toUpperCase();
        nombre=editNombreDocente.getText().toString().toUpperCase();
        apellido=editApellidoDocente.getText().toString().toUpperCase();
        telefono=editTelefonoDocente.getText().toString().toUpperCase();
        direccion=editDireccionDocente.getText().toString().toUpperCase();

        if(carnet.isEmpty() || nombre.isEmpty() || apellido.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.docenteMsj1), Toast.LENGTH_LONG).show();
        }else{
            Docente docente= new Docente();
            docente.setCarnetDocente(carnet);
            docente.setNombreDocente(nombre);
            docente.setApellidoDocente(apellido);
            docente.setTelefonoDocente(telefono);
            docente.setDireccionDocente(direccion);
            helper.open();
            mensaje=helper.insertarDocente(docente);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editCarnetDocente.setText("");
            editNombreDocente.setText("");
            editApellidoDocente.setText("");
            editTelefonoDocente.setText("");
            editDireccionDocente.setText("");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_docente_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_docente_menu_insertar:
                Intent intent0= new Intent(this, DocenteInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_docente_menu_Editar:
                Intent intente= new Intent(this, DocenteEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_docente_menu_consultar:
                Intent intent1= new Intent(this, DocenteBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_docente_menu_eliminar:
                Intent intent2= new Intent(this, DocenteEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_docente_listar:
                Intent intent3= new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
