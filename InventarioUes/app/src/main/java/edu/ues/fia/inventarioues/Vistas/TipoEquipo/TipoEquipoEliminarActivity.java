package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

public class TipoEquipoEliminarActivity extends AppCompatActivity {
    EditText editNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_eliminar);
        editNombre=(EditText)findViewById(R.id.TipoEquipoEliminarNombre1);
    }

    public void eliminarDeleteTipoEquipo(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.tipoDeEquipoMsj1),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            TipoEquipo tipoEquipo= new TipoEquipo();
            tipoEquipo.setNombreTipoEquipo(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoEquipo(tipoEquipo);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoEquipoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoEquipo.getNombreTipoEquipo());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombre.setText("");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
