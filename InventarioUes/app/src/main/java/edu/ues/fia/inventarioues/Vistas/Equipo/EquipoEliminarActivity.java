package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;

public class EquipoEliminarActivity extends AppCompatActivity {

    EditText editSerial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_eliminar);
        editSerial=(EditText)findViewById(R.id.EquipoEliminarNombre1);
    }

    public void eliminarDeleteEquipo(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (editSerial.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.equipoMsj1a),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Equipo equipo= new Equipo();
            equipo.setSerialEquipo(editSerial.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Equipo(equipo);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,EquipoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", equipo.getSerialEquipo());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editSerial.setText("");
            }
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_equipo_menu_insertar:
                Intent intent0= new Intent(this, EquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_equipo_menu_consultar:
                Intent intent1= new Intent(this, EquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_equipo_menu_Editar:
                Intent intente= new Intent(this, EquipoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_equipo_menu_eliminar:
                Intent intent2= new Intent(this, EquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_equipo_listar:
                Intent intent3= new Intent(this, EquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
