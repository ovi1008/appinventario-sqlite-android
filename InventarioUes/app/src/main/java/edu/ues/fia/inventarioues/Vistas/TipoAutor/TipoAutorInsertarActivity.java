package edu.ues.fia.inventarioues.Vistas.TipoAutor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorInsertarActivity extends AppCompatActivity {

    EditText editNombreTipoAutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_autor_insertar);
        editNombreTipoAutor = findViewById(R.id.TipoAutorInsertarNombre);
    }

    public void insertarTipoAutor(View v){
        String nombre, mensaje;
        TablasV helper= TablasV.getInstance(getApplicationContext());
        nombre=editNombreTipoAutor.getText().toString().toUpperCase();
        if(nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.tipoAutorMsj1), Toast.LENGTH_LONG).show();
        }else{
            TipoAutor tipoAutor = new TipoAutor();
            tipoAutor.setNombreTipoAutor(nombre);
            helper.open();
            mensaje=helper.insertarTipoAutor(tipoAutor);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreTipoAutor.setText("");
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_autor_menu_insertar:
                Intent intent0= new Intent(this, TipoAutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_autor_menu_consultar:
                Intent intent1= new Intent(this, TipoAutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_autor_menu_Editar:
                Intent intente= new Intent(this, TipoAutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_autor_menu_eliminar:
                Intent intent2= new Intent(this, TipoAutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_autor_listar:
                Intent intent3= new Intent(this, TipoAutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
