package edu.ues.fia.inventarioues.Vistas.Marca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

public class MarcaEliminarAdvertenciaActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_eliminar_advertencia);
        msj=(TextView)findViewById(R.id.MarcaEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        msj.setText(getResources().getString(R.string.marcaMsj2)+" "+bundle.getString("nombre")+"\n"+getResources().getString(R.string.marcaMsj3)+" "+bundle.getString("msj"));

    }

    public void eliminarAdvMarca(View v){
        Bundle bundle=getIntent().getExtras();
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Marca marca= new Marca();
        marca.setNombreMarca(bundle.getString("nombre"));
        String mensaje= helper.eliminar2Marca(marca);
        helper.close();
        Intent intentM= new Intent(this,MarcaMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarMarca(View v){
        Intent intentCancelar = new Intent(this, MarcaMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
