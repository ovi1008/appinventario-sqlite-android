package edu.ues.fia.inventarioues.Vistas.Horario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.R;
public class HorarioEliminarAdvActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_eliminar_adv);
        msj=(TextView)findViewById(R.id.HorarioEliminarAdvMens);

        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.horarioMsj2)+" "
                +bundle.getInt("id")+
                "\n\n"+getResources().getString(R.string.horarioMsj3)+""+
                bundle.getString("msj"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario,menu);
        return true;
    }

    public void eliminarAdvHorario(View v){
        Bundle bundle=getIntent().getExtras();
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        Horario horario= new Horario();
        horario.setIdHorario(Integer.valueOf(bundle.getInt("id")));
        String mensaje= helper.eliminar2Horario(horario);
        helper.close();
        Intent intentM= new Intent(this,HorarioMenuActivity.class);
        this.startActivity(intentM);
    }

    public void eliminarCancelarHorario(View v){
        Intent intentCancelar = new Intent(this, HorarioMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_horario_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_horario_menu_insertar:
                Intent intent0= new Intent(this, HorarioInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_horario_menu_consultar:
                Intent intent1= new Intent(this, HorarioConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_horario_menu_Editar:
                Intent intente= new Intent(this, HorarioEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_horario_menu_eliminar:
                Intent intent2= new Intent(this, HorarioEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_horario_listar:
                Intent intent3= new Intent(this, HorarioMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
