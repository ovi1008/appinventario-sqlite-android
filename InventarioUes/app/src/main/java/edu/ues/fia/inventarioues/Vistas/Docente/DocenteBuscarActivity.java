package edu.ues.fia.inventarioues.Vistas.Docente;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;

public class DocenteBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editCarnet, editNombre,editApellido, editTelefono,editDireccion;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docente_buscar);

        editCarnet=(EditText)findViewById(R.id.DocenteConsultarId);
        editNombre=(EditText)findViewById(R.id.DocenteConsultarNombre);
        editApellido=(EditText)findViewById(R.id.DocenteConsultarApellido);
        editTelefono=(EditText)findViewById(R.id.DocenteConsultarTelefono);
        editDireccion=(EditText)findViewById(R.id.DocenteConsultarDireccion);
        searchConsultar = (SearchView) findViewById(R.id.Docente_searchConsultar);
        searchConsultar.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasM helper= TablasM.getInstance(getApplicationContext());
        Docente docente= new Docente();
        helper.open();
        docente=helper.docenteConsultar(newText.toUpperCase());
        helper.close();
        if (docente==null){
            editNombre.setText("");
            editCarnet.setText("");
            editApellido.setText("");
            editTelefono.setText("");
            editDireccion.setText("");
        }else {
            editCarnet.setText(docente.getCarnetDocente());
            editNombre.setText(docente.getNombreDocente());
            editApellido.setText(docente.getApellidoDocente());
            editTelefono.setText(docente.getTelefonoDocente());
            editDireccion.setText(docente.getDireccionDocente());
        }
        return false;
    }

    public void buscarEditarDocente(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, DocenteEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("apellido", editApellido.getText().toString());
            intentEditar.putExtra("telefono",editTelefono.getText().toString());
            intentEditar.putExtra("direccion", editDireccion.getText().toString());
            intentEditar.putExtra("carnet",editCarnet.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarDocente(View v){
        if (editCarnet.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            Docente docente= new Docente();
            docente.setCarnetDocente(editCarnet.getText().toString().toUpperCase());
            docente.setNombreDocente(editNombre.getText().toString().toUpperCase());
            docente.setApellidoDocente(editApellido.getText().toString().toUpperCase());
            docente.setTelefonoDocente(editTelefono.getText().toString().toUpperCase());
            docente.setDireccionDocente(editDireccion.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Docente(docente);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, DocenteEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("carnet", docente.getCarnetDocente());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docente,menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_docente_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_docente_menu_insertar:
                Intent intent0= new Intent(this, DocenteInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_docente_menu_Editar:
                Intent intente= new Intent(this, DocenteEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_docente_menu_consultar:
                Intent intent1= new Intent(this, DocenteBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_docente_menu_eliminar:
                Intent intent2= new Intent(this, DocenteEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_docente_listar:
                Intent intent3= new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
