package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.IdiomaAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoConsultarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEditarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEliminarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoInsertarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoMenuActivity;

public class IdiomaMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_menu);
        ListView listView =(ListView)findViewById(R.id.listIdiomas);
        List<Idioma> lista= listadoIdiomas();
        IdiomaAdaptador idiomaAdaptador=new IdiomaAdaptador(this,lista);
        listView.setAdapter(idiomaAdaptador);
    }

    public List<Idioma> listadoIdiomas(){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        Cursor c =helper.obtenerIdioma();
        List<Idioma> idiomasList= new ArrayList<>();
        while (c.moveToNext()){
            Idioma idioma=new Idioma();
            //{"codIdioma", "nombreIdioma"};
            idioma.setCodIdioma(c.getInt(0));
            idioma.setNombreIdioma(c.getString(1));
            idiomasList.add(idioma);
        }
        return idiomasList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
