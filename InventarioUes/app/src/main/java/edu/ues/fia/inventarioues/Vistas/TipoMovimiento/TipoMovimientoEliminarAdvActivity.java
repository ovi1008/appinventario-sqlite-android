package edu.ues.fia.inventarioues.Vistas.TipoMovimiento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.R;

public class TipoMovimientoEliminarAdvActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_movimiento_eliminar_adv);
        msj=(TextView)findViewById(R.id.TipoMovimientoEliminarAdvMens);

        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.tipoMovimientoMsj2)+" "+
                +bundle.getInt("nombre")+"\n"+getResources().getString(R.string.tipoMovimientoMsj3)+" "+
                bundle.getString("msj"));
    }

    public void eliminarAdvTipoMovimiento(View v){
        Bundle bundle=getIntent().getExtras();
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        TipoMovimiento tipoMovimiento= new TipoMovimiento();
        tipoMovimiento.setIdTipoMovimiento(bundle.getInt("nombre"));
        String mensaje= helper.eliminar2TipoMovimiento(tipoMovimiento);
        helper.close();
        Intent intentM= new Intent(this,TipoMovimientoMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarTipoMovimiento(View v){
        Intent intentCancelar = new Intent(this, TipoMovimientoMenuActivity.class);
        this.startActivity(intentCancelar);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_movimiento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_movimiento_menu_insertar:
                Intent intent0= new Intent(this, TipoMovimientoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_movimiento_menu_consultar:
                Intent intent1= new Intent(this, TipoMovimientoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_movimiento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_movimiento_menu_Editar:
                Intent intentEd= new Intent(this, TipoMovimientoEditarActivity.class);
                this.startActivity(intentEd);
                return true;


            case R.id.action_tipo_movimiento_menu_eliminar:
                Intent intent2= new Intent(this, TipoMovimientoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_movimiento_listar:
                Intent intent3= new Intent(this, TipoMovimientoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
