package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;



public class EditorialBuscarActivity  extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editId,editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_buscar);
        searchConsultar=(SearchView) findViewById(R.id.Editorial_searchConsultar);
        editId=(EditText)findViewById(R.id.EditorialConsultarId);
        editNombre=(EditText)findViewById(R.id.EditorialConsultarNombre);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasV helper= TablasV.getInstance(getApplicationContext());
        Editorial editorial= new Editorial();
        helper.open();
        editorial=helper.editorialConsultar(newText.toUpperCase());
        helper.close();
        if (editorial==null){
            editId.setText("");
            editNombre.setText("");
        }else {
            editId.setText(String.valueOf(editorial.getIdEditorial()));
            editNombre.setText(editorial.getNombreEditorial());
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void buscarEditarEditorial(View v){
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, EditorialEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarEditorial(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Editorial editorial= new Editorial();
            editorial.setIdEditorial(Integer.valueOf(editId.getText().toString()));
            editorial.setNombreEditorial(editNombre.getText().toString());
            String mensaje= helper.eliminar1Editorial(editorial);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,EditorialEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", editorial.getNombreEditorial());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
