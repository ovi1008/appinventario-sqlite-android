package edu.ues.fia.inventarioues;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.Usuario;

public class MainActivity extends Activity {

    EditText editUserName, editPassworUsuario;
    Locale locale;
    Configuration config = new Configuration();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editUserName=(EditText)findViewById(R.id.userUsuario);
        editPassworUsuario=(EditText)findViewById(R.id.passwordUsuario);
    }

    public void consultarUsuario(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Usuario usuario=helper.usuarioConsultar(editUserName.getText().toString(), editPassworUsuario.getText().toString());
        helper.close();
        if(usuario==null){
            Toast.makeText(this,"Datos de usuario y contraseña incorrectos o no exiten.",Toast.LENGTH_LONG).show();
        }else {
            Intent inte = new Intent(this, MenuPrincipalActivity.class);
            this.startActivity(inte);
            editUserName.setText("");
            editPassworUsuario.setText("");
        }
    }

    public void idiomaMain(View v){

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Set a title for alert dialog
        builder.setTitle(R.string.loginIdioma);

        // Initializing an array of idiomas
        final String[] idiomas = getResources().getStringArray(R.array.idiomas);

        // Set the list of items for alert dialog
        builder.setItems(idiomas, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectedIdioma = Arrays.asList(idiomas).get(which);
                // Set the layout background color as user selection
                //rl.setBackgroundColor(Color.parseColor(selectedIdioma));
                if (selectedIdioma.equals(idiomas[0])){
                    locale = new Locale("es");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);

                }
                if (selectedIdioma.equals(idiomas[1])){
                    locale = new Locale("en");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }
                if (selectedIdioma.equals(idiomas[2])){
                    locale = new Locale("it");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }
                if (selectedIdioma.equals(idiomas[3])){
                    locale = new Locale("pt");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }

            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();

    }

    @Override
    public void onBackPressed() {
    }
}
