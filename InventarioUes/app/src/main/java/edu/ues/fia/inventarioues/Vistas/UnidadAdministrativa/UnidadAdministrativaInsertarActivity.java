package edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;

public class UnidadAdministrativaInsertarActivity extends AppCompatActivity {
    EditText editNombreUnidadAdministrativa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unidad_administrativa_insertar);
        editNombreUnidadAdministrativa=(EditText)findViewById(R.id.UnidadAdministrativaInsertarNombre);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unidad_administrativa,menu);
        return true;
    }
    public void insertarUnidadAdministrativa(View v){

        String nombre, mensaje;
        TablasL helper= TablasL.getInstance(getApplicationContext());
        nombre=editNombreUnidadAdministrativa.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.unidadMsj1), Toast.LENGTH_LONG).show();
        }else{
            UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
            unidadAdministrativa.setNombreUnidadAdministrativa(nombre);
            helper.open();
            mensaje=helper.insertarUnidadAdministrativa(unidadAdministrativa);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreUnidadAdministrativa.setText("");
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_unidad_administrativa_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_unidad_administrativa_menu_insertar:
                Intent intent0= new Intent(this, UnidadAdministrativaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_unidad_administrativa_menu_consultar:
                Intent intent1= new Intent(this, UnidadAdministrativaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_unidad_administrativa_menu_Editar:
                Intent intente= new Intent(this, UnidadAdministrativaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_unidad_administrativa_menu_eliminar:
                Intent intent2= new Intent(this, UnidadAdministrativaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_unidad_administrativa_listar:
                Intent intent3= new Intent(this, UnidadAdministrativaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
