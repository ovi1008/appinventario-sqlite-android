package edu.ues.fia.inventarioues.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;

public class TablasJ {

    //BASE DE DATOS ACCESO

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TablasJ instance;

    private TablasJ(Context context){
        this.openHelper=new DatabaseOpenHelper(context);
    }

    public static TablasJ getInstance(Context context){
        if(instance==null){
            instance=new TablasJ(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getWritableDatabase();
        habilitarForaneas();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public void habilitarForaneas(){
        db.execSQL("PRAGMA foreign_keys=ON;");
    }


    //Controles de las tablas dentro de la base de datos

    //Tabla usuario
    private static final String[] camposUsuario= new String[]{"idUsuario", "nombreUsuario", "apellidoUsuario", "userUsuario", "passwordUsuario"};

    public Usuario usuarioConsultar(String userUsuario, String passwordUsuario){
        String[] id={userUsuario, passwordUsuario};
        Cursor c=db.query("usuario", camposUsuario, "userUsuario=? AND passwordUsuario=?", id, null, null, null);
        if(c.moveToFirst()){
            //{"idUsuario", "nombreUsuario", "apellidoUsuario", "userUsuario", "passwordUsuario"};
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(c.getInt(0));
            usuario.setNombreUsuario(c.getString(1));
            usuario.setApellidoUsuario(c.getString(2));
            usuario.setUserUsuario(c.getString(3));
            usuario.setPasswordUsuario(c.getString(4));
            ContentValues v = new ContentValues();
            v.put("idUsuario",c.getInt(0));
            String []clave={"1"};
            db.update("usuarioActual", v,"idUsuarioActual=?",clave );
            return usuario;
        }else {
            return null;
        }
    }

    //SELECT idUsuario FROM usuarioActual WHERE idUsuarioActual=1

    public Integer obtenerUsuarioActual(){
        String[]id={"1"};
        Integer valor;
        Cursor c=db.query("usuarioActual",null,"idUsuarioActual=?",id,null,null,null);
        if (c.moveToFirst()){
            return valor=c.getInt(1);
        }else {
            return valor=1;
        }
    }

    public Usuario usuarioDatosConsultar(Integer clave){
        String[] id={String.valueOf(clave)};
        Cursor c=db.query("usuario", camposUsuario, "idUsuario=?", id, null, null, null);
        if(c.moveToFirst()){
            //{"idUsuario", "nombreUsuario", "apellidoUsuario", "userUsuario", "passwordUsuario"};
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(c.getInt(0));
            usuario.setNombreUsuario(c.getString(1));
            usuario.setApellidoUsuario(c.getString(2));
            usuario.setUserUsuario(c.getString(3));
            usuario.setPasswordUsuario(c.getString(4));
            return usuario;
        }else {
            return null;
        }
    }

    //Tabla Acceso Usuario

    // idUsuarioAccesos, idUsuario, tMarca, tTipoEquipo, tEquipo, tEditorial
    //tDocumento, tAutor, tMateria, tTipoMovimiento, tMotivo,tTipoAutor,tTipoDocumento
    // tUnidadAdmin, tIdioma, tDocente tHorario, tPrestamo

    public UsuarioAccesos usuarioAccesosConsultar(Integer clave){
        String[] id={String.valueOf(clave)};
        Cursor c=db.query("usuarioAccesos", null, "idUsuario=?", id, null, null, null);
        if(c.moveToFirst()){
            UsuarioAccesos usuarioAccesos = new UsuarioAccesos();
            usuarioAccesos.setIdUsuarioAccesos(c.getInt(0));
            usuarioAccesos.setIdUsuario(c.getInt(1));
            usuarioAccesos.settMarca(c.getInt(2));
            usuarioAccesos.settTipoEquipo(c.getInt(3));
            usuarioAccesos.settEquipo(c.getInt(4));
            usuarioAccesos.settEditorial(c.getInt(5));
            usuarioAccesos.settDocumento(c.getInt(6));
            usuarioAccesos.settAutor(c.getInt(7));
            usuarioAccesos.settMateria(c.getInt(8));
            usuarioAccesos.settTipoMovimiento(c.getInt(9));
            usuarioAccesos.settMotivo(c.getInt(10));
            usuarioAccesos.settTipoAutor(c.getInt(11));
            usuarioAccesos.settTipoDocumento(c.getInt(12));
            usuarioAccesos.settUnidadAdmin(c.getInt(13));
            usuarioAccesos.settIdioma(c.getInt(14));
            usuarioAccesos.settDocente(c.getInt(15));
            usuarioAccesos.settHorario(c.getInt(16));
            usuarioAccesos.settPrestamo(c.getInt(17));
            return usuarioAccesos;
        }else {
            return null;
        }
    }


    //Tabla Marca

    private static final String[]camposMarca= new String[]{"idMarca", "nombreMarca"};

    public String insertarMarca(Marca marca){
        String mensaje= "Marca insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        v.put("nombreMarca", marca.getNombreMarca());
        inserto=db.insert("marca", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar marca, registro duplicado.";
        }else {
            mensaje=mensaje+marca.getNombreMarca();
        }
        return mensaje;
    }

    public String actualizarMarca(Marca marca, String clave){
       int  exito;
        String mensaje= "Se actualizo marca con nombre: ";
        String id[]={clave};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("marca", null,"nombreMarca=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            //{"idMarca", "nombreMarca"};
            v.put("idMarca",marca.getIdMarca());
            v.put("nombreMarca",marca.getNombreMarca ());
            try {
                db.update("marca",v,"nombreMarca=?",id);
                mensaje=mensaje+marca.getNombreMarca();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La marca con el nombre: "+marca.getNombreMarca()+" no existe";
        }
    }

    public Marca marcaConsultar(String nombreMarca){
        String[] id={nombreMarca};
        Cursor c=db.query("marca", camposMarca, "nombreMarca=?", id, null, null, null);
        if(c.moveToFirst()){
            Marca marca= new Marca();
            //{"idMarca", "nombreMarca"}
            marca.setIdMarca(c.getInt(0));
            marca.setNombreMarca(c.getString(1));
            return marca;
        }else {
            return null;
        }
    }


    public String eliminar1Marca(Marca marca){
        String mensaje = "Se elimino la marca con el nombre: ";
        int contador=0;
        String id[]={marca.getNombreMarca()};
        //{"idMarca", "nombreMarca"};
        Cursor c=db.query("marca", null,"nombreMarca=?",id,null,null,null);
        if(c.moveToFirst()){
            Marca marca2=marcaConsultar(marca.getNombreMarca());
            String id2[]={String.valueOf(marca2.getIdMarca())};
            //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo", "fechaEquipo", "fechaInactivoEquipo"};
            contador=db.query("equipo",camposEquipo,"idMarca=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("marca","nombreMarca=?",id);
                return mensaje=mensaje+marca.getNombreMarca();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="La marca con el nombre: "+marca.getNombreMarca()+" no existe";
        }
    }

    public String eliminar2Marca(Marca marca){
        String mensaje = "Se elimino la marca con el nombre: "+marca.getNombreMarca();
        String id[]={marca.getNombreMarca()};
        db.delete("marca","nombreMarca=?",id);
        return mensaje;
    }

    public Cursor obtenerMarcas() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("marca", camposMarca,null,null, null, null, null);
    }


    /// Tabla Tipo Equipo

    private  static final String [] camposTipoEquipo = new String[]{"idTipoEquipo", "nombreTipoEquipo"};


    public String insertarTipoEquipo(TipoEquipo tipoEquipo){
        String mensaje= "Tipo equipo insertado con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposTipoEquipo = new String[]{"idTipoEquipo", "nombreTipoEquipo"}
        v.put("nombreTipoEquipo", tipoEquipo.getNombreTipoEquipo());
        inserto=db.insert("tipoEquipo", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar tipo equipo, registro duplicado.";
        }else {
            mensaje=mensaje+tipoEquipo.getNombreTipoEquipo();
        }
        return mensaje;
    }

    public TipoEquipo tipoEquipoConsultar(String nombreTipoEquipo){
        String[] id={nombreTipoEquipo};
        Cursor c=db.query("tipoEquipo", camposTipoEquipo, "nombreTipoEquipo=?", id, null, null, null);
        if(c.moveToFirst()){
           TipoEquipo tipoEquipo= new TipoEquipo();
            //{"idTipoEquipo", "nombreTipoEquipo"};

            tipoEquipo.setIdTipoEquipo(c.getInt(0));
            tipoEquipo.setNombreTipoEquipo(c.getString(1));

            return tipoEquipo;
        }else {
            return null;
        }
    }

    public String actualizarTipoEquipo(TipoEquipo tipoEquipo, String clave){
        String mensaje= "Se actualizo el equipo con nombre: ";
        String id[]={clave};
        ////{"idTipoEquipo", "nombreTipoEquipo"};
        Cursor c=db.query("tipoEquipo", null,"nombreTipoEquipo=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                ////{"idTipoEquipo", "nombreTipoEquipo"};
                v.put("idTipoEquipo",tipoEquipo.getIdTipoEquipo());
                v.put("nombreTipoEquipo",tipoEquipo.getNombreTipoEquipo());
                db.update("tipoEquipo",v,"nombreTipoEquipo=?",id);
                mensaje=mensaje+tipoEquipo.getNombreTipoEquipo();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El tipo de equipo con el nombre: "+tipoEquipo.getNombreTipoEquipo()+" no existe";
        }
    }


    public String eliminar1TipoEquipo(TipoEquipo tipoEquipo){
        String mensaje = "Se elimino el tipo de equipo con el nombre: ";
        int contador=0;
        String id[]={tipoEquipo.getNombreTipoEquipo()};
        //{"idTipoEquipo", "nombreTipoEquipo"};
        Cursor c=db.query("tipoEquipo", null,"nombreTipoEquipo=?",id,null,null,null);
        if(c.moveToFirst()){
            TipoEquipo tipoEquipo2=tipoEquipoConsultar(tipoEquipo.getNombreTipoEquipo());
            String id2[]={String.valueOf(tipoEquipo2.getIdTipoEquipo())};
            //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo", "fechaEquipo", "fechaInactivoEquipo"};
            contador=db.query("equipo",camposEquipo,"idTipoEquipo=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("tipoEquipo","nombreTipoEquipo=?",id);
                return mensaje=mensaje+tipoEquipo.getNombreTipoEquipo();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El tipo de equipo con nombre: : "+tipoEquipo.getNombreTipoEquipo()+" no existe";
        }
    }

    public String eliminar2TipoEquipo(TipoEquipo tipoEquipo){
        String mensaje = "Se elimino el tipo de equipo con el nombre: "+tipoEquipo.getNombreTipoEquipo();
        String id[]={tipoEquipo.getNombreTipoEquipo()};
        db.delete("tipoEquipo","nombreTipoEquipo=?",id);
        return mensaje;
    }


    public Cursor obtenerTiposEquipo() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("tipoEquipo", camposTipoEquipo,null,null, null, null, null);
    }





    //TablaEquipo

    private static final String[] camposEquipo= new String[]
            {"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo", "fechaEquipo", "fechaInactivoEquipo", "prestadoEquipo"};

    public String insertarEquipo(Equipo equipo){
        String mensaje= "Equipo insertada con serial: ";
        long inserto=0;
        //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
        // "fechaEquipo", "fechaInactivoEquipo", "equipoPrestado"};
        ContentValues v = new ContentValues();
        v.put("serialEquipo", equipo.getSerialEquipo());
        v.put("idTipoEquipo", equipo.getIdTipoEquipo());
        v.put("idMarca", equipo.getIdMarca());
        v.put("descripcionEquipo", equipo.getDescripcionEquipo());
        v.put("estadoEquipo", equipo.getEstadoEquipo());
        v.put("fechaEquipo", equipo.getFechaEquipo());
        v.put("fechaInactivoEquipo", equipo.getFechaInactivoEquipo());
        v.put("prestadoEquipo", equipo.getPrestamo());

        inserto=db.insert("equipo", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar equipo, registro duplicado.";
        }else {
            mensaje=mensaje+equipo.getSerialEquipo();
        }
        return mensaje;
    }

    public Equipo equipoConsultar(String serialEquipo){
        String[] id={serialEquipo};
        Cursor c=db.query("equipo", camposEquipo, "serialEquipo=?", id, null, null, null);
        if(c.moveToFirst()){
            Equipo Equipo= new Equipo();
            //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
            // "fechaEquipo", "fechaInactivoEquipo", "equipoPrestado"};
            Equipo.setSerialEquipo(c.getString(0));
            Equipo.setIdTipoEquipo(c.getInt(1));
            Equipo.setIdMarca(c.getInt(2));
            Equipo.setDescripcionEquipo(c.getString(3));
            Equipo.setEstadoEquipo(c.getInt(4));
            Equipo.setFechaEquipo(c.getString(5));
            Equipo.setFechaInactivoEquipo(c.getString(6));
            Equipo.setPrestamo(c.getInt(7));
            return Equipo;
        }else {
            return null;
        }
    }

    public String actualizarEquipo(Equipo equipo, String clave){
        int  exito;
        String mensaje= "Se actualizo equipo con serial: ";
        String id[]={clave};
        //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
        // "fechaEquipo", "fechaInactivoEquipo", "equipoPrestado"};
        Cursor c=db.query("equipo", null,"serialEquipo=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
            // "fechaEquipo", "fechaInactivoEquipo", "equipoPrestado"};
            v.put("serialEquipo", equipo.getSerialEquipo());
            v.put("idTipoEquipo", equipo.getIdTipoEquipo());
            v.put("idMarca", equipo.getIdMarca());
            v.put("descripcionEquipo", equipo.getDescripcionEquipo());
            v.put("estadoEquipo", equipo.getEstadoEquipo());
            v.put("fechaEquipo", equipo.getFechaEquipo());
            v.put("fechaInactivoEquipo", equipo.getFechaInactivoEquipo());
            v.put("prestadoEquipo", equipo.getPrestamo());
            try {
                db.update("equipo",v,"serialEquipo=?",id);
                mensaje=mensaje+equipo.getSerialEquipo();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La equipo con el nombre: "+equipo.getSerialEquipo()+" no existe";
        }
    }

    public String eliminar1Equipo(Equipo equipo){
        String mensaje = "Se elimino el equipo con el serial: ";
        int contador=0;
        String id[]={equipo.getSerialEquipo()};
        //{"serialEquipo", "idTipoEquipo", "idMarca", "descripcionEquipo", "estadoEquipo",
        // "fechaEquipo", "fechaInactivoEquipo", "equipoPrestado"};
        Cursor c=db.query("equipo", null,"serialEquipo=?",id,null,null,null);
        if(c.moveToFirst()){
            Equipo equipo2=equipoConsultar(equipo.getSerialEquipo());
            String id2[]={equipo2.getSerialEquipo()};
            //{"serialEquipo", "idTipoEquipo", "idEquipo", "descripcionEquipo", "estadoEquipo", "fechaEquipo", "fechaInactivoEquipo"};
            contador=db.query("movimientoInventario",null,"serialEquipo=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("equipo","serialEquipo=?",id);
                return mensaje=mensaje+equipo.getSerialEquipo();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="La equipo con el nombre: "+equipo.getSerialEquipo()+" no existe";
        }
    }

    public String eliminar2Equipo(Equipo equipo){
        String mensaje = "Se elimino el equipo con el serial: "+equipo.getSerialEquipo();
        String id[]={equipo.getSerialEquipo()};
        db.delete("equipo","serialEquipo=?",id);
        return mensaje;
    }

    public Cursor obtenerEquipos() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.query("equipo", camposEquipo,null,null, null, null, null);
    }





    //Tabla Prestamo
    //Obtiene Documentos Prestado
    public Cursor obtenerPrestamos001() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM movimientoInventario WHERE serialEquipo IS NULL", null, null);
    }

    public Cursor obtenerEquipoDisponibles() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM equipo WHERE prestadoEquipo=1", null, null);
    }

    public Cursor obtenerDocumentoDisponibles() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM documento WHERE prestadoDocumento=1;", null, null);
    }


    //Obtiene Equipo Prestados
    public Cursor obtenerPrestamos002() throws SQLException{
        db=openHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM movimientoInventario WHERE idDocumento IS NULL", null, null);
    }

    public String insertarPrestamo(Prestamo prestamo){
        String mensaje= "Prestamo insertado con id: ";
        long inserto=0;
        ContentValues v = new ContentValues();

        // idTipoMovimiento, carnetDocente, fechaMovimientoInventario
        // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
        // serialEquipo, idDocumento, idMateria
        v.put("idTipoMovimiento", prestamo.getIdTipoMovimiento());
        v.put("carnetDocente", prestamo.getCarnetDocente());
        v.put("fechaMovimientoInventario", prestamo.getFechaMovimientoInventario());
        v.put("fechaDevolucionMovInv", prestamo.getFechaDevolucionMovInv());
        v.put("estadoMovInv", prestamo.getEstadoMovInv());
        v.put("idMotivo", prestamo.getIdMotivo());
        v.put("idHorario", prestamo.getIdHorario());
        v.put("idUnidadAdministrativa", prestamo.getIdUnidadAdministrativa());
        v.put("serialEquipo", prestamo.getSerialEquipo());
        v.put("idDocumento", prestamo.getIdDocumento());
        v.put("idMateria", prestamo.getIdMateria());

        inserto=db.insert("movimientoInventario", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar prestamo, registro duplicado.";
        }else {
            mensaje=mensaje+inserto;
        }
        return mensaje;
    }

    public Prestamo prestamoConsultar(Integer idPrestamo){
        String[] id={String.valueOf(idPrestamo)};
        Cursor c=db.query("movimientoInventario", null, "idMovimientoInventario=?", id, null, null, null);
        if(c.moveToFirst()){
            // idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
            // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
            // serialEquipo, idDocumento, idMateria
            Prestamo prestamo= new Prestamo();
            prestamo.setIdMovimientoInventario(c.getInt(0));
            prestamo.setIdTipoMovimiento(c.getInt(1));
            prestamo.setCarnetDocente(c.getString(2));
            prestamo.setFechaMovimientoInventario(c.getString(3));
            prestamo.setFechaDevolucionMovInv(c.getString(4));
            prestamo.setEstadoMovInv(c.getInt(5));
            prestamo.setIdMotivo(c.getInt(6));
            prestamo.setIdHorario(c.getInt(7));
            prestamo.setIdUnidadAdministrativa(c.getInt(8));
            prestamo.setSerialEquipo(c.getString(9));
            prestamo.setIdDocumento(c.getInt(10));
            prestamo.setIdMateria(c.getString(11));
            return prestamo;
        }else {
            return null;
        }
    }

    public String actualizarPrestamo(Prestamo prestamo, Integer clave){
        int  exito;
        String mensaje= "Se actualizo prestamo con id: ";
        String id[]={String.valueOf(clave)};
        //{"idPrestamo", "nombrePrestamo"};
        Cursor c=db.query("movimientoInventario", null,"idMovimientoInventario=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            // idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
            // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
            // serialEquipo, idDocumento, idMateria
            v.put("idTipoMovimiento", prestamo.getIdTipoMovimiento());
            v.put("carnetDocente", prestamo.getCarnetDocente());
            //v.put("fechaMovimientoInventario", prestamo.getFechaMovimientoInventario());
            //v.put("fechaDevolucionMovInv", prestamo.getFechaDevolucionMovInv());
            //v.put("estadoMovInv", prestamo.getEstadoMovInv());
            v.put("idMotivo", prestamo.getIdMotivo());
            v.put("idHorario", prestamo.getIdHorario());
            v.put("idUnidadAdministrativa", prestamo.getIdUnidadAdministrativa());
            v.put("serialEquipo", prestamo.getSerialEquipo());
            v.put("idDocumento", prestamo.getIdDocumento());
            v.put("idMateria", prestamo.getIdMateria());
            try {
                db.update("movimientoInventario",v,"idMovimientoInventario=?",id);
                mensaje=mensaje+prestamo.getIdMovimientoInventario();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La prestamo con el nombre: "+prestamo.getIdMovimientoInventario()+" no existe";
        }
    }

    public TipoEquipo tipoEquipoConsultarActual(Integer idTipoEquipo){
        String[] id={String.valueOf(idTipoEquipo)};
        Cursor c=db.query("tipoEquipo", camposTipoEquipo, "idTipoEquipo=?", id, null, null, null);
        if(c.moveToFirst()){
            TipoEquipo tipoEquipo= new TipoEquipo();
            //{"idTipoEquipo", "nombreTipoEquipo"};

            tipoEquipo.setIdTipoEquipo(c.getInt(0));
            tipoEquipo.setNombreTipoEquipo(c.getString(1));

            return tipoEquipo;
        }else {
            return null;
        }
    }

    public String eliminarPrestamo(Prestamo prestamo){
        String mensaje = "Se elimino el prestamo con el id: ";
        int contador=0;
        String id[]={String.valueOf(prestamo.getIdMovimientoInventario())};
        Cursor c=db.query("movimientoInventario", null, "idMovimientoInventario=?", id, null, null, null);
        if(c.moveToFirst()){
            db.delete("movimientoInventario","idMovimientoInventario=?",id);
            return mensaje=mensaje+prestamo.getIdMovimientoInventario();
        }else {
            return mensaje="El prestamo con el id: "+prestamo.getIdMovimientoInventario()+" no existe";
        }
    }

    public UnidadAdministrativa unidadActual(Integer idUnidad){
        String[] id={String.valueOf(idUnidad)};
        Cursor c=db.query("unidadAdministrativa", null, "idUnidadAdministrativa=?", id, null, null, null);
        if(c.moveToFirst()){
            UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
            //{"idUnidadAdministrativa", "nombreUnidadAdministrativa"};
            unidadAdministrativa.setIdUnidadAdministrativa(c.getInt(0));
            unidadAdministrativa.setNombreUnidadAdministrativa(c.getString(1));
            return unidadAdministrativa;
        }else {
            return null;
        }
    }

    public String actualizarEstadoPrestamo(Prestamo prestamo, Integer clave){
        int  exito;
        String mensaje= "Se actualizo prestamo con id: ";
        String id[]={String.valueOf(clave)};
        //{"idPrestamo", "nombrePrestamo"};
        Cursor c=db.query("movimientoInventario", null,"idMovimientoInventario=?",id,null,null,null);
        if(c.moveToFirst()){
            ContentValues v=new ContentValues();
            // idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
            // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
            // serialEquipo, idDocumento, idMateria
            //v.put("idTipoMovimiento", prestamo.getIdTipoMovimiento());
            //v.put("carnetDocente", prestamo.getCarnetDocente());
            //v.put("fechaMovimientoInventario", prestamo.getFechaMovimientoInventario());
            v.put("fechaDevolucionMovInv", prestamo.getFechaDevolucionMovInv());
            v.put("estadoMovInv", prestamo.getEstadoMovInv());
            //v.put("idMotivo", prestamo.getIdMotivo());
            //v.put("idHorario", prestamo.getIdHorario());
            //v.put("idUnidadAdministrativa", prestamo.getIdUnidadAdministrativa());
            //v.put("serialEquipo", prestamo.getSerialEquipo());
            //v.put("idDocumento", prestamo.getIdDocumento());
            //v.put("idMateria", prestamo.getIdMateria());
            try {
                db.update("movimientoInventario",v,"idMovimientoInventario=?",id);
                mensaje=mensaje+prestamo.getIdMovimientoInventario();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto en una campo al actualizar";
            }
        }else {
            return mensaje="La prestamo con el nombre: "+prestamo.getIdMovimientoInventario()+" no existe";
        }
    }

    public Prestamo prestamoActivosConsultar(Integer idPrestamo){
        String[] id={String.valueOf(idPrestamo),"1"};
        Cursor c=db.query("movimientoInventario", null, "idMovimientoInventario=? AND estadoMovInv=?", id, null, null, null);
        if(c.moveToFirst()){
            // idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
            // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
            // serialEquipo, idDocumento, idMateria
            Prestamo prestamo= new Prestamo();
            prestamo.setIdMovimientoInventario(c.getInt(0));
            prestamo.setIdTipoMovimiento(c.getInt(1));
            prestamo.setCarnetDocente(c.getString(2));
            prestamo.setFechaMovimientoInventario(c.getString(3));
            prestamo.setFechaDevolucionMovInv(c.getString(4));
            prestamo.setEstadoMovInv(c.getInt(5));
            prestamo.setIdMotivo(c.getInt(6));
            prestamo.setIdHorario(c.getInt(7));
            prestamo.setIdUnidadAdministrativa(c.getInt(8));
            prestamo.setSerialEquipo(c.getString(9));
            prestamo.setIdDocumento(c.getInt(10));
            prestamo.setIdMateria(c.getString(11));
            return prestamo;
        }else {
            return null;
        }
    }






}
