package edu.ues.fia.inventarioues.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;


import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;

public class TablasM {

    //BASE DE DATOS ACCESO

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TablasM instance;

    private TablasM(Context context){
        this.openHelper=new DatabaseOpenHelper(context);
    }

    public static TablasM getInstance(Context context){
        if(instance==null){
            instance=new TablasM(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getWritableDatabase();
        habilitarForaneas();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public void habilitarForaneas(){
        db.execSQL("PRAGMA foreign_keys=ON;");
    }


    //Controles de las tablas dentro de la base de datos


    //tabla tipoMovimiento

    private static final String[]camposTipoMovimiento= new String[]{"idTipoMovimiento", "nombreTipoMovimiento"};


    public String insertarTipoMovimiento(TipoMovimiento tipoMovimiento){
        String mensaje= "Tipo movimiento insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposTipoMovimiento= new String[]{"idTipoMovimiento", "nombreTipoMovimiento"};
        v.put("nombreTipoMovimiento", tipoMovimiento.getNombreTipoMovimiento());
        inserto=db.insert("tipoMovimiento", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar tipo movimiento, registro duplicado.";
        }else {
            mensaje=mensaje+tipoMovimiento.getNombreTipoMovimiento();
        }
        return mensaje;
    }

    public TipoMovimiento tipoMovimientoConsultar(String nombreTipoMovimiento){
        String[] id={nombreTipoMovimiento};
        Cursor c=db.query("tipoMovimiento", camposTipoMovimiento, "idTipoMovimiento=?", id, null, null, null);
        if(c.moveToFirst()){
            TipoMovimiento tipoMovimiento= new TipoMovimiento();
            //{"idTipoMovimiento", "nombreTipoMovimiento"};
            tipoMovimiento.setIdTipoMovimiento(c.getInt(0));
            tipoMovimiento.setNombreTipoMovimiento(c.getString(1));
            return tipoMovimiento;
        }else {
            return null;
        }
    }

    public String actualizarTipoMovimiento(TipoMovimiento tipoMovimiento, String clave){
        String mensaje= "Se actualizo tipo movimiento con nombre: ";
        String id[]={clave};
        ////{"idTipoMovimiento", "nombreTipoMovimiento"};
        Cursor c=db.query("tipoMovimiento", null,"idTipoMovimiento=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"idTipoMovimiento", "nombreTipoMovimiento"};
                v.put("idTipoMovimiento",tipoMovimiento.getIdTipoMovimiento());
                v.put("nombreTipoMovimiento",tipoMovimiento.getNombreTipoMovimiento());
                db.update("tipoMovimiento",v,"idTipoMovimiento=?",id);
                mensaje=mensaje+tipoMovimiento.getNombreTipoMovimiento();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El tipo de movimiento con el nombre: "+tipoMovimiento.getNombreTipoMovimiento()+" no existe";
        }
    }


    public Cursor obtenerTiposMovimiento() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("tipoMovimiento", camposTipoMovimiento,null,null, null, null, null);
    }


    public String eliminar1TipoMovimiento(TipoMovimiento tipoMovimiento){
        String mensaje = "Se elimino el tipo de movimiento con el id: ";
        int contador=0;
        String id[]={String.valueOf(tipoMovimiento.getIdTipoMovimiento())};
        //{"idTipoMovimiento", "nombreTipoMovimiento"};
        Cursor c=db.query("tipoMovimiento", null,"idTipoMovimiento=?",id,null,null,null);
        if(c.moveToFirst()){
            TipoMovimiento tipoMovimiento2=tipoMovimientoConsultar(String.valueOf(tipoMovimiento.getIdTipoMovimiento()));
            String id2[]={String.valueOf(tipoMovimiento2.getIdTipoMovimiento())};
           //duda
            contador=db.query("movimientoInventario",null,"idTipoMovimiento=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("tipoMovimiento","idTipoMovimiento=?",id);
                return mensaje=mensaje+tipoMovimiento.getIdTipoMovimiento();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El tipo de movimiento con id: : "+tipoMovimiento.getIdTipoMovimiento()+" no existe";
        }
    }

    public String eliminar2TipoMovimiento(TipoMovimiento tipoMovimiento){
        String mensaje = "Se elimino el tipo de movimiento con el id: "+tipoMovimiento.getNombreTipoMovimiento();
        String id[]={String.valueOf(tipoMovimiento.getIdTipoMovimiento())};
        db.delete("tipoMovimiento","idTipoMovimiento=?",id);
        return mensaje;
    }



    //tabla motivo

    private static final String[]camposMotivo= new String[]{"idMotivo", "nombreMotivo"};

    public String insertarMotivo(Motivo motivo){
        String mensaje= "Motivo insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposMotivo= new String[]{"idMotivo", "nombreMotivo"};
        v.put("nombreMotivo", motivo.getNombreMotivo());
        inserto=db.insert("motivo", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar motivo, registro duplicado.";
        }else {
            mensaje=mensaje+motivo.getNombreMotivo();
        }
        return mensaje;
    }

    public Motivo motivoConsultar(String nombreMotivo){
        String[] id={nombreMotivo};
        Cursor c=db.query("motivo", camposMotivo, "idMotivo=?", id, null, null, null);
        if(c.moveToFirst()){
            Motivo motivo= new Motivo();
            //{"idMotivo", "nombreMotivo"}
            motivo.setIdMotivo(c.getInt(0));
            motivo.setNombreMotivo(c.getString(1));
            return motivo;
        }else {
            return null;
        }
    }

    public String actualizarMotivo(Motivo motivo, String clave){
        String mensaje= "Se actualizo el motivo con nombre: ";
        String id[]={clave};
        //{"idMotivo", "nombreMotivo"}
        Cursor c=db.query("motivo", null,"idMotivo=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"idMotivo", "nombreMotivo"}
                v.put("idMotivo",motivo.getIdMotivo());
                v.put("nombreMotivo",motivo.getNombreMotivo());
                db.update("motivo",v,"idMotivo=?",id);
                mensaje=mensaje+motivo.getNombreMotivo();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El motivo con el nombre: "+motivo.getNombreMotivo()+" no existe";
        }
    }

    public String eliminar1Motivo(Motivo motivo){
        String mensaje = "Se elimino el motivo con el id: ";
        int contador=0;
        String id[]={String.valueOf(motivo.getIdMotivo())};
        //{"idMotivo", "nombreMotivo"};
        Cursor c=db.query("motivo", null,"idMotivo=?",id,null,null,null);
        if(c.moveToFirst()){
            Motivo motivo2=motivoConsultar(String.valueOf(motivo.getIdMotivo()));
            String id2[]={String.valueOf(motivo2.getIdMotivo())};
            //duda
            contador=db.query("movimientoInventario",null,"idMotivo=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("motivo","idMotivo=?",id);
                return mensaje=mensaje+motivo.getIdMotivo();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El motivo con id: : "+motivo.getIdMotivo()+" no existe";
        }
    }

    public String eliminar2Motivo(Motivo motivo){
        String mensaje = "Se elimino el motivo con el nombre: "+ motivo.getNombreMotivo();
        String id[]={String.valueOf(motivo.getIdMotivo())};
        db.delete("motivo","idMotivo=?",id);
        return mensaje;
    }

    public Cursor obtenerMotivos() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("motivo", camposMotivo,null,null, null, null, null);
    }



    //tabla tipoDocumento

    private static final String[]camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"};

    public String insertarTipoDocumento(TipoDocumento tipoDocumento){
        String mensaje= "Tipo documento insertada con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"};
        v.put("nombreTipoDocumento", tipoDocumento.getNombreTipoDocumento());
        inserto=db.insert("tipoDocumento", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar tipo documento, registro duplicado.";
        }else {
            mensaje=mensaje+tipoDocumento.getNombreTipoDocumento();
        }
        return mensaje;
    }

    public TipoDocumento tipoDocumentoConsultar(String nombreTipoDocumento){
        String[] id={nombreTipoDocumento};
        Cursor c=db.query("tipoDocumento", camposTipoDocumento, "nombreTipoDocumento=?", id, null, null, null);
        if(c.moveToFirst()){
            TipoDocumento tipoDocumento= new TipoDocumento();
            //{"idTipoDocumento", "nombreTipoDocumento"};
            tipoDocumento.setIdTipoDocumento(c.getInt(0));
            tipoDocumento.setNombreTipoDocumento(c.getString(1));
            return tipoDocumento;
        }else {
            return null;
        }
    }

    public String actualizarTipoDocumento(TipoDocumento tipoDocumento, String clave){
        String mensaje= "Se actualizo tipo documento con nombre: ";
        String id[]={clave};
        ////{"idTipoDocumento", "nombreTipoDocumento"};
        Cursor c=db.query("tipoDocumento", null,"nombreTipoDocumento=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"idTipoDocumento", "nombreTipoDocumento"};
                v.put("idTipoDocumento",tipoDocumento.getIdTipoDocumento());
                v.put("nombreTipoDocumento",tipoDocumento.getNombreTipoDocumento());
                db.update("tipoDocumento",v,"nombreTipoDocumento=?",id);
                mensaje=mensaje+tipoDocumento.getNombreTipoDocumento();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El tipo de documento con el nombre: "+tipoDocumento.getNombreTipoDocumento()+" no existe";
        }
    }

    public String eliminar1TipoDocumento(TipoDocumento tipoDocumento){
        String mensaje = "Se elimino el tipo de documento con el nombre: ";
        int contador=0;
        String id[]={tipoDocumento.getNombreTipoDocumento()};
        //{"idTipoDocumento", "nombreTipoDocumento"};
        Cursor c=db.query("tipoDocumento", null,"nombreTipoDocumento=?",id,null,null,null);
        if(c.moveToFirst()){
            TipoDocumento tipoDocumento2=tipoDocumentoConsultar(tipoDocumento.getNombreTipoDocumento());
            String id2[]={String.valueOf(tipoDocumento2.getIdTipoDocumento())};
            //duda
            contador=db.query("documento",null,"idTipoDocumento=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("tipoDocumento","nombreTipoDocumento=?",id);
                return mensaje=mensaje+tipoDocumento.getNombreTipoDocumento();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El tipo de documento con nombre: : "+tipoDocumento.getNombreTipoDocumento()+" no existe";
        }
    }

    public String eliminar2TipoDocumento(TipoDocumento tipoDocumento){
        String mensaje = "Se elimino el tipo de documento con el nombre: "+tipoDocumento.getNombreTipoDocumento();
        String id[]={tipoDocumento.getNombreTipoDocumento()};
        db.delete("tipoDocumento","nombreTipoDocumento=?",id);
        return mensaje;
    }


    public Cursor obtenerTiposDocumento() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("tipoDocumento", camposTipoDocumento,null,null, null, null, null);
    }


    // tabla Horario

    private static final String[]camposHorario= new String[]{"idHorario", "tiempoHorario"};

    public String insertarHorario(Horario Horario){
        String mensaje= "Horario insertada con hora: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposHorario= new String[]{"idHorario", "tiempoHorario"};
        v.put("tiempoHorario", Horario.getTiempoHorario());
        inserto=db.insert("Horario", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar Horario, registro duplicado.";
        }else {
            mensaje=mensaje+Horario.getTiempoHorario();
        }
        return mensaje;
    }

    public Horario horarioConsultar(String tiempoHorario){
        String[] id={tiempoHorario};
        Cursor c=db.query("Horario", camposHorario, "idHorario=?", id, null, null, null);
        if(c.moveToFirst()){
            Horario Horario= new Horario();
            //{"idHorario", "tiempoHorario"}
            Horario.setIdHorario(c.getInt(0));
            Horario.setTiempoHorario(c.getString(1));
            return Horario;
        }else {
            return null;
        }
    }

    public String actualizarHorario(Horario Horario, String clave){
        String mensaje= "Se actualizo el Horario con hora: ";
        String id[]={clave};
        //{"idHorario", "tiempoHorario"}
        Cursor c=db.query("Horario", null,"idHorario=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"idHorario", "tiempoHorario"}
                v.put("idHorario",Horario.getIdHorario());
                v.put("tiempoHorario",Horario.getTiempoHorario());
                db.update("Horario",v,"idHorario=?",id);
                mensaje=mensaje+Horario.getTiempoHorario();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El Horario con la hora: "+Horario.getTiempoHorario()+" no existe";
        }
    }

    public String eliminar1Horario(Horario Horario){
        String mensaje = "Se elimino el Horario con el nombre: ";
        int contador=0;
        String id[]={String.valueOf(Horario.getIdHorario())};
        //{"idHorario", "tiempoHorario"};
        Cursor c=db.query("Horario", null,"idHorario=?",id,null,null,null);
        if(c.moveToFirst()){
            Horario Horario2=horarioConsultar(String.valueOf(Horario.getIdHorario()));
            String id2[]={String.valueOf(Horario2.getIdHorario())};
            //duda
            contador=db.query("movimientoInventario",null,"idHorario=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("Horario","idHorario=?",id);
                return mensaje=mensaje+Horario.getIdHorario();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El Horario con nombre: : "+Horario.getIdHorario()+" no existe";
        }
    }

    public String eliminar2Horario(Horario Horario){
        String mensaje = "Se elimino el Horario con el nombre: "+ Horario.getIdHorario();
        String id[]={String.valueOf(Horario.getIdHorario())};
        db.delete("Horario","idHorario=?",id);
        return mensaje;
    }

    public Cursor obtenerHorarios() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("Horario", camposHorario,null,null, null, null, null);
    }


    //tabla Docente

    private static final String[]camposDocente= new String[]{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};

    public String insertarDocente(Docente docente){
        String mensaje= "Docente insertado con nombre: ";
        long inserto=0;
        ContentValues v = new ContentValues();
        //camposDocente= new String[]{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};
        v.put("carnetDocente", docente.getCarnetDocente());
        v.put("nombreDocente", docente.getNombreDocente());
        v.put("apellidoDocente", docente.getApellidoDocente());
        v.put("telefonoDocente", docente.getTelefonoDocente());
        v.put("direccionDocente", docente.getDireccionDocente());
        inserto=db.insert("docente", null,v);
        if (inserto==-1 || inserto==0){
            mensaje="Error, al insertar Docente, registro duplicado.";
        }else {
            mensaje=mensaje+docente.getNombreDocente()+" "+docente.getApellidoDocente();
        }
        return mensaje;
    }

    public Docente docenteConsultar(String carnetDocente){
        String[] id={carnetDocente};
        Cursor c=db.query("docente", camposDocente, "carnetDocente=?", id, null, null, null);
        if(c.moveToFirst()){
            Docente docente= new Docente();
            //{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};
            docente.setCarnetDocente(c.getString(0));
            docente.setNombreDocente(c.getString(1));
            docente.setApellidoDocente(c.getString(2));
            docente.setTelefonoDocente(c.getString(3));
            docente.setDireccionDocente(c.getString(4));
            return docente;
        }else {
            return null;
        }
    }

    public String actualizarDocente(Docente docente, String clave){
        String mensaje= "Se actualizo el docente con carnet: ";
        String id[]={clave};
        //{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};
        Cursor c=db.query("docente", null,"carnetDocente=?",id,null,null,null);
        if(c.moveToFirst()){
            try {
                ContentValues v=new ContentValues();
                //{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};
                v.put("carnetDocente",docente.getCarnetDocente());
                v.put("nombreDocente",docente.getNombreDocente());
                v.put("apellidoDocente",docente.getApellidoDocente());
                v.put("telefonoDocente",docente.getTelefonoDocente());
                v.put("direccionDocente",docente.getDireccionDocente());
                db.update("docente",v,"carnetDocente=?",id);
                mensaje=mensaje+docente.getCarnetDocente();
                return mensaje;
            }catch (SQLException e){
                return mensaje="Conflicto de campos, no se pudo actualizar";
            }
        }else {
            return mensaje="El docente con el carnet: "+docente.getCarnetDocente()+" no existe";
        }
    }

    public String eliminar1Docente(Docente docente){
        String mensaje = "Se elimino el docente con el carnet: ";
        int contador=0;
        String id[]={String.valueOf(docente.getCarnetDocente())};
        //{"carnetDocente", "nombreDocente", "apellidoDocente", "telefonoDocente", "direccionDocente"};
        Cursor c=db.query("docente", null,"carnetDocente=?",id,null,null,null);
        if(c.moveToFirst()){
            Docente docente2=docenteConsultar(docente.getCarnetDocente());
            String id2[]={String.valueOf(docente2.getCarnetDocente())};
            //duda
            contador=db.query("movimientoInventario",null,"carnetDocente=?", id2,null,null,null).getCount();
            if (contador==0){
                db.delete("docente","carnetDocente=?",id);
                return mensaje=mensaje+docente.getCarnetDocente();
            }else {
                return mensaje=String.valueOf(contador);
            }
        }else {
            return mensaje="El Docente con carnet: : "+docente.getCarnetDocente()+" no existe";
        }
    }

    public String eliminar2Docente(Docente docente){
        String mensaje = "Se elimino el docente con el carnet: "+ docente.getCarnetDocente();
        String id[]={String.valueOf(docente.getCarnetDocente())};
        db.delete("docente","carnetDocente=?",id);
        return mensaje;
    }

    public Cursor obtenerDocentes() throws SQLException {
        db=openHelper.getReadableDatabase();
        return db.query("docente", camposDocente,null,null, null, null, null);
    }


}
