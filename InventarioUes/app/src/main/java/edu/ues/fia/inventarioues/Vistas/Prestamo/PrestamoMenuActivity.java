package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.PrestamoAdaptador001;
import edu.ues.fia.inventarioues.Adaptador.PrestamoAdaptador002;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.R;

public class PrestamoMenuActivity extends AppCompatActivity {

    Integer vista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_menu);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");

        if (vista==1){
            ListView listView =(ListView)findViewById(R.id.listPrestamosMenu);
            List<Prestamo> lista= listadoPrestamos();
            PrestamoAdaptador001 prestamoAdaptador=new PrestamoAdaptador001(this,lista);
            listView.setAdapter(prestamoAdaptador);
        }else {
            ListView listView =(ListView)findViewById(R.id.listPrestamosMenu);
            List<Prestamo> lista= listadoPrestamos();
            PrestamoAdaptador002 prestamoAdaptador=new PrestamoAdaptador002(this,lista);
            listView.setAdapter(prestamoAdaptador);
        }

    }

    public List<Prestamo> listadoPrestamos(){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (vista==1){
            Cursor c =helper.obtenerPrestamos001();
            List<Prestamo> prestamoList= new ArrayList<>();
            while (c.moveToNext()){
                Prestamo prestamo=new Prestamo();
                //idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
                // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
                // serialEquipo, idDocumento, idMateria
                
                prestamo.setIdMovimientoInventario(c.getInt(0));
                prestamo.setIdTipoMovimiento(c.getInt(1));
                prestamo.setCarnetDocente(c.getString(2));
                prestamo.setFechaMovimientoInventario(c.getString(3));
                prestamo.setFechaDevolucionMovInv(c.getString(4));
                prestamo.setEstadoMovInv(c.getInt(5));
                prestamo.setIdMotivo(c.getInt(6));
                prestamo.setIdHorario(c.getInt(7));
                prestamo.setIdUnidadAdministrativa(c.getInt(8));
                prestamo.setSerialEquipo(c.getString(9));
                prestamo.setIdDocumento(c.getInt(10));
                prestamo.setIdMateria(c.getString(11));
                prestamoList.add(prestamo);
            }
            return prestamoList;
        }else {
            Cursor c =helper.obtenerPrestamos002();
            List<Prestamo> prestamoList= new ArrayList<>();
            while (c.moveToNext()){
                Prestamo prestamo=new Prestamo();
                //idMovimientoInventario, idTipoMovimiento, carnetDocente, fechaMovimientoInventario
                // fechaDevolucionMovInv, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa
                // serialEquipo, idDocumento, idMateria

                prestamo.setIdMovimientoInventario(c.getInt(0));
                prestamo.setIdTipoMovimiento(c.getInt(1));
                prestamo.setCarnetDocente(c.getString(2));
                prestamo.setFechaMovimientoInventario(c.getString(3));
                prestamo.setFechaDevolucionMovInv(c.getString(4));
                prestamo.setEstadoMovInv(c.getInt(5));
                prestamo.setIdMotivo(c.getInt(6));
                prestamo.setIdHorario(c.getInt(7));
                prestamo.setIdUnidadAdministrativa(c.getInt(8));
                prestamo.setSerialEquipo(c.getString(9));
                prestamo.setIdDocumento(c.getInt(10));
                prestamo.setIdMateria(c.getString(11));
                prestamoList.add(prestamo);
            }
            return prestamoList;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


}
